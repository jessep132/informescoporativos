<?php

/**
 *
 * Clase de la cual heredan el resto de las controladoras usadas en la aplicación.
 * Provee métodos al desarrollador para cargar vistas de backend y frontend, restringir acceso a determinados controladores a un grupo
 * de usuarios o tipos de usuario, entre otras funciones.
 *
 * @package    CORE
 * @author    Casas y Terrenos
 */
class MY_Controller extends CI_Controller
{

    /**
     * Constructor
     */
    public function __construct($logued_required = TRUE)
    {
        parent::__construct();
    }



    /** Muestra la página de frontend
     *
     * @param string Vista que se mostrará
     * @param array $data Datos a pasarle a la vista
     * @author Grupo solana
     * @access public
     */



    public function view($subview, $data = array())
    {

        
		$this->load->database();
		/* Prueba de conexion a la DB */
		//sql ="SELECT * FROM WEB_APPS.dbo.general_directory WHERE STATUS = 1 ORDER BY NAME2 ASC";
		//$agencias = $this->db->query($sql)->result_array();
        $agencias="";
        
        
        $data['subview'] = $subview;
        $data['agencias'] = $agencias;
        $new_data = $data;
        $this->load->view("tpl/page.php", $new_data);
    }


    public function view2($subview, $data = array())
    {

        
		$this->load->database();        
        $data['subview'] = $subview;
        $new_data = $data;
        $this->load->view("tpl/page2.php", $new_data);
    }

    
    public function view3($subview, $data = array())
    {

        
		$this->load->database();        
        $data['subview'] = $subview;
        $new_data = $data;
        $this->load->view("tpl/page3.php", $new_data);
    }
    
}
