<?php
/**
 *
 * Clase genérica usada para la sincronización de registros en las tablas locales con las del Webservice.
 *
 * @package	CORE
 * @author	Casas y Terrenos
 */
class MY_WebserviceModel extends MY_Model
{

        /**
	* Constructor
	*/
	public function __construct()
        {
		parent::__construct();
						
		$this->load->library('nu_soap'); 

	    // Instanciamos la clase servidor de nusoap
	    //$this->NuSoap_server = new nusoap_server();
		
		$this->soapClient = new nusoap_client( SOAP_SERV , TRUE);
		
		//$respuesta = $this->soapClient->call("Catalogos");
		 
	}

        /** Ejecuta una llamada al método del webservice
        *  @author Casas y Terrenos
        *  @access public
        *  @param  string $method Nombre del método del webservice a invocar
        *  @param  array $params Array de parámetos que recibe
        *  @return array Elementos devueltos por el método
        */
	public function call( $method, $params="" )
    {
		$response = $this->soapClient->call( $method, $params );
		return $response;
	}

        /** Sincroniza la tabla de la base de datos: $table, con los resultados devueltos por el webservice para la misma
        *  @author Casas y Terrenos
        *  @access public
        *  @param  string $tabla, tabla a sincronizar
        *  @param  array  $data, datos obtenidos del ws
        *  @param  array  $fields (key_db => key_ws) recibe los campos a insertar/actualizar, la llave es el nombre de la columna de la base de datos y
        *  el valor la llave con la que viene del webservice. El primer elemento de este arreglo es el campo primario (pk) de la tabla para el caso en que
        *  la tabla solamente tenga una pk {$cant_pk = 1}
        *  @param  bool  $catalogos, se refiere a si lo que se sincroniza son catálogos, ya que de serlo habrá que eliminar aquellos que ya no son relacionados en $data
        *  @param  int $cant_pk, en el array $fields, el primer campo que recibe es la llave primaria de la tabla, en caso de existir más de una llave, se especifica
        *  la cantidad y el array $fields las contendrá como los primeras {$cant_pk} datos de este arreglo
        *  @param  bool $forceInt Si cant_pk es mayor a uno, y si los campos primarios distintos de código son enteros
        *  @return array Cantidades de elementos actualizados o modificados tras la sincronización
        */
        public function sincronizar( $tabla, $data, $fields, $catalogos = false, $cant_pk = 1, $forceInt = false )
        {
                set_time_limit(0); 
                $this->load->database();
                $data_to_insert = array();
                $result = array( 'inserts' => 0, 'updates' => 0, 'errores' => 0 );
                $ids    = array();
                $texto  = 'Identificador - Nuevo/Modificado - Query ' .chr(13).chr(10);

                foreach( $data as $i )
                {
                        $query_result = '';
                        $first = true;
                        $pk = $valor = '';
                        $keys_added = 0;
                        $actual_pk = $actual_val = '';
                        foreach ($fields as $key_db => $key_ws)
                        {
                                if( $first && $cant_pk == 1 )
                                {
                                        $pk     = $key_db;
                                        $valor  = ( $forceInt && $key_db != 'codigo' ) ? (int) trim( $i[$key_ws] ) : trim( $i[$key_ws] ) ;
                                        $first  = false;
                                }
                                else if( $cant_pk > 1 && $keys_added < $cant_pk)
                                {
                                        $pk[] = $key_db;
                                        $valor[] = ( $forceInt && $key_db != 'codigo' ) ? (int) trim( $i[$key_ws] ) : trim( $i[$key_ws] ) ;
                                        $keys_added++;
                                }
                                if( !isset( $i[$key_ws] ) && $key_db != "last_update" )
                                {
                                    $data_to_insert[$key_db] = "";
                                }
                                else if( ( $tabla == 'productos' && $key_db == 'last_update' ) || ( $tabla == 'clientes' && $key_db == 'last_update' ) )
                                {
                                    $data_to_insert["last_update"] = date_to_mysql( date("Y/m/d H:i:s") );
                                }
                                else if( is_array ( $i[$key_ws] ) )
                                {
                                    $data_to_insert[$key_db] = '';
                                }
                                else if( $tabla == 'productos' && $key_db == 'descripcion' )
                                {
                                    $data_to_insert[$key_db] = first_capital ( trim( utf8_decode(  $i[$key_ws] ) ) );
                                }
                                else
                                {
                                    $data_to_insert[$key_db] = trim( utf8_encode(  $i[$key_ws] ) );
                                }
                        }
                        try{
                                $ids[] = $valor;
                                $temp = $key_result = '';
                                if( !$this->dataExists( $tabla, $pk, $valor ) )
                                {
                                        if( is_array( $pk ) )
                                        {
                                            $temp = implode(",", $valor)." - Registro Nuevo.".chr(13).chr(10);
                                        }
                                        else
                                        {
                                            $temp = $valor." - Registro Nuevo.".chr(13).chr(10);
                                        }
                                        $this->db->insert( $tabla, $data_to_insert );
                                        $key_result = 'inserts';
                                }
                                else
                                {
                                        $key_result = 'updates';
                                        if( is_array( $pk ) )
                                        {
                                            $temp = implode(",", $valor)." - Registro Modificado.";
                                            foreach( $pk as $k=>$prim )
                                            {
                                                $this->db->where( $prim, $valor[$k] );
                                                unset( $data_to_insert[$prim] );
                                            }
                                        }
                                        else
                                        {
                                            $temp = $valor." - Registro Modificado.";
                                            $this->db->where( $pk, $valor );
                                            unset( $data_to_insert[$pk] );
                                        }
                                        $this->db->update( $tabla, $data_to_insert );
                                }

                                $query_result = $this->db->last_query();
                                
                                $temp .= ' - [QUERY: '.$query_result.']'.chr(13).chr(10);

                                if( $this->db->affected_rows() < 0 )
                                {
                                    throw new Exception( "Error de consulta SQL" );
                                }
                                $result[$key_result]++;
                                $texto .= $temp;
                        }
                        catch(Exception $e)
                        {
                            $result['errores'] ++;
                            if( is_array( $pk ) )
                            {
                                $codigo = implode(",", $valor);
                            }
                            else
                            {
                                $codigo = $valor;
                            }
                            $texto .= '**** REGISTRO CON ERROR ***** ['.date("Y/m/d").']['.date("H:i:s").'][CODIGO: '.$codigo.'][QUERY: '.$query_result.']'.chr(13).chr(10);
                        }
                }

                $fp=fopen('temp/sinc_' . $tabla . '_' . date_int(date("d-m-Y")) . ( $result['errores'] ? '_con_error' : ''). '.txt',"w+");
                fwrite($fp, $texto);
                fclose($fp);
                if( $catalogos && isset( $pk ) )
                {
                    $this->deleteUnused ($tabla, $pk, $ids);
                }
                return $result;
        }

		/** Sincroniza la tabla de la base de datos: $table, con los resultados devueltos por el webservice para la misma
        *  @author Casas y Terrenos
        *  @access public
        *  @param  string $tabla, tabla a sincronizar
        *  @param  array  $data, datos obtenidos del ws
        *  @param  array  $fields (key_db => key_ws) recibe los campos a insertar/actualizar, la llave es el nombre de la columna de la base de datos y
        *  el valor la llave con la que viene del webservice. El primer elemento de este arreglo es el campo primario (pk) de la tabla para el caso en que
        *  la tabla solamente tenga una pk {$cant_pk = 1}
        *  @param  bool  $catalogos, se refiere a si lo que se sincroniza son catálogos, ya que de serlo habrá que eliminar aquellos que ya no son relacionados en $data
        *  @param  int $cant_pk, en el array $fields, el primer campo que recibe es la llave primaria de la tabla, en caso de existir más de una llave, se especifica
        *  la cantidad y el array $fields las contendrá como los primeras {$cant_pk} datos de este arreglo
        *  @param  bool $forceInt Si cant_pk es mayor a uno, y si los campos primarios distintos de código son enteros
        *  @return array Cantidades de elementos actualizados o modificados tras la sincronización
        */
        public function sincronizar_pv_clientes( $tabla, $data, $fields, $catalogos = false, $cant_pk = 1, $forceInt = false )
        {
                set_time_limit(0); 
                $this->load->database();
                $data_to_insert = array();
                $result = array( 'inserts' => 0, 'updates' => 0, 'errores' => 0 );
                $ids    = array();
                $texto  = 'Identificador - Nuevo/Modificado - Query ' .chr(13).chr(10);

                foreach( $data as $i )
                {
                        $query_result = '';
                        $first = true;
                        $pk = $valor = '';
                        $keys_added = 0;
                        $actual_pk = $actual_val = '';
                        foreach ($fields as $key_db => $key_ws)
                        {
                                if( $first && $cant_pk == 1 )
                                {
                                        $pk     = $key_db;
                                        $valor  = ( $forceInt && $key_db != 'codigo' ) ? (int) trim( $i[$key_ws] ) : trim( $i[$key_ws] ) ;
                                        $first  = false;
                                }
                                else if( $cant_pk > 1 && $keys_added < $cant_pk)
                                {
                                        $pk[] = $key_db;
                                        $valor[] = ( $forceInt && $key_db != 'codigo' ) ? (int) trim( $i[$key_ws] ) : trim( $i[$key_ws] ) ;
                                        $keys_added++;
                                }
                                if( !isset( $i[$key_ws] ) && $key_db != "last_update" )
                                {
                                    $data_to_insert[$key_db] = "";
                                }
                                else if( ( $tabla == 'productos' && $key_db == 'last_update' ) || ( $tabla == 'pb_clientes' && $key_db == 'last_update' ) )
                                {
                                    $data_to_insert["last_update"] = date_to_mysql( date("Y/m/d H:i:s") );
                                }
                                else if( is_array ( $i[$key_ws] ) )
                                {
                                    $data_to_insert[$key_db] = '';
                                }
                                else if( $tabla == 'productos' && $key_db == 'descripcion' )
                                {
                                    $data_to_insert[$key_db] = first_capital ( trim( utf8_decode(  $i[$key_ws] ) ) );
                                }
                                else
                                {
                                    $data_to_insert[$key_db] = trim( utf8_encode(  $i[$key_ws] ) );
                                }
                        }
                        try{
                                $ids[] = $valor;
                                $temp = $key_result = '';
                                if( !$this->dataExists( $tabla, $pk, $valor ) )
                                {
                                        if( is_array( $pk ) )
                                        {
                                            $temp = implode(",", $valor)." - Registro Nuevo.".chr(13).chr(10);
                                        }
                                        else
                                        {
                                            $temp = $valor." - Registro Nuevo.".chr(13).chr(10);
                                        }
                                        $this->db->insert( $tabla, $data_to_insert );
                                        $key_result = 'inserts';
                                }
                                else
                                {
                                        $key_result = 'updates';
                                        if( is_array( $pk ) )
                                        {
                                            $temp = implode(",", $valor)." - Registro Modificado.";
                                            foreach( $pk as $k=>$prim )
                                            {
                                                $this->db->where( $prim, $valor[$k] );
                                                unset( $data_to_insert[$prim] );
                                            }
                                        }
                                        else
                                        {
                                            $temp = $valor." - Registro Modificado.";
                                            $this->db->where( $pk, $valor );
                                            unset( $data_to_insert[$pk] );
                                        }
                                        $this->db->update( $tabla, $data_to_insert );
                                }

                                $query_result = $this->db->last_query();
                                
                                $temp .= ' - [QUERY: '.$query_result.']'.chr(13).chr(10);

                                if( $this->db->affected_rows() < 0 )
                                {
                                    throw new Exception( "Error de consulta SQL" );
                                }
                                $result[$key_result]++;
                                $texto .= $temp;
                        }
                        catch(Exception $e)
                        {
                            $result['errores'] ++;
                            if( is_array( $pk ) )
                            {
                                $codigo = implode(",", $valor);
                            }
                            else
                            {
                                $codigo = $valor;
                            }
                            $texto .= '**** REGISTRO CON ERROR ***** ['.date("Y/m/d").']['.date("H:i:s").'][CODIGO: '.$codigo.'][QUERY: '.$query_result.']'.chr(13).chr(10);
                        }
                }

                $fp=fopen('temp/sinc_' . $tabla . '_' . date_int(date("d-m-Y")) . ( $result['errores'] ? '_con_error' : ''). '.txt',"w+");
                fwrite($fp, $texto);
                fclose($fp);
                if( $catalogos && isset( $pk ) )
                {
                    $this->deleteUnused ($tabla, $pk, $ids);
                }
                return $result;
        }



        /** Sincroniza la tabla de la base de datos: $table, con los resultados devueltos por el webservice para la misma
        *  @author Casas y Terrenos
        *  @access public
        *  @param  string $tabla, tabla a sincronizar
        *  @param  array  $data, datos obtenidos del ws
        *  @param  array  $fields (key_db => key_ws) recibe los campos a insertar/actualizar, la llave es el nombre de la columna de la base de datos y
        *  el valor la llave con la que viene del webservice. El primer elemento de este arreglo es el campo primario (pk) de la tabla para el caso en que
        *  la tabla solamente tenga una pk {$cant_pk = 1}
        *  @param  bool  $catalogos, se refiere a si lo que se sincroniza son catálogos, ya que de serlo habrá que eliminar aquellos que ya no son relacionados en $data
        *  @param  int $cant_pk, en el array $fields, el primer campo que recibe es la llave primaria de la tabla, en caso de existir más de una llave, se especifica
        *  la cantidad y el array $fields las contendrá como los primeras {$cant_pk} datos de este arreglo
        *  @param  bool $forceInt Si cant_pk es mayor a uno, y si los campos primarios distintos de código son enteros
        *  @return array Cantidades de elementos actualizados o modificados tras la sincronización
        */
       public function sincronizar_pv( $tabla, $data, $fields, $catalogos = false, $cant_pk = 1, $forceInt = false )
        {
                set_time_limit(0); 
                $this->load->database();
                $data_to_insert = array();
                $result = array( 'inserts' => 0, 'updates' => 0, 'errores' => 0 );
                $ids    = array();
                $texto  = 'Identificador - Nuevo/Modificado - Query ' .chr(13).chr(10);


                foreach( $data as $i )
                {
                        $query_result = '';
                        $first = true;
                        $pk = $valor = '';
                        $keys_added = 0;
                        $actual_pk = $actual_val = '';
                        foreach ($fields as $key_db => $key_ws)
                        {
                                if( $first && $cant_pk == 1 )
                                {
                                        $pk     = $key_db;
                                        
                   
                                        $valor  = ( $forceInt && $key_db != 'codigo' ) ? (int) trim( $i[$key_ws] ) : trim( $i[$key_ws] ) ;
                                        $first  = false;
                                }
                                else if( $cant_pk > 1 && $keys_added < $cant_pk)
                                {
                                	
									;
                                        $pk[] = $key_db;
                                        $valor[] = ( $forceInt && $key_db != 'codigo' ) ? (int) trim( $i[$key_ws] ) : trim( $i[$key_ws] ) ;
                                        $keys_added++;
                                }
                                if( !isset( $i[$key_ws] ) && $key_db != "last_update" )
                                {
                                	
                                    $data_to_insert[$key_db] = "";
                                }
                                else if( ( $tabla == 'pv_productos' && $key_db == 'last_update' ) || ( $tabla == 'clientes' && $key_db == 'last_update' ) )
                                {
                              
                                    $data_to_insert["last_update"] = date_to_mysql( date("Y/m/d H:i:s") );
                                }
                                else if( is_array ( $i[$key_ws] ) )
                                {
                                	
                                    $data_to_insert[$key_db] = '';
                                }
                                else if( $tabla == 'pv_productos' && $key_db == 'descripcion' )
                                {
                                	
                                    $data_to_insert[$key_db] = first_capital ( trim( utf8_decode(  $i[$key_ws] ) ) );
                                }
								else if( $tabla == 'pv_productos' && $key_db == 'suc' )
                                {
                                	$pk1 = $key_db;
									$valor1  = ( $forceInt && $key_db != 'suc' ) ? (int) trim( $i[$key_ws] ) : trim( $i[$key_ws] ) ;
									
									 $data_to_insert[$key_db] =  trim( utf8_decode(  $i[$key_ws] ));
                                    
                                }
                                else
                                {
                                	
                                    $data_to_insert[$key_db] = trim( utf8_encode(  $i[$key_ws] ) );
                                }
                        }
                        try{
                                $ids[] = $valor;
                                $temp = $key_result = '';
								
								
                                if( !$this->dataExists_pv( $tabla, $pk, $valor, $pk1, $valor1 ) )
                                {
                                	
                                        if( is_array( $pk ) )
                                        {
                                            $temp = implode(",", $valor)." - Registro Nuevo.".chr(13).chr(10);
                                        }
                                        else
                                        {
                                            $temp = $valor." - Registro Nuevo.".chr(13).chr(10);
                                        }
                                        $this->db->insert( $tabla, $data_to_insert );
                                        $key_result = 'inserts';
                                }
                                else
                                {
                                	
                                        $key_result = 'updates';
                                        if( is_array( $pk ) )
                                        {
                                        	
                                            $temp = implode(",", $valor)." - Registro Modificado.";
                                            foreach( $pk as $k=>$prim )
                                            {
                                                $this->db->where( $prim, $valor[$k] );
                                                unset( $data_to_insert[$prim] );
                                            }
                                        }
                                        else
                                        {
                                       
                                            $temp = $valor." - Registro Modificado.";
                                            $this->db->where( $pk, $valor );
											$this->db->where( $pk1, $valor1 );
                                            unset( $data_to_insert[$pk] );
											unset( $data_to_insert[$pk1] );
                                        }
                                        $this->db->update( $tabla, $data_to_insert );
                                }

                                $query_result = $this->db->last_query();
                                
                                $temp .= ' - [QUERY: '.$query_result.']'.chr(13).chr(10);

                                if( $this->db->affected_rows() < 0 )
                                {
                                    throw new Exception( "Error de consulta SQL" );
                                }
                                $result[$key_result]++;
                                $texto .= $temp;
                        }
                        catch(Exception $e)
                        {
                            $result['errores'] ++;
                            if( is_array( $pk ) )
                            {
                                $codigo = implode(",", $valor);
                            }
                            else
                            {
                                $codigo = $valor;
                            }
                            $texto .= '**** REGISTRO CON ERROR ***** ['.date("Y/m/d").']['.date("H:i:s").'][CODIGO: '.$codigo.'][QUERY: '.$query_result.']'.chr(13).chr(10);
                        }
                }

                $fp=fopen('temp/sinc_' . $tabla . '_' . date_int(date("d-m-Y")) . ( $result['errores'] ? '_con_error' : ''). '.txt',"w+");
                fwrite($fp, $texto);
                fclose($fp);
                if( $catalogos && isset( $pk ) )
                {
                    $this->deleteUnused ($tabla, $pk, $ids);
                }
                return $result;
        }

        /** Verifica la existencia del elemento a sincronizar en la tabla de la base de datos
        *  @author Casas y Terrenos
        *  @access public
        *  @param  string $table Nombre de la tabla
        *  @param  array/string  $pk Array/string de llave primaria de la tabla
        *  @param  array/string  $valor Array/string de valores a buscar si existen o no
        *  @return bool True en caso de existir, false en caso contrario
        */
        public function dataExists( $table, $pk, $valor )
        {
                if( is_array($pk) )
                {
                        foreach ( $pk as $k => $prim)
                        {
                                $this->db->where( $prim, $valor[$k] );
                        }
                }
                else
                {
                    $this->db->where( $pk, $valor );
                }

                return $this->db->count_all_results( $table );

        }
		
		
		 public function dataExists_pv( $table, $pk, $valor , $pk1, $valor1 )
        {
           
                 $this->db->where( $pk, $valor );
                 $this->db->where( $pk1, $valor1 );
                
                 return $this->db->count_all_results( $table );
				 	

        }
		
		
		

        /** Usado solamente para el caso de los catálogos, elimina aquellos catálogos que no fueron traidos del webservice
        *  @author Casas y Terrenos
        *  @access public
        *  @param  string $table Nombre de la tabla
        *  @param  string $pk Llave primaria de la tabla
        *  @param  array  $ids Array de valores a eliminar
        */
        public function deleteUnused( $table, $pk, $ids )
        {
                $sql = "DELETE FROM $table WHERE $pk NOT IN (". implode(',', $ids) .")";

                $this->db->query( $sql );

        }
       
}