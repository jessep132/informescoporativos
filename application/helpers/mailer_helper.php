<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Form Helpers
 *
 * @package     Helpers
 * @author	Casas y Terrenos
 * @version     1.0
 */

// ------------------------------------------------------------------------

/**
 *  uriDetail 
 * 
 *  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
 *  @access public
 *  @param  string $name
 *  @param  string $from
 *  @param  string/Array $to String o Array de destinatarios
 *  @param  string $subject (html)
 *  @param  string (html) $html Cuerpo del correo
 *  @param  string/Array $copy String o Array de direcciones de correo para enviar copia.
 *  @param  string/Array $attach String o Array de destinos a los ficheros a adjuntar.
 *  @return bolean
 */
if (!function_exists('mailSend')) {
    function mailSend($name, $from, $to, $subjet, $html, $copy = null, $attach = null, $attach2 = null)
    {
        $CI = &get_instance();
        $CI->load->library('MY_PHPMailer');

        $mail = new PHPMailer(true);

        $mail->IsSMTP();
        try {

            $mail->SMTPDebug = 0;
            $mail->Host = 'smtp.sendgrid.net';
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail->CharSet = "UTF-8";
            //$mail->Username   = 'casasyterrenos';
            $mail->Username = 'grupociosa';
            //$mail->Username   = 'ciosaonline';
            //$mail->Username   = 'it4pymes';
            //$mail->Username   = 'mbsoftcloud';
            //$mail->Password   = 'Nelson20';
            $mail->Password = 'Ciosagrupo03';
			//$mail->Password   = 'witenadm03';
            $mail->crlf = '\r\n';
            $mail->newline = '\r\n';
            $mail->mailtype = 'html';
            $mail->From = $from;
            $mail->FromName = $name;
            $mail->AddAddress($to);
            if (!is_null($copy)) {
                if (is_array($copy)) {
                    foreach ($copy as $cc)
                        $mail->AddBcc($cc);
                } else{
                    $mail->AddBcc($copy);
                }
            }

            if (!is_null($attach)) {
                if (is_array($attach)) {
                    foreach ($attach as $at)
                        $mail->AddAttachment($at);
                } else{
                    $mail->AddAttachment($attach);
                }
            }

            $mail->WordWrap = 50;                              // set word wrap
            $mail->IsHTML(true);                               // send as HTML
            $mail->Subject = $subjet;
            $mail->Body = $html;


            if (!$mail->Send()) {
                $mail->ClearAddresses();
                $mail->ClearAttachments();
                $mail->ClearReplyTos();
                $mail->ClearAllRecipients();
                $mail->ClearCustomHeaders();
                return false;
            } else {
                $mail->ClearAddresses();
                $mail->ClearAttachments();
                $mail->ClearReplyTos();
                $mail->ClearAllRecipients();
                $mail->ClearCustomHeaders();

                return true;
            }
        } catch (phpmailerException $e) {


            $rand = md5(uniqid(rand(), true));
             //flog( '[EMAIL LOG][ERROR: '.$mail->ErrorInfo.'][TO: '.$to.'][SUBJECT: '.$subjet.'][BODY: <a href="javascript:;" onclick="javascript:show(\''.$rand.'\');">Here</a>]                 <div id="'.$rand.'" style="display:none"> '.$html.'</div>');
            $mail->ClearAddresses();
            $mail->ClearAttachments();
            return false;
        }
    }
}
