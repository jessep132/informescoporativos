<?php
/**
 * Clase que es utilizada para recuperar los catálogos de sitemas, grupos, subgrupos, marcas, tipos de pedido y vendedores provenientes del webservice
 * y sincronizarlos con los existentes en las tablas locales.
 *
 * @package	Modelos
 * @author	Casas y Terrenos
 * @version     1.0
 */

//require_once("C:\AppServ\www\ciosa\application\core\MY_WebserviceModel.php");
require_once("./application/core/MY_WebserviceModel.php"); // Buscar que se carge en el load =D

class Catalogo_w extends MY_WebserviceModel
{
	var $method = "Catalogos";

        /**
        * Constructor
        */
	public function __construct()
        {
		parent::__construct();
	}

        /** Función que ejecuta la llamada al método Catálogos del Webservice
        *  @author Casas y Terrenos
        *  @access public
        *  @param  string $fecha Fecha desde la cual se actualizará el catálogo
        *  @return array Catálogos
        */
	public function get( $fecha= "" )
        { // Formato de la fecha es YYYYmmdd
		if( $fecha )
                {
                    $params = array( "fecha"  => $fecha );
                }
		else
                {
                    $params = array();
                }
		$data = $this->call( $this->method, $params );
		$this->response = $data;
		return $data;
	}

        /**
        *  @author Casas y Terrenos
        *  @access private
        */
	public function call( $fecha = "" )
        { // Formato de la fecha es YYYYmmdd
		if( $fecha )
                {
                    $params = array( "fecha"  => $fecha );
                }
		else
                {
                    $params = array();
                }
		$data = parent::call( $this->method, $params );
		return $data;
	}

        /** Función que toma los catálogos pertenecientes a Sistemas y los retorna
        *  @author Casas y Terrenos
        *  @access public
        *  @return array Sistemas devueltos tras la llamada al webservice Catalogos
        */
	public function getSistemas()
        {
                if( isset( $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["Sistemas"] ) )
                {
                    return $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["Sistemas"];
                }
                else
                {
                    return array();
                }
	}

        /** Función que toma los catálogos pertenecientes a Grupos y los retorna
        *  @author Casas y Terrenos
        *  @access public
        *  @return array Grupos devueltos tras la llamada al webservice Catalogos
        */
	public function getGrupos()
        {
                if( isset( $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["Grupos"] ) )
                {
                    return $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["Grupos"];
                }
                else
                {
                    return array();
                }
	}

        /** Función que toma los catálogos pertenecientes a Subgrupos y los retorna
        *  @author Casas y Terrenos
        *  @access public
        *  @return array Subgrupos devueltos tras la llamada al webservice Catalogos
        */
	public function getSubGrupos()
        {
                if( isset( $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["SubGrupos"] ) )
                {
                    return $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["SubGrupos"];
                }
                else
                {
                    return array();
                }
	}

        /** Función que toma los catálogos pertenecientes a Marcas y los retorna
        *  @author Casas y Terrenos
        *  @access public
        *  @return array Marcas devueltas tras la llamada al webservice Catalogos
        */
	public function getMarcas()
        {
                if( isset( $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["Marcas"] ) )
                {
                    return $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["Marcas"];
                }
                else
                {
                    return array();
                }
	}

        /** Función que toma los catálogos pertenecientes a TiposPedido y los retorna
        *  @author Casas y Terrenos
        *  @access public
        *  @return array Tipos de pedido devueltos tras la llamada al webservice Catalogos
        */
	public function getTiposPedido()
        {
                if( isset( $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["TiposPedido"] ) )
                {
                    return $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["TiposPedido"];
                }
                else
                {
                    return array();
                }
	}

        /** Función que toma los catálogos pertenecientes a Vendedores y los retorna
        *  @author Casas y Terrenos
        *  @access public
        *  @return array Vendedores devueltos tras la llamada al webservice Catalogos
        */
	public function getVendedores()
        {
                if( isset( $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["Vendedores"] ) )
                    return $this->response["CatalogosResult"]["diffgram"]["NewDataSet"]["Vendedores"];
                else return array();
	}
	
	

    /** Función que sincroniza la tabla de la base de datos: $table, con los resultados devueltos por el webservice para la misma
    *  @author Casas y Terrenos
    *  @access public
    *  @param  string $table Nombre de la tabla a sincronizar
    *  @param  array $data Array de elementos devueltos por el webservice
    *  @return array Cantidades de elementos actualizados o modificados tras la sincronización
    */
	public function sincronizar($table, $data )
    {
            
		if( in_array( $table, array("sistemas", "grupos", "subgrupos", "tipospedido", "marcas") ) )
                {
			$fields = array(
                            "codigo"        => "codigo",
                            "descripcion"   => "descripcion"
                        );
		}
                elseif( $table == "vendedores" )
                {
			$fields = array(
                            "codigo"    => "clave",
                            "nombre"    => "nombre",
                            "email"     => "correo"
                        );
		}
                return parent::sincronizar($table, $data, $fields, true);
	}
	
}

























