<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Sysmaster - casasyterrenos.com
 * Model: Login
 * 
 * @Src: /application/models/
 * @Copyright: Copyright 2012 - Marketing Digital Casas y Terrenos S.A. de C.V.
 * @Developer: Jessep Martinez Barba (jessep.martinez@casasyterrenos.com)
 * @Create: 26-Noviembre-2012
 * 
 * 
*/


class Login_model extends CI_Model 
{
	
	#@ create session user
	function loginUser($user, $password)
	{
		//, "usu_password" => $password

	
		
		$this->load->model('db_model');
		$condicion = array("use_login" => $user, "use_active" => '1');
		$query = $this->db_model->oneRow("users", $condicion);

	
		if(empty($query)){
			$result_count == 0;
		}else{
			$result_count = count($query);
		}
		
		if($result_count == 0)
		{
			$resultado['error'] = 'El usuario no existe';
		}
		else
		{
			

			$pw = hash('sha256',$password);
		
			//if(strcmp($password, $this->encrypt->decode($query['use_key'])) == 0)
			if(strcmp($pw, $query['use_key']) == 0)
			{
	
	            $parametros = array(
		            'use_lastaccess'    => date('Y-m-d H:m:s')
	            );
	
		
				$resultado =$query;
				

			}
			else
			{

				$resultado['error'] = 'Login Incorrecto';
			}
	
       	}
		
		return $resultado;
		
	}
	
	
	
	
	
}
	