 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
          <a href="<?php echo base_url();?>home/menu" class="brand-link">
                <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >

              </a>
           
          </div><!-- /.col -->
          <div class="col-sm-6" align="center">
             <!-- Brand Logo -->
             <h2 class="m-0"><B>Dashboard(GLOBAL)</B></h2>
          </div><!-- /.col -->
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
             <a href="<?php echo base_url();?>home/menu"  >
              <button type="button" class="btn btn-primary btn-lg"> << Regresar</button>
             </a>
            </div>
          </div>

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<div class="container-fluid">
    





        <!-- Small boxes (Stat box) -->
     

        <div style="">
        &nbsp;&nbsp;<B>Opinión de Cumplimiento Obligaciones Fiscales</B>
        <br>
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info" >
              <div class="inner">
                <h3><?php echo ($proveedores);?></h3>

                <p><br ><br>Total</p>
              </div>
              <div class="icon">
                
                <i class="ion ion-person"></i>
              </div>
              <!--
              <a href="<?php echo base_url();?>proveedores/todos" class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success" >
              <div class="inner">
                <h3><?php echo ($proveedoresPositivo);?></h3>

                <p><br ><br>Cumplimiento Positivo</p>
              </div>
              <div class="icon">
                <i class="far "><?php 
                        $prov1 = $proveedoresPositivo * 100;
                        $porcentaje1 = $prov1/$proveedores;

                        echo  $porcentaje1.' %';
                ?></i>
              </div>
               <!--
              <a href="<?php echo base_url();?>proveedores/positivo"  class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
              <h3 style="color:white;"><?php echo ($proveedoresSinRespuesta);?></h3>

                <p style="color:white;"><br>Sin Respuesta <br >Portal SAT / Proveedor </p>
              </div>
              <div class="icon">
                <i class="far ">
                <?php 
                        $prov2 = $proveedoresSinRespuesta * 100;
                        $porcentaje2 = $prov2/$proveedores;

                        echo  $porcentaje2.' %';
                ?>
                </i>
              </div>
               <!--
              <a href="<?php echo base_url();?>proveedores/sinrespuesta"  class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
              <h3><?php echo ($proveedoresNegativo);?></h3>

                <p><br>Opinión del cumplimiento de obligaciones fiscales con inconsistencias </p>
              </div>
              <div class="icon">
                <i class="far ">
                <?php 
                        $prov3 = $proveedoresNegativo * 100;
                        $porcentaje3 = $prov3/$proveedores;

                        echo  $porcentaje3.' %';
                ?>
                </i>
              </div>
               <!--
              <a href="<?php echo base_url();?>proveedores/negativo" class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <!-- ./col -->

          
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
           <div class="col-md-6">
             <!-- DONUT CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Insumos</h3>

                <div class="card-tools">
                 
                 
                </div>
              </div>
              <div class="card-body">
                <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

           </div> 

           <div class="col-md-6">
            <!-- DONUT CHART -->
           <div class="card card-primary">
             <div class="card-header">
               <h3 class="card-title">Servicios</h3>

               <div class="card-tools">
                
               </div>
             </div>
             <div class="card-body">
               <canvas id="donutChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
             </div>
             <!-- /.card-body -->
           </div>
           <!-- /.card -->

          </div> 
        </div>
        <!-- /.row (main row) -->
        </div>









      <div>
          <br>
      </div>

<div style="">
&nbsp;&nbsp;<B>Facturas - CFDI´s </B>
<br>
        <!-- Main row -->
        <div class="row">
           <div class="col-md-6">
             <!-- DONUT CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">TOTALES</h3>

                <div class="card-tools">
                 
                 
                </div>
              </div>
              <div class="card-body">
              <canvas id="totalesChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

           </div> 

           

           <div class="col-md-6">
            <!-- DONUT CHART -->
           <div class="card card-primary">
             <div class="card-header">
               <h3 class="card-title"> Por Tipo de Operación Montos</h3>

               <div class="card-tools">
                
               </div>
             </div>
             <div class="card-body">
             <canvas id="montosChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
             </div>
             <!-- /.card-body -->
           </div>
           <!-- /.card -->

          </div> 
        </div>
        <!-- /.row (main row) -->
</div>

<div>
          <br>
      </div>



      <div style="">
      <B> Contribuyentes con Operaciones Presuntamente Inexistentes / EFOS</B>
<br>
         <!-- Main row -->
         <div class="row">
           <div class="col-md-12">
             <!-- DONUT CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><B> EFOS</B></h3>
              </div>
              <div class="card-body">
              <table id="example1"  class="table table-bordered table-striped"> 
              <thead>
                <tr  >
                  <td colspan="4" >
                    <span class="title text-center"><B>NOVIEMBRE</B></span>
                  </td>
                  <td>
                  </td>
                  <td colspan="4" >
                    <span class="title text-center"><B>DICIEMBRE</B></span>
                  </td>
                  <td>
                  </td>
                  <td colspan="4" >
                    <span class="title text-center"><B>ENERO</B></span>
                  </td>
                </tr>
         
                <tr  >
                  <td >
                    <span class="title text-center"><FONT SIZE="2"><B>PROVEEDOR</B></font></span>
                  </td>
                  <td >
                    <span class="title text-center"><FONT SIZE="2"><B>SITUACION</B></font></span>
                  </td>
                  <td >
                    <span class="title text-center"><FONT SIZE="2"><B>VALOR</B></font></span>
                  </td>

                  <td >
                    <span class="title text-center"><FONT SIZE="2"><B>IVA</B></font></span>
                  </td>
                  <td>
                  </td>

                  <td>
                    <span class="title text-center"><FONT SIZE="2"><B>PROVEEDOR</B></font></span>
                  </td>
                  <td >
                    <span class="title text-center"><FONT SIZE="2"><B>SITUACION</B></font></span>
                  </td>
                  <td >
                    <span class="title text-center"><FONT SIZE="2"><B>VALOR</B></font></span>
                  </td>

                  <td>
                    <span class="title text-center"><FONT SIZE="2"><B>IVA</B></font></span>
                  </td>
                  <td>
                  </td>

                  <td >
                    <span class="title text-center"><FONT SIZE="2"><B>PROVEEDOR</B></font></span>
                  </td>
                  <td>
                    <span class="title text-center"><FONT SIZE="2"><B>SITUACION</B></font></span>
                  </td>
                  <td >
                    <span class="title text-center"><FONT SIZE="2"><B>VALOR</B></font></span>
                  </td>

                  <td >
                    <span class="title text-center"><FONT SIZE="2"><B>IVA</B></font></span>
                  </td>

                
                </tr>
                </thead>
     
              <tbody>
                <tr>
                      <td colspan="14">Sin informacion</td>
                    </tr>
              </tbody>
            </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

           </div> 


          </div> 
        </div>
        <!-- /.row (main row) -->
        <div>


      </div><!-- /.container-fluid -->
      
<script src="<?php echo base_url();?>/inc/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>/inc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url();?>/inc/plugins/chart.js/Chart.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>/inc/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>/inc/dist/js/demo.js"></script>
  <script>
  $('#Dashboard').attr('class','nav-link active');
  
  $(function () {



   

    //-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var donutChartCanvas2 = $('#donutChart2').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Positivo',
          'Pendiente',
          'Negativo',
      ],
      datasets: [
        {
          data: [<?php echo ($proveedoresPositivoInsumos);?>,<?php echo ($proveedoresSinRespuestaInsumos);?>,<?php echo ($proveedoresNegativoInsumos);?>],
          backgroundColor : ['#00a65a', '#f39c12', '#f56954'],
        }
      ]
    }

    var donutData2        = {
      labels: [
          'Positivo',
          'Pendiente',
          'Inconcistencias',
      ],
      datasets: [
        {
          data: [<?php echo ($proveedoresPositivoServicio);?>,<?php echo ($proveedoresSinRespuestaServicio);?>,<?php echo ($proveedoresNegativoServicio);?>],
          backgroundColor : ['#00a65a', '#f39c12', '#f56954'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var donutChart = new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })


    var donutChart2 = new Chart(donutChartCanvas2, {
      type: 'doughnut',
      data: donutData2,
      options: donutOptions
    })

    

  


     //-------------
    //- BAR CHART -
    //-------------
    var barChart = $('#totalesChart').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Emitidos',
          'Recibidos',
      ],
      datasets: [
        {
          data: [<?php echo $totalemitidos;?>,<?php echo $totalrecibidos;?>],
          backgroundColor : ['#f56954', '#00a65a'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var donutChart = new Chart(barChart, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })

    /*
			var barChart = document.getElementById('totalesChart').getContext('2d');
			window.myBar = new Chart(barChart, {
				type: 'bar',
        data: {
          labels: ["Emitidos", "Recibidos"],
          datasets: [
            {
              label: "Totales",
              backgroundColor: ["#3e95cd", "#8e5ea2"],
              data: [<?php echo $totalemitidos;?>,<?php echo $totalrecibidos;?>]
            }
          ]
        },
        options: {
          legend: { display: false },
          title: {
            display: true,
            text: 'Totales Emitidos/Recibidos'
          }
        }
			});
*/





     //-------------
    //- BAR CHART2 -
    //-------------
    var pieChartCanvas = $('#montosChart').get(0).getContext('2d')
    var donutData1        = {
      labels: [
          'Emitidos',
          'Recibidos',
      ],
      datasets: [
        {
          data: [<?php echo round($montoremitidos,2);?>,<?php echo round($montorecibidos,2);?>],
          backgroundColor : ['#f56954', '#00a65a'],
        }
      ]
    }
    var pieData        = donutData1;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })
    /*
			var barChart2 = document.getElementById('montosChart').getContext('2d');
			window.myBar = new Chart(barChart2, {
				type: 'bar',
          data: {
            labels: ["Emitidos", "Recibidos"],
            datasets: [
              {
                label: "Montos",
                backgroundColor: ["#3e95cd", "#8e5ea2"],
                data: [<?php echo $montoremitidos;?>,<?php echo $montorecibidos;?>]
              }
            ]
          },
          options: {
            legend: { display: false },
            title: {
              display: true,
              text: 'Montos Emitidos/Recibidos'
            }
          }
			});*/

      $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

  });
	

 
  })
</script>