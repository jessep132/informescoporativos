<head>
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 
 
</head>
<body class = 'background'>
  <div class = 'center'>
      <div class = 'mainBoard'>
        <div class  = 'topLeftRedLight'></div>
        <div class  = 'topRightRedLight'>
         
        <a href="<?php echo base_url();?>home"  style="text-decoration-line: none;color:white">
         <div class="first">
            <span class="text">
            Dashboard <br>(Global)


            </span>
          </div>
          </a>
          <a href="<?php echo base_url();?>proveedores/cfdi"  style="text-decoration-line: none;color:white">
          <div class="second">
            <span class="text">
            Facturas - CFDI´s
            </span>
          </div>
          </a>

          <a href="<?php echo base_url();?>proveedores/todos"  style="text-decoration-line: none;color:white">
           <div class="third">
            <span class="text">
            Opinión del Cumplimiento de Obligaciones Fiscales
            </span>
          </div>
          </a>

        </div>
        <div class  = 'buttomLeftRedLight'></div>
        <a href=""  style="text-decoration-line: none;color:white">
        <div class  = 'buttomRightRedLight'>
          <div class="fourth">
              <span class="text">
              Registro Público del Comercio
               </span>
          </div>
          </a>
          <a href=""  style="text-decoration-line: none;color:white">
           <div class="fifth">
            <span class="text">
            Constancia de Situación Fiscal
            </span>
          </div>
          </a>
          <a href="<?php echo base_url();?>proveedores/efos"  style="text-decoration-line: none;color:white">
           <div class="sixth">  <span class="text">
           Contribuyentes con operaciones presuntamente inexistentes
            </span>
          </div>
          </a>

          </div>


        </div>   
          <div class = 'middleMenu' >
            <div class = 'logoText'>
              <span class = 'textLogo'>
                <div style="margin-left: -101px;margin-top:-17px">
                 <img src="<?php echo base_url();?>/inc/logo.png" alt=""  width=""/>
               </div>
               </span>
            </div>
         </div>
      </div>
    <div align="right">
    <a href="<?php echo base_url();?>login/logout"  style="text-decoration-line: none;color:white">
    <button type="button" class="btn btn-danger">Salir</button>
    </a>
    </div>
  </div>
</body>




<style type="text/css">

  .first {
              border-top-left-radius: 60px;
              width: 164px;
    height: 170px;
    background-color: #e73a57;
    position: absolute;
    top: -293px;
    left: -309px;
    transform: rotate(28deg); z-index: 2; border-right: 10px solid white;
            }
            .first:hover, .second:hover, .third:hover, .fourth:hover, .fifth:hover, .sixth:hover {
             cursor: pointer;
             transition-property: all; 
             transition-duration: .5s;
             background-color: gray;
             /*border-right: 10px solid gray;*/ 
            } 

            .first .text {
              transform: rotate(-26deg);
              position: absolute;
              top: 64px;
              left: 44px;
              font-weight: 300;
              color: white;
              letter-spacing: 0px;
            }

            .second {
              width: 190px;
    height: 176px;
    background-color: #f5d300;
    position: absolute;
    top: -224px;
    left: -190px;
    transform: rotate(44deg);
    z-index: 1;
            }
            .second .text {
              transform: rotate(-45deg);
    position: absolute;
    top: 80px;
    left: 30px;
    font-weight: 300;
    color: white;
    letter-spacing: 0px;
            }
            .third {
              width: 120px;
              height: 250px;
              background-color: #5cb6f8;
              position: absolute;
              top: -145px;
              left: -160px;
              transform: rotate(80deg); z-index: 2; border-left: 10px solid white;
            }
            .third .text {
              transform: rotate(-80deg);
    position: absolute;
    top: 31px;
    left: -14px;
    font-weight: 300;
    color: white;
    letter-spacing: 0px;
            }
            .fourth {
              width: 150px;
    height: 172px;
    background-color: #e73a57;
    position: absolute;
    top: 129px;
    left: -307px;
    transform: rotate(-30deg);
    z-index: 3; border-right: 10px solid white; border-bottom-left-radius: 70px;
            }
            .fourth .text {
              transform: rotate(28deg);
    position: absolute;
    top: 74px;
    left: 44px;
    font-weight: 300;
    color: white;
    letter-spacing: 0px;
            }
            .fifth {
              width: 145px;
    height: 172px;
    background-color: #3f51b5;
    position: absolute;
    top: 78px;
    left: -196px;
    transform: rotate(-57deg);
    z-index: 2; border-right: 10px solid white;
            }
            .fifth .text {
              transform: rotate(57deg);
    position: absolute;
    top: 74px;
    left: 33px;
    font-weight: 300;
    color: white;
    letter-spacing: 0px;
            }
            .sixth {
               width: 150px;
    height: 172px;
    background-color: #82d32e;
    position: absolute;
    top: -14px;
    left: -143px;
    transform: rotate(-88deg);
    z-index: 1;
    border-right: 10px solid white;
            }
            .sixth .text {
              transform: rotate(87deg);
    position: absolute;
    top: 74px;
    left: 36px;
    font-weight: 300;
    color: white;
    letter-spacing: 0px;
            }




  .center{
   position: relative;
   margin: auto;
   width: 50%;
}
 

.mainBoard{
  width: 600px;
  height: 600px;
  background-color: white;
  border-radius: 300px;
  box-shadow: 0px 0px 0px black;
  overflow: hidden;
  position: absolute;
}


.topLeftRedLight {
  border-style: solid;
  border-color: white; z-index: 9;
  border-width: 275px 275px 0px 0px;
  border-top-left-radius: 275px;
  position: absolute;
  left: 20px;
  top: 20px;
}

.topLeftRedLight:after {
  top: -155px;
  left: 120px;
  border-style: solid;
  border-color: white;
  border-width: 155px 155px 0px 0px;
  border-top-left-radius: 155px;
  position: absolute;
  content: "";
}

.topRightRedLight {
  position: absolute;
  top: 20px;
  left: 305px;
  width: 0;
  height: 0;
  /*border-top: 275px solid red;
  border-left: 275px solid transparent;*/
   border-style: solid;
   border-color: red;
   border-width: 275px 0px 0px 275px;
  border-top-right-radius: 275px;
}

.topRightRedLight:after {
  top: -155px;
  left: -275px;
  /*border-top: 155px solid brown;
  border-left: 155px solid transparent;
  border-top-right-radius: 155px;*/
   border-style: solid;
   border-color: white;
   border-width: 155px 0px 0px 155px;
  border-top-right-radius: 155px;
  position: absolute;
  content: "";
}

.buttomLeftRedLight {
  position: absolute;
  z-index: 9; 
  top: 305px;
  left: 20px;
  width: 0;
  height: 0;
  border-style: solid;
  border-color: white;
  border-width: 0px 275px 275px 0px;
  border-bottom-left-radius: 275px;
}

.buttomLeftRedLight:after {
  top: 0px;
  left: 120px;
  border-style: solid;
  border-color: white;
  border-width: 0px 155px 155px 0px;
  border-bottom-left-radius: 155px;
  position: absolute;
  content: "";
}

.colorDiv:hover{
  cursor: pointer;  
  opacity: 0.5;
}

.buttomRightRedLight {
  position: absolute;
  top: 305px;
  left: 305px;
  width: 0;
  height: 0;
  border-style: solid;
  border-color: blue;
  z-index: 2;
  /*border-bottom: 275px solid blue;
  border-left: 275px solid transparent;
  border-widht: 0px*/
  border-width: 0px 0px 275px 275px;
  border-bottom-right-radius: 275px;
}

.buttomRightRedLight:after {
  top: 0px;
  left: -275px;
  border-style: solid;
  border-color: brown;
   border-width: 0px 0px 155px 155px;
  border-bottom-right-radius: 155px;
  position: absolute;
  content: "";
}

.middleMenu{
  position: absolute;
  top: 150px;
  left: 150px;
  width: 300px;
  height: 300px;
  border-radius: 300px; 
  font-size: 30px; 
  background-color: #e3e4e8; 
  z-index: 99; 
  border: 30px solid white;
  position: absolute;
    top: 150px;
    left: 150px;
    width: 300px;
    height: 300px;
    border-radius: 300px;
    font-size: 30px;
    background-color: #e3e4e8;
    z-index: 99;
    border: 30px solid white;
}

.sign{
  font-size: 15px;  
}

.logoText{
  position: absolute;
  top: 70px;
  left: 55px;
   width: 203px;
    padding-top: 77px;
    padding-left: 10px;
    height: 202px;
    background: #e3e4e8;
    border: 1.5px solid white;
    border-radius: 50%;
    top: 20px;
    left: 20px;
    text-align: center;
}

.countArea{
  position: absolute;
  top: 130px;
  left: 30px;
}

.startButtonArea{
  position: absolute;
  top: 140px;
  left: 130px;
}

.strictButtonArea{
  position: absolute;
  top: 108px;
  left: 230px;
}

.onoffButtonArea{
  position: absolute;
  top: 230px; 
  left: 102px;
}

.switch {
  position: relative;
  display: inline-block;
  width: 41px;
  height: 22px;
}

.spanShift{
  position: relative;
  top: -12px;
  font-size: 14px;
  font-family: "Comic Sans MS", cursive, sans-serif;
  color: black;
}


.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: black;
}

.slider:before {
  position: absolute;
  content: "";
  height: 20px;
  width: 13px;
  left: 1px;
  bottom: 1px;
  background-color: lightblue;
  -webkit-transition: .7s;
  transition: .7s;
}

input:checked + .slider {
  background-color: black;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

#countInput{
  width: 38px;
  font-size: 17px;
  fount-weight: bold;
  color: #f7a8a8;
  background: red;
  border: solid black 3px;
  border-radius: 3px;
  text-align: center;
}

.countText{
  font-size: 14px;
  color: black;
  font-family: "Comic Sans MS", cursive, sans-serif;
}

#startButton{
  width: 30px;
  height: 30px;
  border: 2px solid black;
  border-radius: 25px;
  background-color: red;
}

.startText{
  margin-top: 5px;
  font-size: 14px;
  color: black;
  font-family: "Comic Sans MS", cursive, sans-serif;
}

#strictButtonSmall{
  width: 15px;
  height: 15px;
  border: 2px solid black;
  border-radius: 10px;
}

.greyColor{
  background-color: grey;
}

.redColor{
  background-color: red;
}

 

.strictText{
  position: relative;
  top: 37px;
  left: -9px;
  font-size: 14px;
  color: black;
  font-family: "Comic Sans MS", cursive, sans-serif;
}

.opa50{
  opacity: 0.5;
}
</style>