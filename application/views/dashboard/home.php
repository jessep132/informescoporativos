 <style>
.card-primary:not(.card-outline)>.card-header {
    background-color: #CAC7C8;
}
 </style>
 
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
          <a href="<?php echo base_url();?>home/menu" class="brand-link">
                <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >

              </a>
           
          </div><!-- /.col -->
          <div class="col-sm-6" align="center">
             <!-- Brand Logo -->
           
          </div><!-- /.col -->
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
             <a href="<?php echo base_url();?>home/menu"  >
              <button type="button" class="btn btn-primary btn-sm"> << Regresar</button>
             </a>
            </div>
          </div>

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<div class="container-fluid">
<div class="row">
      <div class="col-md-4">
      &nbsp;&nbsp;<B>Opinión de Cumplimiento Obligaciones Fiscales</B>
        <br>
        <div class="row">
          <!---INICIO-->
          <div class="col-lg-12 col-6">
            <!-- small box  style="height: 60px;" style="background-color: #17a2b8!important"-->
            <div class="small-box" style="background-color: #016CC0!important;color:#ffffff">
              <div class="inner">
                <h6>Total:&nbsp;&nbsp; <?php echo ($proveedores);?> </h6>
                
               
              </div>
              <div class="icon">
                
                <i class="ion ion-person"></i>
              </div>
              <!--
              <a href="<?php echo base_url();?>proveedores/todos" class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <!---FIN---->
          
          <!---INICIO-->
          <div class="col-lg-12 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #0385EA!important;color:#ffffff">
              <div class="inner">
                <h6>Cumplimiento Positivo:&nbsp;&nbsp;<?php echo ($proveedoresPositivo);?>  
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                 <B><FONT SIZE=5> 
                <?php 
                        $prov1 = $proveedoresPositivo * 100;
                        if($prov1 > 0){
                          $porcentaje1 = $prov1/$proveedores;
                        }else{
                          $porcentaje1 = 0;
                        }
                       

                        echo  round($porcentaje1,2).' %';
                ?>
                </B></FONT></h6>

                
              </div>
              <div class="icon">
                <i class="far "></i>
              </div>
               <!--
              <a href="<?php echo base_url();?>proveedores/positivo"  class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>


          <!---FIN---->
          <!---INICIO-->
          <div class="col-lg-12 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #2790E2!important;color:#B3B7BC">
              <div class="inner">
              <h6 style="color:white;">Sin Respuesta Portal SAT / Proveedor :&nbsp;&nbsp;<?php echo ($proveedoresSinRespuesta);?>
              &nbsp; &nbsp; &nbsp; <B><FONT SIZE=5> 
              <?php 

                        $prov2 = $proveedoresSinRespuesta * 100;
                        if($prov2>0){
                          $porcentaje2 = $prov2/$proveedores;
                        }else{
                          $porcentaje2 = 0;
                        }
                        

                        echo  round($porcentaje2,2).' %';
                ?></FONT></B>
              </h6>

              
              </div>
         
              
               
               <!--
              <a href="<?php echo base_url();?>proveedores/sinrespuesta"  class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <!-- ./col -->


          <!---FIN---->
          <!---INICIO-->
          <div class="col-lg-12 col-6">
            <!-- small box -->
            <div class="small-box bg-danger" style="background-color: #49A3EA!important;color:#B3B7BC">
              <div class="inner">
              <h6>Opinión del cumplimiento de obligaciones fiscales con inconsistencias : &nbsp;&nbsp;<?php echo ($proveedoresNegativo);?>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <B><FONT SIZE=5>
              <?php 
                        $prov3 = $proveedoresNegativo * 100;
                        if($prov3>0){
                          $porcentaje3 = $prov3/$proveedores;
                        }else{
                          $porcentaje3 = 0;
                        }
                    

                        echo  round($porcentaje3,2).' %';
                ?></FONT></B>
              </h6>
                
              </div>

               <!--
              <a href="<?php echo base_url();?>proveedores/negativo" class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <!-- ./col -->
          <!---FIN---->

          <!---INICIO-->
          <div class="col-lg-12 col-6">
            <!-- small box  style="height: 60px;" style="background-color: #17a2b8!important"--> 
            <div class="small-box" style="background-color: #7AC4FF!important;color:#ffffff">
              <div class="inner">
                <h6>Proveedores Inactivos:&nbsp;&nbsp; <?php echo ($proveedoresInactivos);
                ?> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              
                <B><FONT SIZE=5>
              <?php   
                  if($proveedores > 0){
                    $inactivosPor = ($proveedoresInactivos * 100)/$proveedores;  
                    echo  round($inactivosPor,2).' %';
                  }else{
                    echo '0%';
                  }
                  
                ?></FONT></B>
                </h6>
                
               
              </div>
              <div class="icon">
                
                <i class="ion ion-person"></i>
              </div>
              <!--
              <a href="<?php echo base_url();?>proveedores/todos" class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <!---FIN---->

           <!---INICIO-->
           <div class="col-md-6">
             <!-- DONUT CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Otros</h3>

                <div class="card-tools">
                 
                 
                </div>
              </div>
              <div class="card-body">
                <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

           </div> 
             <!---FIN---->
             <!---INICIO-->
             <div class="col-md-6">
            <!-- DONUT CHART -->
           <div class="card card-primary">
             <div class="card-header">
               <h3 class="card-title">Servicios</h3>

               <div class="card-tools">
                
               </div>
             </div>
             <div class="card-body">
               <canvas id="donutChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
             </div>
             <!-- /.card-body -->
           </div>
           <!-- /.card -->

          </div>

        
              <!---FIN---->
              &nbsp;&nbsp;<B>Constancia de Situación Fiscal</B>
           <div class="col-md-12">
              <div class="small-box" style="background-color: #016CC0!important;color:#ffffff;font-size:25px;">
                  <div class="inner">
                    <h6 >Total:&nbsp;&nbsp; <?php echo ($proveedores2c);?> <br><br></h6>
                    
                  
                  </div>
                  <!--
                  <a href="<?php echo base_url();?>proveedores/todos" class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
                  -->
                </div>
            </div>
            <div class="col-md-12">
            <div class="small-box" style="background-color: #2790E2!important;color:#ffffff">
              <div class="inner">
                <h6 >Recibida :&nbsp;&nbsp;<?php echo ($proveedoresPositivo);?>  
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><FONT SIZE=5>
                <?php 
                        $prov1c = $proveedoresPositivoc * 100;
                        if($prov1c > 0){
                          $porcentaje1c = $prov1c/$proveedores2c;
                        }else{
                          $porcentaje1c = 0;
                        }
                        echo  round($porcentaje1c,2).' %';
                ?>
                 </FONT></B> <br><br></h6>

                
              </div>
              <div class="icon">
                <i class="far "></i>
              </div>
               <!--
              <a href="<?php echo base_url();?>proveedores/positivo"  class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
              
            </div>
            </div>
            <div class="col-md-12">
            <div class="small-box" style="background-color: #49A3EA!important;color:#B3B7BC">
              <div class="inner">
              <h6 style="color:white;">Sin Respuesta  Proveedor :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ($proveedoresSinRespuestac);?>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><FONT SIZE=5>
              <?php 
                        $prov2c = $proveedoresSinRespuestac * 100;
                        if($prov2c > 0){
                          $porcentaje2c = $prov2c/$proveedores2c;
                        }else{
                          $porcentaje2c = 0;
                        }
                        

                        echo  round($porcentaje2c,2).' %';
                ?></FONT></B>
                <br><br>
              </h6>

              
              </div>
         
              
               
               <!--
              <a href="<?php echo base_url();?>proveedores/sinrespuesta"  class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>

            </div>






        </div>
      </div>
      <div class="col-md-4">
      &nbsp;&nbsp;<B>Facturas - CFDI´s </B>
      <div class="row">

       <!------INICIO-------->
       <div class="col-md-12">
             <!-- DONUT CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Proveedores</h3>

                <div class="card-tools">
               
                 
                </div>
              </div>
              <div class="card-body">


              <canvas id="totalesChartProve" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

           </div>  
            <!------INICIO-------->
            <div class="col-md-12">
             <!-- DONUT CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Total  actualizado al dia <?php echo date('d-m-y'); ?></h3>

                <div class="card-tools">
               
                 
                </div>
              </div>
              <div class="card-body">
              <B>Emitidos:</B>  <?php echo $totalemitidos;?>   
              <br>
              <B>Recibidos:</B> <?php echo $totalrecibidos;?>

              <canvas id="totalesChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

           </div>  
 <!------INICIO-------->
      <div class="col-md-12">
                  <!-- DONUT CHART -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title"> Por Tipo de Operación (MXN)</h3>

                    <div class="card-tools">
                      
                    </div>
                  </div>
                  <div class="card-body">
                  <B>Emitidos:</B>  $<?php echo number_format($montoremitidos,2);?>  MXN 
                    <br>
                    <B>Recibidos:</B> $<?php echo number_format($montorecibidos,2);?> MXN
                  <canvas id="montosChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->

                </div> 






                <div class="col-md-12">
                  <!-- DONUT CHART -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title"> Por Tipo de Operación (USD)</h3>

                    <div class="card-tools">
                      
                    </div>
                  </div>
                  <div class="card-body">
                  <B>Emitidos:</B>  $<?php echo "0.00";?>  USD 
                    <br>
                    <B>Recibidos:</B> $<?php echo "0.00";?> USD
                  <canvas id="montosChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->

                </div> 








            </div>









      </div>
      <div class="col-md-4">
      <B> Contribuyentes con Operaciones Presuntamente Inexistentes</B>
      <div class="row">
             <div class="col-md-12">
                 <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">EFOS actualizado al dia <?php  echo date('d-m-Y',strtotime($efosActualiza['efosActualiza']));?></h3>
                    </div>
                    <div class="card-body">

                    <table id="example1"  class="table table-bordered table-striped"> 
                    <thead>
                      <tr  >
                        <td colspan="8" >
                          <span class="title text-center"><B><?php echo $mesAnp;?></B></span>
                        </td>
              
                      <tr  >
                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>PROVEEDOR</B></font></span>
                        </td>
                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>SITUACIÓN</B></font></span>
                        </td>
                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>VALOR</B></font></span>
                        </td>

                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>IVA</B></font></span>
                        </td>
       
        
                      </tr>
                      </thead>
          
                    <tbody>
                    <tr>
                            <?php echo $proveedoresAnteriorPre;?>

                          </tr>
                    </tbody>

                    <thead>
                      <tr  >
                        <td colspan="4" >
                          <span class="title text-center"><B><?php echo $mesAn;?></B></span>
                        </td>

              
                      <tr  >
                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>PROVEEDOR</B></font></span>
                        </td>
                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>SITUACIÓN</B></font></span>
                        </td>
                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>VALOR</B></font></span>
                        </td>

                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>IVA</B></font></span>
                        </td>
        
        
                      </tr>
                      </thead>
          
                    <tbody>
                    <tr>
                         <?php echo $proveedoresAnterior;?>
                          </tr>
                    </tbody>



                    <thead>
                      <tr  >
                        <td colspan="4" >
                          <span class="title text-center"><B><?php echo $mesA;?></B></span>
                        </td>
              
                        
              
                      <tr  >
                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>PROVEEDOR</B></font></span>
                        </td>
                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>SITUACIÓN</B></font></span>
                        </td>
                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>VALOR</B></font></span>
                        </td>

                        <td >
                          <span class="title text-center"><FONT SIZE="2"><B>IVA</B></font></span>
                        </td>
   
        
                      </tr>
                      </thead>
          
                    <tbody>
                      <tr>
                      <?php echo $proveedoresmesActual;?>

                          </tr>
                    </tbody>


                    



                  </table>

                    </div>

                 </div>
             </div>

      </div>
      </div>

</div>
  
</div><!-- /.container-fluid -->
      
<script src="<?php echo base_url();?>/inc/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>/inc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<!--
<script src="<?php echo base_url();?>/inc/plugins/chart.js/Chart.min.js"></script>
-->
<script src="<?php echo base_url();?>/inc/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<script src="<?php echo base_url();?>/inc/chart/samples/dist/Chart.min.js"></script>
<script src="<?php echo base_url();?>/inc/chart/samples/utils.js"></script>



<!-- AdminLTE App -->
<script src="<?php echo base_url();?>/inc/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>/inc/dist/js/demo.js"></script>
  <script>
  $('#Dashboard').attr('class','nav-link active');
  
  $(function () {

    //-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var donutChartCanvas = $('#donutChart').get(0).getContext('2d');
    var donutChartCanvas2 = $('#donutChart2').get(0).getContext('2d');
    var donutData        = {
      labels: [
          'Positivo',
          'Pendiente',
          'Inconsistencias',
      ],
      datasets: [
        {
          data: [<?php echo ($proveedoresPositivoInsumos);?>,<?php echo ($proveedoresSinRespuestaInsumos);?>,<?php echo ($proveedoresNegativoInsumos);?>],
          backgroundColor : ['#00a65a', '#f39c12', '#f56954'],
        }
      ]
    }

    var donutData2        = {
      labels: [
          'Positivo',
          'Pendiente',
          'Inconsistencias',
      ],
      datasets: [
        {
          data: [<?php echo ($proveedoresPositivoServicio);?>,<?php echo ($proveedoresSinRespuestaServicio);?>,<?php echo ($proveedoresNegativoServicio);?>],
          backgroundColor : ['#00a65a', '#f39c12', '#f56954'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
      plugins: {
            datalabels: {
                formatter: function(value, context) {
                    return context.chart.data.labels[context.dataIndex];
                }
            }
        }
      
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var donutChart = new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions,
      
    })


    var donutChart2 = new Chart(donutChartCanvas2, {
      type: 'doughnut',
      data: donutData2,
      options: donutOptions
    })

    

  


     //-------------
    //- BAR CHART -
    //-------------
    var barChart = $('#totalesChart').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Emitidos',
          'Recibidos' ,
      ],
      datasets: [
        {
          data: [<?php echo $totalemitidos;?>,<?php echo $totalrecibidos;?>],
          backgroundColor : ['#f56954', '#00a65a'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var donutChart = new Chart(barChart, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })

    /*
			var barChart = document.getElementById('totalesChart').getContext('2d');
			window.myBar = new Chart(barChart, {
				type: 'bar',
        data: {
          labels: ["Emitidos", "Recibidos"],
          datasets: [
            {
              label: "Totales",
              backgroundColor: ["#3e95cd", "#8e5ea2"],
              data: [<?php echo $totalemitidos;?>,<?php echo $totalrecibidos;?>]
            }
          ]
        },
        options: {
          legend: { display: false },
          title: {
            display: true,
            text: 'Totales Emitidos/Recibidos'
          }
        }
			});
*/





     //-------------
    //- BAR CHART2 -
    //-------------
    var pieChartCanvas = $('#montosChart').get(0).getContext('2d')
    var donutData1        = {
      labels: [
          'Emitidos' ,
          'Recibidos',
      ],
      datasets: [
        {
          data: [<?php echo round($montoremitidos,2);?>,<?php echo round($montorecibidos,2);?>],
          backgroundColor : ['#f56954', '#00a65a'],
        }
      ]
    }
    var pieData        = donutData1;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })
    /*
			var barChart2 = document.getElementById('montosChart').getContext('2d');
			window.myBar = new Chart(barChart2, {
				type: 'bar',
          data: {
            labels: ["Emitidos", "Recibidos"],
            datasets: [
              {
                label: "Montos",
                backgroundColor: ["#3e95cd", "#8e5ea2"],
                data: [<?php echo $montoremitidos;?>,<?php echo $montorecibidos;?>]
              }
            ]
          },
          options: {
            legend: { display: false },
            title: {
              display: true,
              text: 'Montos Emitidos/Recibidos'
            }
          }
			});*/





       //-------------
    //- BAR CHART22 -
    //-------------
    var pieChartCanvas2 = $('#montosChart2').get(0).getContext('2d')
    var donutData12        = {
      labels: [
          'Emitidos' ,
          'Recibidos',
      ],
      datasets: [
        {
          data: [0,0],
          backgroundColor : ['#f56954', '#00a65a'],
        }
      ]
    }
    var pieData        = donutData12;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas2, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })
    /*
			var barChart2 = document.getElementById('montosChart').getContext('2d');
			window.myBar = new Chart(barChart2, {
				type: 'bar',
          data: {
            labels: ["Emitidos", "Recibidos"],
            datasets: [
              {
                label: "Montos",
                backgroundColor: ["#3e95cd", "#8e5ea2"],
                data: [<?php echo $montoremitidos;?>,<?php echo $montorecibidos;?>]
              }
            ]
          },
          options: {
            legend: { display: false },
            title: {
              display: true,
              text: 'Montos Emitidos/Recibidos'
            }
          }
			});*/


//-------------
    //- BAR CHART22 -
    //-------------
    var pieChartCanvas23 = $('#totalesChartProve').get(0).getContext('2d')
    var donutData12        = {
      labels: [
          'Proveedores sin CFDI´s' ,
          'Proveedores con CFDI´s'
      ],
      datasets: [
        {
          data: [<?php echo ($proveedores_sin_cfdi);?>,<?php echo ($proveedores_con_cfdi);?>],
          backgroundColor : ['#00a65a','#f56954'],
        }
      ]
    }
    var pieData        = donutData12;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas23, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })
    /*
			var barChart2 = document.getElementById('montosChart').getContext('2d');
			window.myBar = new Chart(barChart2, {
				type: 'bar',
          data: {
            labels: ["Emitidos", "Recibidos"],
            datasets: [
              {
                label: "Montos",
                backgroundColor: ["#3e95cd", "#8e5ea2"],
                data: [<?php echo $montoremitidos;?>,<?php echo $montorecibidos;?>]
              }
            ]
          },
          options: {
            legend: { display: false },
            title: {
              display: true,
              text: 'Montos Emitidos/Recibidos'
            }
          }
			});*/

	

 
  })
</script>