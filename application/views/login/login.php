<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SAT-Proveedores</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>/inc/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url();?>/inc/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>/inc/dist/css/adminlte.min.css">
</head>
<!DOCTYPE html>
<html>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
  <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo" width="100%">
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">

      <form name="loginInformes" id="loginInformes"  action="<?php echo base_url();?>login/valid"   method="post">
        <div class="input-group mb-3">
          <input type="email" class="form-control" name="usuarios" id="usuarios" placeholder="Usuario">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control"  name="password" id="password" placeholder="Contraseña">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        </form>
        <div class="row">
        
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block" onclick="ingresarLogin();">Ingresar</button>
          </div>
          <!-- /.col -->
          <?php if($error != ''){?>
          <div class="col-12">
          <br>
          <div class="alert alert-success" role="alert">
            Contraseña Restaurada con Exito!!
          </div>
          </div>
          <?php }?>
          <br><br><br><br>
          <div class="col-6">
          Versión 1.0
          </div>

          <div class="col-6">
          <a href="<?php echo base_url();?>login/recuperaPass">Restaurar contraseña</a>
          </div>
          
        </div>

       
        <!--
        <br><br><br><br>
        <p class="mb-1">
        <a href="<?php echo base_url();?>login/subir32D" >Carga cumplimiento 32D CFF</a>
        </p>
        -->
     

     
      <!-- /.social-auth-links -->

    
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
<script>
        function ingresarLogin(){
            if($('#usuarios').val() == ""){
                alertify.alert('Ingresa un usuario', function(){ location.reload(true);});
                return false;
            }


            if($('#usuarios').val() != "" ){
                $('#loginInformes').submit();
            }

        }
</script>
<!-- jQuery -->
<script src="<?php echo base_url();?>/inc/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>/inc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>/inc/dist/js/adminlte.min.js"></script>
</body>
</html>




















