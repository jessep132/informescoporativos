<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SAT-Proveedores</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/inc/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/inc/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/inc/dist/css/adminlte.min.css">
</head>
<!DOCTYPE html>
<html>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <img src="<?php echo base_url(); ?>/inc/logo.png" alt="AdminLTE Logo" width="100%">
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">

        <form name="loginInformes" id="loginInformes" action="<?php echo base_url(); ?>login/validvalidateSatCompliance" method="post">
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Ingresa tu RFC</label>
              <input type="text" class="form-control" name="rfc" id="rfc" placeholder="RFC">
            </div>

            <div class="form-group">
              <label for="month_date">Mes</label>
              <input type="number" class="form-control" name="document_month" id="document_month" placeholder="MM">
            </div>
            <div class="form-group">
              <label for="year_date">Año</label>
              <input type="number" class="form-control" name="document_year" id="document_year" placeholder="YYYY">
            </div>

            <input type="hidden" name="document_base64" id="document_base64" />

            <div class="form-group">
              <label for="exampleInputFile">Adjunta tu archivo de cumplimiento</label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="document" name="document">
                  <label class="custom-file-label" for="exampleInputFile"></label>
                </div>
              </div>
            </div>

          </div>
        </form>
        <!-- /.col -->
        <div class="col-12">
          <button type="submit" class="btn btn-primary btn-block" onclick="subir();">Subir</button>
        </div>
        <!-- /.col -->




        <!-- /.social-auth-links -->


      </div>
      <!-- /.login-card-body -->
    </div>
  </div>
  <!-- /.login-box -->
  <script src="<?php echo base_url(); ?>/inc/scripts/sat_compliance.js"></script>
  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>/inc/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url(); ?>/inc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url(); ?>/inc/dist/js/adminlte.min.js"></script>
</body>

</html>