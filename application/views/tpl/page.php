<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ZILA-MODULE</title>

<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url();?>/inc/plugins/fontawesome-free/css/all.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url();?>/inc/dist/css/adminlte.min.css">



</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color: #ffff;">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:100%" >

    </a>

    <!-- Sidebar -->
    <div class="sidebar" >
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="<?php echo base_url();?>home" class="nav-link" id="Dashboard" style="background-color: #0CCD9E;">
              <i class="fas fa-tachometer-alt"></i>
              <p>
                Dashboard(Global)
              </p>
            </a>
          </li>
          <li class="nav-item menu-open">
          <a href="<?php echo base_url();?>proveedores/cfdi" class="nav-link " id="cfdi" style="background-color: #DBD813;">
              <i class="fas fa-file-alt"></i>
              <p>
                Facturas - CFDI´s
              </p>
            </a>
          </li>

          <li class="nav-item menu-open">
            <a href="<?php echo base_url();?>proveedores/todos" class="nav-link " id="Proveedores2" style="background-color: #13DBC6;">
              <i class="far fa-address-card"></i>
              <p>
              Opinión del Cumplimiento de Obligaciones Fiscales
              </p>
            </a>
          </li>

          <li class="nav-item menu-open">
            <a href="<?php echo base_url();?>proveedores/efos" class="nav-link " id="Efos"  style="background-color: #1FDB13;">
              <i class="fas fa-book"></i>
   
              <p>
              Opinión del Cumplimiento deObligaciones Fiscales
              </p>
            </a>
          </li>
          <li class="nav-item menu-open">
          <br>
          </li>
          <li class="nav-item menu-open">
          <br>
          </li>
          <li class="nav-item menu-open">
          <br>
          </li>

        
            <li class="nav-item menu-open">
            <a href="<?php echo base_url();?>login/logout" class="nav-link " id="Efos"  style="background-color: #DB1313;">

   
              <p>
                Salir
              </p>
            </a>
          </li>

        

        </ul>
       
       
       
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <?php if (isset($subview)) {
        $this->load->view($subview);
   } ?>
  </div>
   <!-- /.content -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
