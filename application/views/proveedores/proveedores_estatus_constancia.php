 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
              <a href="<?php echo base_url();?>home/menu" class="brand-link">
                <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >

              </a>
          </div><!-- /.col -->
          <div class="col-sm-6"  align="center">
            <h4><B> Constancia de Situación Fiscal</B> </h4>
          </div><!-- /.col -->
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
             <a href="<?php echo base_url();?>home/menu"  >
              <button type="button" class="btn btn-primary btn-sm"><< Regresar</button>
             </a>
            </div>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
</div>



<div class="container-fluid">

<div class="row">

        <?php if(isset($mostratTodos)){?>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #016CC0!important;color:#ffffff;font-size:25px;">
              <div class="inner">
                <h6 >Total:&nbsp;&nbsp; <?php echo ($proveedores2);?> <br><br></h6>
                
               
              </div>
              <!--
              <a href="<?php echo base_url();?>proveedores/todos" class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <?php } ?>
          <!-- ./col -->
          <?php if(isset($mostratTodos)){?>
          <div class="col-lg-3 col-6">
          <div class="small-box" style="background-color: #2790E2!important;color:#ffffff">
              <div class="inner">
                <h6 >Recibida :&nbsp;&nbsp;<?php echo ($proveedoresPositivo);?>  
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><FONT SIZE=5>
                <?php 
                        $prov1 = $proveedoresPositivo * 100;
                        if($prov1 > 0){
                          $porcentaje1 = $prov1/$proveedores2;
                        }else{
                          $porcentaje1 = 0;
                        }
                        echo  round($porcentaje1,2).' %';
                ?>
                 </FONT></B> <br><br></h6>

                
              </div>
              <div class="icon">
                <i class="far "></i>
              </div>
               <!--
              <a href="<?php echo base_url();?>proveedores/positivo"  class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
              
            </div>
         
            
          </div>
          <?php } ?>
          <!-- ./col -->
          <?php if(isset($mostratTodos)){?>
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #49A3EA!important;color:#B3B7BC">
              <div class="inner">
              <h6 style="color:white;">Sin Respuesta  Proveedor :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ($proveedoresSinRespuesta);?>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><FONT SIZE=5>
              <?php 
                        $prov2 = $proveedoresSinRespuesta * 100;
                        if($prov2 > 0){
                          $porcentaje2 = $prov2/$proveedores2;
                        }else{
                          $porcentaje2 = 0;
                        }
                        

                        echo  round($porcentaje2,2).' %';
                ?></FONT></B>
                <br><br>
              </h6>

              
              </div>
         
              
               
               <!--
              <a href="<?php echo base_url();?>proveedores/sinrespuesta"  class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <?php } ?>
          <!-- ./col -->

          </div>
       

        <div class="row">
        <div class="col-2" >            
              <div style="margin-left:10px;">
              <a  href="<?php echo base_url();?>proveedores/historicoConstancia" class="nav-link ">
              <button type="button"  class="btn btn-block btn-secondary">Historico</button>
              </a>
              </div>
          </div>

          <div class="col-12">
            <!-- /.card-header -->
              <div class="">
                <div class="col-md-12">
                  <div class="card">
                    <div class="">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr class=" text-center">
                       
                            <th>Razón Social</th>
                            <th>RFC</th>
                            <th>Fecha Recepción </th>
  
                            <th>Tipo Proveedor</th>
                            <th>Situación Fiscal</th>
                            <th >Documento</th>
            
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($proveedores as $key => $value) {
                          ?>
                          
                          <tr>
                            <td><?php echo $value['RazonSocial']?></td>
                            <td><?php echo $value['RFC']?></td>
                            <td><?php echo date('d-m-Y' ,strtotime($value['fecha_constancia2']));?></td>
                            <td><?php echo $value['tipoProveedor']?></td>
                            <?php if($value['constancia2'] == '1'){?>
                            <td>
                            Recibida
                            </td>
              
                          <?php }else{?>
                            <td>
                            Sin Respuesta Proveedor
                            </td>

                          <?php }?>

                          <td>
                          <?php 
                            if($value['constancia2'] == 1){
                            ?>
                             <div class="icon" align="center" style="cursor:pointer">
                             <img src="<?php echo base_url();?>/inc/icono-pdf.png" alt="icono-pdf"  style="width:40%" >
                               </div>

                               <?php
                            }else{  
                            ?>

                            <div class="icon" align="center" style="">
                             <img src="<?php echo base_url();?>/inc/iconoPdfNegro.jpg" alt="icono-pdf"  style="width:40%" >
                               </div>
                            <?php }?>
                            </td>

                            
                         
                          
                          </tr>
                          <?php }?>
                        
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                    <!--
                    <div class="card-footer clearfix">
                      <ul class="pagination pagination-sm m-0 float-right">
                        <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                      </ul>
                    </div>
                    -->


                  </div>
                  <!-- /.card -->
      
                  <!-- /.card -->
                </div>
      

          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>

<script src="<?php echo base_url();?>/inc/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url();?>/inc/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script>
  $('#Proveedores2').attr('class','nav-link active');
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print"],
      "autoWidth": false,
      "columnDefs": [
        { "width": "300px", "targets": 0 },
        { "width": "300px", "targets": 1 },
        { "width": "300px", "targets": 2 },
        { "width": "300px", "targets": 3 },
        { "width": "300px", "targets": 4 },
        { "width": "10px", "targets": 5 }
        ]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

  });

</script>
