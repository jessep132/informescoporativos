 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
              <a href="<?php echo base_url();?>home/menu" class="brand-link">
                <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >
              </a>
          </div><!-- /.col -->
          <div class="col-sm-6" align="center">
            <h4 ><B>Facturas - CFDI´s</B></h4>
          </div><!-- /.col -->
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
             <a href="<?php echo base_url();?>home/menu"  >
              <button type="button" class="btn btn-primary btn-sm"><< Regresar</button>
             </a>
            </div>
          </div>

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
     <!-- page plugins css -->
     <link rel="stylesheet" href="<?php echo base_url();?>/inc/bower_components/selectize/dist/css/selectize.default.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/inc/bower_components/bootstrap-daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/inc/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/inc/bower_components/summernote/dist/summernote.css" />



  <section class="content">
    <div class="col-md-12">
    <form name="proveedorForm" id="proveedorForm"  action="<?php echo base_url();?>proveedores/cfdi"   method="post">
    <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                        <label for="exampleInputEmail1">Fecha Inicio</label>
                        <input type="text"class="form-control datepicker-1" value="<?php echo $fechaInicio;?>" placeholder="Fecha inicio dd/mm/yyyy" data-date-format="dd/mm/yyyy" data-provide="datepicker" id="fecha1" name="fecha1"  placeholder="Fecha Inicio" autocomplete="off">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                        <label for="exampleInputEmail1">Fecha Fin</label>
                        <input type="text" class="form-control datepicker-1" value="<?php echo $fechaFin;?>" placeholder="Fecha Fin dd/mm/yyyy" data-date-format="dd/mm/yyyy" data-provide="datepicker" id="fecha2" name="fecha2" placeholder="Fecha Fin" autocomplete="off">
                </div>
            </div>
            <div class="col-md-4">
            <div class="form-group">
                  <label for="exampleSelectBorder">Tipo de Comprobante</code></label>
                  <select class="custom-select form-control-border" id="tipoComprobante" name="tipoComprobante">
                    <option value="">Elige</option>
                    <option value="E">Emitidos</option>
                    <option value="R">Recibidos</option>
                  </select>
                </div>
            </div>

           
           
        </div>

    </form>
    <div class="row">
            <div class="col-5">    
            </div>
            <div class="col-2">
                <button type="submit" class="btn btn-success btn-block" onclick="guardar();">Consultar</button>
            </div>
            <div class="col-5">    
            </div>
        </div>
    </div>
    <br><br><br>

    <?php if($error == 1){?>
    <div class="row">
        <div class="col-4">    
        </div>
        <div class="col-4">
        <div class="alert alert-danger" role="alert" align="center">
      No se cuenta con información
      </div>
        </div>
      
      <div class="col-4">    
            </div>
    </div>
    <br><br><br>
    <?php } ?>
    <?php if($mostrarGrafica == 1){?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <canvas id="montosChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            
            </div>
          </div>

        </div>
    </div>
    <br><br><br>
    <?php } ?>

    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            
              <?php echo $detalle;?>
            </div>
          </div>
        </div>
    </div>

    
</section>






<script src="<?php echo base_url();?>inc/bower_components/selectize/dist/js/standalone/selectize.min.js"></script>
    <script src="<?php echo base_url();?>inc/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>inc/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>inc/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>inc/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>inc/bower_components/summernote/dist/summernote.min.js"></script>


<script src="<?php echo base_url();?>/inc/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<script src="<?php echo base_url();?>/inc/chart/samples/dist/Chart.min.js"></script>
<script src="<?php echo base_url();?>/inc/chart/samples/utils.js"></script>

<script>
  $('#cfdi').attr('class','nav-link active');

  $(document).ready(function () {                                  
            $('.datepicker-1').datepicker({
                autoclose: true
            });

        });       

  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');






     //-------------
    //- BAR CHART2 -
    //-------------
    var config = {
			type: 'line',
			data: {
				labels: [<?php echo $detalleGrafica;?>],
				datasets: [{
					label: 'Montos por Día',
					fill: false,
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: [<?php echo $encabezadoGrafica;?>
					],
				}]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: 'CFDI'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					x: {
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					},
					y: {
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('montosChart').getContext('2d');
			window.myLine = new Chart(ctx, config);
		};
  
    /*
			var barChart2 = document.getElementById('montosChart').getContext('2d');
			window.myBar = new Chart(barChart2, {
				type: 'Line',
          data: {
            labels: [<?php echo $encabezadoGrafica;?>],
            datasets: [
              {
                label: "Cantidad",
                data: [<?php echo $detalleGrafica;?>]
              }
            ]
          },
          options: {
            legend: { display: false },
            title: {
              display: true,
              text: 'Numero de folios'
            }
          }
			});*/



       //-------------
    //- BAR CHART2 -
    //-------------


  });

  function guardar(){

    var fecha1 = $( "#fecha1" ).val();
    var fecha2 = $( "#fecha2" ).val();
    var tipoComprobante = $( "#tipoComprobante" ).val();

    

    if(fecha1 == ""){
        alert('Ingresa una Fecha de inicio');
    }

    if(fecha2 == ""){
        alert('Ingresa una Fecha de fin');
    }


    if(tipoComprobante == ""){
        alert('Selecciona un tipo de comprobante');
    }

    if(fecha1 != '' && fecha2 != '' && tipoComprobante != ''){
      $( "#proveedorForm" ).submit();
    }

    
  }
</script>
