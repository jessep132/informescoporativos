 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
              <a href="<?php echo base_url();?>home/menu" class="brand-link">
                <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >

              </a>
          </div><!-- /.col -->
          <div class="col-sm-6"  align="center">
            <h4><B> Opinión del Cumplimiento de Obligaciones Fiscales</B> </h4>
          </div><!-- /.col -->
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
             <a href="<?php echo base_url();?>home/menu"  >
              <button type="button" class="btn btn-primary btn-sm"><< Regresar</button>
             </a>
            </div>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
</div>



<div class="">

<div class="row">

        <?php if(isset($mostratTodos)){?>
          <div class="col-lg-1 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #016CC0!important;color:#ffffff;font-size:25px;">
              <div class="inner">
                <h6 >Total:&nbsp;&nbsp; <?php echo ($proveedores2);?> <br><br></h6>
                
               
              </div>
              <!--
              <a href="<?php echo base_url();?>proveedores/todos" class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <?php } ?>
          <!-- ./col -->
          <?php if(isset($mostratTodos)){?>
          <div class="col-lg-3 col-6">
          <div class="small-box" style="background-color: #2790E2!important;color:#ffffff">
              <div class="inner">
                <h6 >Cumplimiento Positivo:&nbsp;&nbsp;<?php echo ($proveedoresPositivo);?>  
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <B><FONT SIZE=5>
                <?php 
                        $prov1 = $proveedoresPositivo * 100;
                        if($prov1 > 0){
                          $porcentaje1 = $prov1/$proveedores2;
                        }else{
                          $porcentaje1 = 0;
                        }
               

                        echo  round($porcentaje1,2).' %';
                ?>
               </FONT></B> <br><br></h6>

                
              </div>
              <div class="icon">
                <i class="far "></i>
              </div>
               <!--
              <a href="<?php echo base_url();?>proveedores/positivo"  class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
              
            </div>
         
            
          </div>
          <?php } ?>
          <!-- ./col -->
          <?php if(isset($mostratTodos)){?>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background-color: #49A3EA!important;color:#B3B7BC">
              <div class="inner">
              <h6 style="color:white;">Sin Respuesta Portal SAT / Proveedor :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ($proveedoresSinRespuesta);?>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><FONT SIZE=5>
              <?php 
                        $prov2 = $proveedoresSinRespuesta * 100;
                       
                        if($prov2>0){
                          $porcentaje2 = $prov2/$proveedores2;
                        }else{
                          $porcentaje2 = 0;
                        }

                        echo  round($porcentaje2,2).' %';
                ?></FONT></B>
              </h6>

              
              </div>
         
              
               
               <!--
              <a href="<?php echo base_url();?>proveedores/sinrespuesta"  class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
          </div>
          <?php } ?>
          <!-- ./col -->
          <?php if(isset($mostratTodos)){?>
          <div class="col-lg-3 col-6">
          <div class="small-box bg-danger" style="background-color: #7AC4FF!important;color:#B3B7BC">
              <div class="inner">
              <h6>Opinión del cumplimiento de obligaciones fiscales con inconsistencias : &nbsp;&nbsp; <?php echo ($proveedoresNegativo);?>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><FONT SIZE=5>
              <?php 
                        $prov3 = $proveedoresNegativo * 100;
                        if($prov3>0){
                          $porcentaje3 = $prov3/$proveedores2;
                        }else{
                          $porcentaje3 = 0;
                        }

                        echo  round($porcentaje3,2).' %';
                ?></FONT></B>
              </h6>
                
              </div>

               <!--
              <a href="<?php echo base_url();?>proveedores/negativo" class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
              
            </div>
            <?php } ?>


            <!-- ./col -->
          <?php if(isset($mostratTodos)){?>
          <div class="col-lg-2 col-6">
          <div class="small-box bg-danger" style="background-color: #7AC4FF!important;color:#B3B7BC">
              <div class="inner">
              <h6>Proveedores Inactivos: &nbsp;&nbsp; <?php echo ($proveedoresInactivos);?>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><FONT SIZE=5>
              <?php 
                    $inactivosPor = ($proveedoresInactivos * 100)/$proveedores2;  
                    echo  round($inactivosPor,2).' %';
                ?></FONT></B>
              </h6>
                
              </div>

               <!--
              <a href="<?php echo base_url();?>proveedores/negativo" class="small-box-footer">Ver detalle <i class="fas fa-arrow-circle-right"></i></a>
              -->
            </div>
              
            </div>
            <?php } ?>
          </div>
       

        <div class="row">
        
           
          
          <div class="col-2" >            
              <div style="margin-left:10px;">
              <a  href="<?php echo base_url();?>proveedores/historico32d" class="nav-link ">
              <button type="button"  class="btn btn-block btn-secondary">Historico</button>
              </a>
              </div>
          </div>
          <div class="col-12">
            <!-- /.card-header -->
              <div class="">
                <div class="col-md-12">
                  <div class="card">
                    <div class="">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr class=" text-center">
                       
                            <th ></th>
                            <th >Razón Social</th>
                            <th >RFC</th>
                            <th >Fecha</th>
                            <th >Tipo Proveedor</th>
                            <th >Situación Fiscal</th>
                            
                            <th >Documento</th>
                            <th >QR</th>
                           
            
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($proveedores as $key => $value) {
                          ?>
                          
                          <tr class=" text-center">
                            <td>
                            <a href="<?php echo base_url();?>proveedores/editar/<?php echo $value['id'];?>"  >
                            <img src="<?php echo base_url();?>/inc/editar.png" alt="icono-pdf"  style="width:50%" >
                            </a>
                            </td>
                            <td>
                           
                            <?php echo $value['RazonSocial']?>
                            
                            </td>
                            <td><?php echo $value['RFC']?></td>
                            <td><?php echo date('d-m-Y' ,strtotime($value['fecha_constancia1']));?></td>
                            <td><?php echo $value['tipoProveedor']?></td>
                            <?php if($value['Estatus'] == 'Positivo'){?>
                            <td>
                            Cumplimiento Positivo
                            </td>
                          <?php } elseif($value['Estatus'] == 'SinRespuesta'){?>
                            <td>
                            Sin Respuesta Portal SAT / Proveedor
                            </td>
                          <?php }else{?>
                            <td>
                            Opinión del cumplimiento de obligaciones fiscales con inconsistencias 
                            </td>
                            
                          

                          <?php }?>
                          <td>
                          <?php 
                            if($value['constancia'] == 1){
                              $url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FIN/aws_get_32d.aspx?wsdl";
                              $url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FIN/aws_get_32d.aspx";
                              $client = new SoapClient($url, [] );
                              //var_dump($client->__getFunctions()); 
                              //var_dump($client->__getTypes()); 
                              $params = array(
                                "Pruserrfc" => $value['RFC'],
                                "Pruser4anio" =>  date('Y'),
                                "Pruser4mes" =>  date('m'),
                                );
                      
                        
                                $client->__setLocation($url2);
                                $response = $client->Execute($params);

                           
                            
                            ?>
                             <div class="icon" align="center" style="cursor:pointer">
                              <a href="<?php echo $response->Result;?>" target="_blank">
                             <img src="<?php echo base_url();?>/inc/icono-pdf.png" alt="icono-pdf"  style="width:40%" >
                               </div>
                                </a>
                            <?php
                            }else{  
                            ?>

                            <div class="icon" align="center" style="">
                             <img src="<?php echo base_url();?>/inc/iconoPdfNegro.jpg" alt="icono-pdf"  style="width:40%" >
                               </div>
                            <?php }?>
                            </td>




                            <td>
                          <?php 
                            if($value['constancia'] == 1){
                              $url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_url_32d.aspx?wsdl";
                              $url2 = "http://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_url_32d.aspx";
                              $client = new SoapClient($url, [] );
                              //var_dump($client->__getFunctions()); 
                              //var_dump($client->__getTypes()); 
                              $params = array(
                                "Pruserrfc" => $value['RFC'],
                                "Pruser4anio" =>  date('Y'),
                                "Pruser4mes" =>  date('m'),
                                );
                      
                        
                                $client->__setLocation($url2);
                                $response = $client->Execute($params);

                           
                            
                            ?>
                             <div class="icon" align="center" style="cursor:pointer">
                              <a href="<?php echo $response->Result;?>" target="_blank">
                             <img src="<?php echo base_url();?>/inc/qr.png" alt="icono-pdf"  style="width:100%" >
                               </div>
                                </a>
                            <?php
                            }else{  
                            ?>

                           
                            <?php }?>
                            </td>
                         
                          
                          </tr>
                          <?php }?>
                        
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                    <!--
                    <div class="card-footer clearfix">
                      <ul class="pagination pagination-sm m-0 float-right">
                        <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                      </ul>
                    </div>
                    -->


                  </div>
                  <!-- /.card -->
      
                  <!-- /.card -->
                </div>
      

          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>

<script src="<?php echo base_url();?>/inc/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url();?>/inc/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script>
  $('#Proveedores2').attr('class','nav-link active');
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print"],
      "autoWidth": false,
      "columnDefs": [
        { "width": "100px", "targets": 0 },
        { "width": "300px", "targets": 1 },
        { "width": "300px", "targets": 2 },
        { "width": "300px", "targets": 3 },
        { "width": "300px", "targets": 4 },
        { "width": "300px", "targets": 5 },
        { "width": "10px", "targets": 6 }
      ]


    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

  });

</script>
