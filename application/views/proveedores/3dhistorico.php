 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
              <a href="<?php echo base_url();?>home/menu" class="brand-link">
                <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >
              </a>
          </div><!-- /.col -->
          <div class="col-sm-6" align="center">
            <h1 class="m-0"><B> Opinión del Cumplimiento de Obligaciones Fiscales</B> </h1>
          </div><!-- /.col -->
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
             <a href="<?php echo base_url();?>proveedores/todos"  >
             <button type="button" class="btn btn-primary btn-sm"> << Regresar</button>
             </a>
            </div>
          </div>

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
     <!-- page plugins css -->
     <link rel="stylesheet" href="<?php echo base_url();?>/inc/bower_components/selectize/dist/css/selectize.default.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/inc/bower_components/bootstrap-daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/inc/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/inc/bower_components/summernote/dist/summernote.css" />

<section class="content">
    <div class="col-md-12">
    <form name="proveedorForm" id="proveedorForm"  action="<?php echo base_url();?>proveedores/historico32d"   method="post">
    <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                        <label for="exampleInputEmail1">RFC</label>
                        <input type="text" class="form-control"  id="RFC" name="RFC"  placeholder="RFC" autocomplete="off">
                </div>
            </div>


            <div class="col-md-6">
            <label for="exampleSelectBorder">Año</code></label>
                  <select class="custom-select form-control-border" id="ano" name="ano">
                    <option value="">Elige</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                  </select>
            </div>
            
            <!---
            <div class="col-md-4">
                <div class="form-group">
                        <label for="exampleInputEmail1">Fecha Inicio</label>
                        <input type="text"class="form-control datepicker-1" placeholder="Fecha Inicio dd/mm/yyyy" data-date-format="dd/mm/yyyy" data-provide="datepicker" id="fecha1" name="fecha1"  placeholder="Fecha Inicio" autocomplete="off">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                        <label for="exampleInputEmail1">Fecha Fin</label>
                        <input type="text" class="form-control datepicker-1" placeholder="Fecha Fin dd/mm/yyyy" data-date-format="dd/mm/yyyy" data-provide="datepicker" id="fecha2" name="fecha2" placeholder="Fecha Fin" autocomplete="off">
                </div>
            </div>
            -->

           
           
        </div>

    </form>
    <div class="row">
             <div class="col-5">    
            </div>
            <div class="col-2">
                <button type="submit" class="btn btn-success btn-block" onclick="guardar();">Consultar</button>
            </div>
            <div class="col-5">    
            </div>
        </div>
    </div>
    <br><br><br>

    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            
              <?php echo $detalle;?>
            </div>
          </div>
        </div>
    </div>

    
</section>






<script src="<?php echo base_url();?>inc/bower_components/selectize/dist/js/standalone/selectize.min.js"></script>
    <script src="<?php echo base_url();?>inc/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>inc/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>inc/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>inc/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>inc/bower_components/summernote/dist/summernote.min.js"></script>


<script src="<?php echo base_url();?>/inc/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<script>
  $('#cfdi').attr('class','nav-link active');


  $(document).ready(function () {                                  
            $('.datepicker-1').datepicker({
                autoclose: true
            });

        });    

        $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print"],
      "autoWidth": false,
      "columnDefs": [
        { "width": "300px", "targets": 0 },
        { "width": "300px", "targets": 1 },
        { "width": "300px", "targets": 2 },
        { "width": "300px", "targets": 3 },
        { "width": "300px", "targets": 4 },
        { "width": "10px", "targets": 5 },
        { "width": "10px", "targets": 6 }
      ]


    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

  });

  function guardar(){
    $( "#proveedorForm" ).submit();
  }
</script>
