<style>
.card-primary:not(.card-outline)>.card-header {
    background-color: #CAC7C8;
}
 </style>
 
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
          <a href="<?php echo base_url();?>home/menu" class="brand-link">
                <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >

              </a>
           
          </div><!-- /.col -->
          <div class="col-sm-6" align="center">
             <!-- Brand Logo -->
             <h4><B> Configuración</B> </h4>
          </div><!-- /.col -->
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
             <a href="<?php echo base_url();?>home/menu"  >
              <button type="button" class="btn btn-primary btn-sm"> << Regresar</button>
             </a>
            </div>
          </div>

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<div class="container-fluid">
<div class="row">
      <div class="col-md-4">
      <B> Usuarios</B>
      <br> <br> <br>
      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr class=" text-center">
                       
                            <th>Usuario</th>

                            <th>Ultimo acceso</th>
                            <th># Ingresos</th>
                            <th></th>

            
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($usuarios as $key => $value) {
                          ?>
                          
                          <tr>
                            <td><?php echo $value['use_session']?></td>
                            <td><?php echo $value['use_lastaccess']?></td>
                            <td class=" text-center"><?php 
                            
                            if($value['metodo'] != ''){
                              echo $value['total'];
                            }else{
                              echo 0;
                            }
                           ?></td>
                                              
                                              <td>
                                              <a href="<?php echo base_url();?>proveedores/deleteUsers/<?php echo $value['use_id']?>" class="tooltip-error" data-rel="tooltip" title="Delete">
                                              <button type="button" class="close" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                              </button>
                                              </a>
                                              </td>
                          </tr>
                          <?php }?>
                        
                        </tbody>
                      </table> 

                      <hr>
                         
                     <form name="usuariosGuar" id="usuariosGuar"  action="<?php echo base_url();?>proveedores/guardarUsuario"   method="post">
                      <div class="col-md-12" >
                          <div class="form-group">
                          <B>Nuevo Usuario</B>
                              <input type="text" class="form-control" id="Usuario" name="Usuario" placeholder="Usuario">       
                            </div>
                      </div>
                      <div class="col-md-12" >
                          <div class="form-group">
                              <label for="exampleSelectBorder">Contraseña </code></label>
                              <input type="text" class="form-control" id="password" name="password" placeholder="Contraseña">       
                            </div>
                      </div>

                      <div class="col-md-12" >
                          <div class="form-group">
                              <label for="exampleSelectBorder">Correo </code></label>
                              <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo">       
                            </div>
                      </div>

                      <div class="col-md-12" >
                          <div class="form-group">
                              <label for="exampleSelectBorder">Tipo usuario</code></label>
                              <select class="custom-select form-control-border" id="tipoUser" name="tipoUser">
                              <option value="">Elige</option>
                              <option value="12">Usuario</option>
                              <option value="1">Administrador</option>
                            </select>     
                            </div>
                      </div>
                      </form>

                      <div class="row">

                      <div class="col-3">
                      </div>

                      <div class="col-6">
                        <button type="submit" class="btn btn-success btn-block" onclick="GuardarUsuario();">Guardar Usuario</button>
                      </div>

                        <div class="col-3">
                      </div>

                      </div>

      </div>
      <div class="col-md-4">
      <B>Envío de correos electrónicos</B>
      <br> <br> <br>
     Opinión del Cumplimiento de Obligaciones Fiscales 
      <form name="frecuecia" id="frecuecia"  action="<?php echo base_url();?>proveedores/guardarFrecuencia"   method="post">
            <div class="col-md-12">
                <div class="form-group">
                  <label for="exampleSelectBorder">Frecuencia mensual del día 5 al día 25 de cada mes </code></label>
                  <div class="col-md-12" id="" style="display:none">
                  <select class="custom-select form-control-border" id="frecuencua" name="frecuencua" onchange="mostrarConfigura();" disabled>
                    <option value="">Elige</option>
                    <option value="Mes" <?php if(!empty($configuracion) && $configuracion[0]['frecuencia']=='Mes'){ echo "selected='selected'";}  ?>>Mes</option>
                  </select>
                  </div>

                </div>
            </div>

            <?php if(!empty($configuracion) && $configuracion[0]['frecuencia']=='Mes'){?>
            <div class="col-md-12" id="mes" style="display:block">
            <?php }else{?>
              <div class="col-md-12" id="mes" style="display:none">
            <?php } ?>

              <div class="form-group">
                    <?php if(!empty($configuracion)){ ?>
                    <input type="hidden" class="form-control" id="diacadames" name="diacadames" value="<?php echo $configuracion[0]['diames'];?>" placeholder="" disabled>       
                    <?php }else{ ?>
                      <input type="hidden" class="form-control" id="diacadames" name="diacadames" value="" placeholder="" disabled>       
                    <?php } ?>
                  </div>

                 
            </div>

            <?php if(!empty($configuracion) && $configuracion[0]['frecuencia']=='Dia'){?>
            <div class="col-md-12" id="dia" style="display:block">
            <?php }else{?>
              <div class="col-md-12" id="dia" style="display:none">
              <?php } ?>
            <div class="form-group">
                  <label for="exampleSelectBorder">Día de la semana</code></label>
                  <select class="custom-select form-control-border" id="diaSemana" name="diaSemana">
                    <option value="">Elige</option>
                    <option value="1" <?php if(!empty($configuracion) && $configuracion[0]['diasemana']=='1'){ echo "selected='selected'";}  ?>>Lunes</option>
                    <option value="2" <?php if(!empty($configuracion) && $configuracion[0]['diasemana']=='2'){ echo "selected='selected'";}  ?>>Martes</option>
                    <option value="3" <?php if( !empty($configuracion) && $configuracion[0]['diasemana']=='3'){ echo "selected='selected'";}  ?>>Miercoles</option>
                    <option value="4" <?php if(!empty($configuracion) && $configuracion[0]['diasemana']=='4'){ echo "selected='selected'";}  ?>>Jueves</option>
                    <option value="5" <?php if(!empty($configuracion) && $configuracion[0]['diasemana']=='5'){ echo "selected='selected'";}  ?>>Viernes</option>
                    <option value="6" <?php if(!empty($configuracion) && $configuracion[0]['diasemana']=='6'){ echo "selected='selected'";}  ?>>Sabado</option>
                    <option value="0" <?php if(!empty($configuracion) && $configuracion[0]['diasemana']=='0'){ echo "selected='selected'";}  ?>>Domingo</option>
                  </select>
                </div>

            </div>
            

            <div class="row">


              <div class="col-12">
              <B>Texto de solicitud </B>
              <textarea class="form-control" id="exampleFormControlTextarea1" name="exampleFormControlTextarea1" rows="3"><?php echo $configuracion[0]['texto'];?>
              </textarea>
              </div>


              </div>
              <br> <br> <br>
              <div class="row">
<div class="col-12">
<B>Texto de recordatorio</B>
<textarea class="form-control" id="exampleFormControlTextarea123" name="exampleFormControlTextarea123" rows="3">
<?php echo $configuracion[0]['recordatorio'];?>
</textarea>
</div>


<br> <br> <br>  <br> <br> <br>

</div>
    
             Constancia de Situación Fiscal
              <div class="col-md-12">
                <div class="form-group">
                  <label for="exampleSelectBorder">Frecuencia</code></label>
                  <select class="custom-select form-control-border" id="frecuencua2" name="frecuencua2" >
                    <option value="">Elige</option>
                    <option value="Cuatrimestral" <?php if(!empty($configuracion2) && $configuracion2[0]['frecuencia']=='cuatrimestral'){ echo "selected='selected'";}  ?>>Cuatrimestral</option>
                    <option value="Semestral" <?php if(!empty($configuracion2) && $configuracion2[0]['frecuencia']=='Semestral'){ echo "selected='selected'";}  ?>>Semestral</option>
                    <option value="Anual" <?php if(!empty($configuracion2) && $configuracion2[0]['frecuencia']=='Anual'){ echo "selected='selected'";}  ?>>Anual</option>
                  </select>
                </div>
            </div>

            <div class="row">


<div class="col-12">
<B>Texto de solicitud </B> 
<textarea class="form-control" id="exampleFormControlTextarea12" name="exampleFormControlTextarea12" rows="3">
<?php 
echo $configuracion2[0]['texto'];
?>
</textarea>
</div>
</div>
</form>
<br> <br> <br>



<div class="row">

            <div class="col-6">
     
    
                    <label for="exampleInputFile"> Subir plantilla </label>
                    <form   action="<?php echo base_url();?>proveedores/guardarCorreos" enctype="multipart/form-data"   method="post">
                  
                      <div class="">
                      <input type="file"  id="subirPlantilla" name="subirPlantilla">
                      </div>
                      <br>
                    <button type="submit" class="btn btn-secondary btn-block" >Subir</button>
                    </form>

                    <?php if($ArchivoGuardado == 1){ ?>
                    <br>
                    <div class="alert alert-success" role="alert" align="center">
                    Información cargada éxito 
                  </div>
                  <?php }?>
             
            </div>

            <div class="col-6">
            <br><br><br>
            <p class="mb-1">
            <a href="<?php echo base_url();?>proveedores/excelProveedores" >
            <div style="margin-top:14px">
             <button type="bottom" class="btn btn-secondary btn-block" >Plantilla de correos</button>
            </div>
           
            </a>
            </p>
            </div>

            


            <br> <br> <br>  <br> <br> <br>




            </div>
              



            <div class="row">

            <div class="col-3">
            
            </div>

            <div class="col-6">
              <button type="submit" class="btn btn-success btn-block" onclick="GuardarFrecuencia();">Guardar configuracion</button>
            </div>

              <div class="col-3">
            </div>



            <br> <br> <br>  <br> <br> <br>



            </div>


   
      </div>
      <div class="col-md-4">
      <B>FIEL</B>
        <br> <br> <br>
        
        <div class="form-group">
                    <label for="exampleSelectBorder">FIEL	Contraseña</label>
                    <input type="email" class="form-control" id="fielpass" name="fielpass" placeholder="">       
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Certificado (.cer)</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="certificado">
                        <label class="custom-file-label" for="exampleInputFile"></label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Clave Privada (.key)</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="llave">
                        <label class="custom-file-label" for="exampleInputFile"></label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-3">
                  </div>
                  <div class="col-6">
                    <button type="submit" class="btn btn-success btn-block" onclick="GuardarDatos();">Guardar Datos conexion</button>
                  </div>
                  <div class="col-3">
                  </div>
                  </div>  
      </div>
      </div>

</div>
  
</div><!-- /.container-fluid -->
      
<script src="<?php echo base_url();?>/inc/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>/inc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url();?>/inc/plugins/chart.js/Chart.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>/inc/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>/inc/dist/js/demo.js"></script>
  <script>


function GuardarUsuario(){
    $( "#usuariosGuar" ).submit();
  }

  function GuardarFrecuencia(){
    $( "#frecuecia" ).submit();
  }

  function mostrarConfigura(){
      var frecuencia = $('#frecuencua').val();
      if(frecuencia != ''){

        if(frecuencia == 'Mes'){
          $( "#mes" ).show();

          $( "#dia" ).hide();
        }else{
          $( "#dia" ).show();

          $( "#mes" ).hide();
        }

      }else{
        $( "#mes" ).hide();
        $( "#dia" ).hide();
      }

  }

$(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

  });

</script>