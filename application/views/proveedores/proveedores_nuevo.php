  <!-- Content Header (Page header) -->
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
            <a href="<?php echo base_url();?>home/menu" class="brand-link">
                <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >
              </a> 
          </div><!-- /.col -->
          <div class="col-sm-6"  align="center">
            <h1 class="m-0">Alta proveedores</h1>
          </div><!-- /.col -->
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
             <a href="<?php echo base_url();?>home/menu"  >
              <button type="button" class="btn btn-primary btn-lg">Menú</button>
             </a>
            </div>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
</div>
    <!-- Main content -->
   
<section class="content">
    <div class="col-md-12">
    <form name="proveedorForm" id="proveedorForm"  action="<?php echo base_url();?>proveedores/guardar"   method="post">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                        <label for="exampleInputEmail1">Nombre</label>
                        <input type="email" class="form-control" id="nombre" name="nombre"  placeholder="Nombre">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                        <label for="exampleInputEmail1">Apellido Paterno</label>
                        <input type="email" class="form-control" id="apellidoPaterno" name="apellidoPaterno" placeholder="Apellido Paterno">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                        <label for="exampleInputEmail1">Apellido Materno</label>
                        <input type="email" class="form-control" id="apellidoMaterno" name="apellidoMaterno" placeholder="Apellido Materno">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleSelectBorder">Tipo de persona</code></label>
                  <select class="custom-select form-control-border" id="tipoPersona" name="tipoPersona">
                    <option value="">Elige</option>
                    <option value="Fisica">Fisica</option>
                    <option value="Moral">Moral</option>
                  </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleSelectBorder">Tipo de proveedor</code></label>
                  <select class="custom-select form-control-border" id="tipoProveedor" name="tipoProveedor">
                    <option value="">Elige</option>
                    <option value="Servicios">Servicios</option>
                    <option value="Insumos">Insumos</option>
                  </select>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                        <label for="exampleInputEmail1">Nombre comercial</label>
                    <input type="email" class="form-control" id="nombreComercial" name="nombreComercial" placeholder="Nombre comercial">
                </div>
                
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                        <label for="exampleInputEmail1">Nombre/Razon Social</label>
                        <input type="email" class="form-control" id="nombreRazonSocial" name="nombreRazonSocial" placeholder="Nombre/Razon Social">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                        <label for="exampleInputEmail1">RFC</label>
                        <input type="email" class="form-control" id="RFC" placeholder="RFC" name="RFC">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <div class="form-group">
                            <label for="exampleInputEmail1">C.P.</label>
                        <input type="email" class="form-control" id="codigoPostal" name="codigoPostal" placeholder="Codigo postal">
                </div>
            </div>
           
            <div class="col-md-4">
                <div class="form-group">
                            <label for="exampleInputEmail1">Calle</label>
                        <input type="email" class="form-control" id="calle" name="calle" placeholder="Calle">
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                            <label for="exampleInputEmail1">No Int</label>
                        <input type="email" class="form-control" id="noInt" name="noInt" placeholder="No Int">
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                            <label for="exampleInputEmail1">No Ext</label>
                        <input type="email" class="form-control" id="noExt" name="noExt" placeholder="Colonia">
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                            <label for="exampleInputEmail1">Colonia</label>
                        <input type="email" class="form-control" id="colonia" name="colonia" placeholder="Colonia">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                            <label for="exampleInputEmail1">Estado</label>
                        <input type="email" class="form-control" id="estado" name="estado" placeholder="Colonia">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                            <label for="exampleInputEmail1">Municipio</label>
                        <input type="email" class="form-control" id="municipio" name="municipio" placeholder="Colonia">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                            <label for="exampleInputEmail1">Ciudad</label>
                        <input type="email" class="form-control" id="ciudad" name="ciudad" placeholder="Colonia">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                            <label for="exampleInputEmail1">Contacto</label>
                        <input type="email" class="form-control" id="contacto" name="contacto" placeholder="Contacto">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                            <label for="exampleInputEmail1">Telefono</label>
                        <input type="email" class="form-control" id="telefono" name="telefono" placeholder="Telefono">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                            <label for="exampleInputEmail1">Correo</label>
                        <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo">
                </div>
            </div>
        </div>
        </form>

        <div class="row">
            <div class="col-12">  
                <br><br><br>
            </div>
            <div class="col-12">    
            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block" onclick="guardar();">Guardar</button>
            </div>
        </div>

    </div>
</section>




<script src="<?php echo base_url();?>/inc/plugins/jquery/jquery.min.js"></script>

<script>
  $('#Proveedores2').attr('class','nav-link active');


  function guardar(){
    $( "#proveedorForm" ).submit();
  }

</script>