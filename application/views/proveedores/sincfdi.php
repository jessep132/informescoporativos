 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
              <a href="<?php echo base_url();?>home/menu" class="brand-link">
                <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >

              </a>
          </div><!-- /.col -->
          <div class="col-sm-6"  align="center">
            <h4><B> Proveedores sin CFDI´s</B> </h4>
          </div><!-- /.col -->
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
             <a href="<?php echo base_url();?>home/menu"  >
              <button type="button" class="btn btn-primary btn-sm"><< Regresar</button>
             </a>
            </div>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
</div>



<div class="">

     
        <div class="row">
        <div class="col-3" >
              <div style="margin-left:20px;">
                    <form   action="<?php echo base_url();?>proveedores/guardarProveedores" enctype="multipart/form-data"   method="post">
    
                        <input type="file"  id="subirPlantilla" name="subirPlantilla">
                  
                    <div style="margin-top:5px">
                    <button type="submit" class="btn btn-secondary btn-block" >Subir</button>
                    </div>
                    </form>
                    <?php if($ArchivoGuardado == 1){ ?>
                    <br>
                    <div class="alert alert-success" role="alert" align="center">
                    Información cargada éxito 
                  </div>
                  <?php }?>
              </div>
          </div>
          <div class="col-3" >            
              <div style="margin-left:10px;margin-top:34px">
              <a href="<?php echo base_url();?>proveedores/excelProveedoressinCFDI" >
              <button type="bottom" class="btn btn-secondary btn-block" >Plantilla Proveedores sin  CFDI´s</button>
              </a>
              </div>
          </div>
           
         
          <div class="col-12">
          <br> <br> <br>
          <?php if(!empty($proveedores)) {
          ?>
                    
            <!-- /.card-header -->
              <div class="">
                <div class="col-md-12">
                  <div class="card">
                    <div class="">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr class=" text-center">
                       
                            
                            <th >Razón Social</th>
                            <th >RFC</th>
                            <th >Fecha</th>
                            <th >Tipo Proveedor</th>
                            <th >Situación Fiscal</th>
                            
                            <th >Documento</th>
                           
            
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($proveedores as $key => $value) {
                          ?>
                          
                          <tr class=" text-center">
                            <td><?php echo $value['RazonSocial']?></td>
                            <td><?php echo $value['RFC']?></td>
                            <td><?php echo date('d-m-Y' ,strtotime($value['fecha_constancia1']));?></td>
                            <td><?php echo $value['tipoProveedor']?></td>
                            <?php if($value['Estatus'] == 'Positivo'){?>
                            <td>
                            Cumplimiento Positivo
                            </td>
                          <?php } elseif($value['Estatus'] == 'SinRespuesta'){?>
                            <td>
                            Sin Respuesta Portal SAT / Proveedor
                            </td>
                          <?php }else{?>
                            <td>
                            Opinión del cumplimiento de obligaciones fiscales con inconsistencias :
                            </td>
                            
                          

                          <?php }?>
                          <td>
                          <?php 
                            if($value['constancia'] == 1){
                              $url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FIN/aws_get_32d.aspx?wsdl";
                              $url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FIN/aws_get_32d.aspx";
                              $client = new SoapClient($url, [] );
                              //var_dump($client->__getFunctions()); 
                              //var_dump($client->__getTypes()); 
                              $params = array(
                                "Pruserrfc" => $value['RFC'],
                                "Pruser4anio" =>  date('Y'),
                                "Pruser4mes" =>  date('m'),
                                );
                      
                        
                                $client->__setLocation($url2);
                                $response = $client->Execute($params);

                           
                            
                            ?>
                             <div class="icon" align="center" style="cursor:pointer">
                              <a href="<?php echo $response->Result;?>" target="_blank">
                             <img src="<?php echo base_url();?>/inc/icono-pdf.png" alt="icono-pdf"  style="width:40%" >
                               </div>
                                </a>
                            <?php
                            }else{  
                            ?>

                            <div class="icon" align="center" style="">
                             <img src="<?php echo base_url();?>/inc/iconoPdfNegro.jpg" alt="icono-pdf"  style="width:40%" >
                               </div>
                            <?php }?>
                            </td>
                         
                          
                          </tr>
                          <?php }?>
                        
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                    <!--
                    <div class="card-footer clearfix">
                      <ul class="pagination pagination-sm m-0 float-right">
                        <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                      </ul>
                    </div>
                    -->


                  </div>
                  <!-- /.card -->
      
                  <!-- /.card -->
                </div>
      

          <!-- /.col -->
          <?php }?>
        </div>
        <!-- /.row -->
      </div>

<script src="<?php echo base_url();?>/inc/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url();?>/inc/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script>
  $('#Proveedores2').attr('class','nav-link active');
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print"],
      "autoWidth": false,
      "columnDefs": [
        { "width": "300px", "targets": 0 },
        { "width": "300px", "targets": 1 },
        { "width": "300px", "targets": 2 },
        { "width": "300px", "targets": 3 },
        { "width": "300px", "targets": 4 },
        { "width": "10px", "targets": 5 }
      ]


    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

  });

</script>
