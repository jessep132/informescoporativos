  <!-- Content Header (Page header) -->
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-3">
                <a href="<?php echo base_url();?>home/menu" class="brand-link">
                    <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >
                </a> 
            </div><!-- /.col -->

            <div class="col-sm-6" align="center">
            <h4><B>Editar proveedores</B></h4>
          </div><!-- /.col -->
          
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
            <a href="<?php echo base_url();?>proveedores/todos"  >
              <button type="button" class="btn btn-primary btn-sm"><< Regresar</button>
             </a>
            </div>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
</div>
    <!-- Main content -->
   
<section class="content">
    <div class="col-md-12">
    <form name="proveedorForm" id="proveedorForm"  action="<?php echo base_url();?>proveedores/update"   method="post">
    <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $proveedores['id'];?>"  placeholder="Nombre">
    
 
        <div class="row">
            
            <div class="col-md-3">
                <div class="form-group">
                  <label for="exampleSelectBorder">Tipo de proveedor</code></label>
                  <select class="custom-select form-control-border" id="tipoProveedor" name="tipoProveedor">
                    <option value="">Elige</option>
                    <option value="Servicios" <?php if($proveedores['tipoProveedor'] == 'Servicios'){?> selected="selected"<?php }?> >Servicios</option>
                    <option value="Insumos" <?php if($proveedores['tipoProveedor'] == 'Insumos'){?> selected="selected"<?php }?> >Otros</option>
                  </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        <label for="exampleInputEmail1">Nombre/Razón Social</label>
                        <input type="text" class="form-control" id="nombreRazonSocial" name="nombreRazonSocial" value="<?php echo $proveedores['RazonSocial'];?>" placeholder="Nombre/Razon Social">
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                        <label for="exampleInputEmail1">RFC</label>
                        <input type="text" class="form-control" id="RFC" placeholder="RFC" value="<?php echo $proveedores['RFC'];?>" name="RFC">
                </div>
            </div>
        </div>
        
        <div class="row">
 
            <div class="col-md-4">
                <div class="form-group">
                            <label for="exampleInputEmail1">Correo</label>
                        <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo" value="<?php echo $proveedores['Correo'];?>">
                </div>
            </div>
        </div>
        </form>

        <div class="row">
        <div class="col-12">  
            <br><br><br>
        </div>
        <div class="col-4">    
        </div>
        <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block" onclick="guardar();">Actualizar</button>
        </div>
        <div class="col-4">    
        </div>
        </div>

    </div>
</section>




<script src="<?php echo base_url();?>/inc/plugins/jquery/jquery.min.js"></script>

<script>
  $('#Proveedores2').attr('class','nav-link active');


  function guardar(){
    $( "#proveedorForm" ).submit();
  }

</script>