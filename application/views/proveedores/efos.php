 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
            <a href="<?php echo base_url();?>home/menu" class="brand-link">
                  <img src="<?php echo base_url();?>/inc/logo.png" alt="AdminLTE Logo"  style="width:190%;margin-left:-80px" >
                </a>
          </div><!-- /.col -->
          <div class="col-sm-6" align="center">
            <h4><B>Contribuyentes con Operaciones Presuntamente Inexistentes
            <br>
            
            <?php  echo date('d-m-Y',strtotime($efosActualiza['efosActualiza']));?>
            </B></h4>
          </div><!-- /.col -->
          <div class="col-sm-3">
            <div style="margin-left:170px;margin-top:25px;">
             <a href="<?php echo base_url();?>home/menu"  >
             <button type="button" class="btn btn-primary btn-sm"><< Regresar</button>
             </a>
            </div>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
</div>


<div class="container-fluid">
          <div class="row">
          <div class="col-3" >
              <div style="margin-left:20px;">
              <a  href="<?php echo base_url();?>proveedores/historicoefos2" class="nav-link ">
              <button type="button"  class="btn btn-block btn-secondary">Historico - EFOS Relacionados </button>
              </a>
              </div>
          </div>
          <div class="col-2" >            
              <div style="margin-left:10px;">
              <a  href="<?php echo base_url();?>proveedores/historicoefos" class="nav-link ">
              <button type="button"  class="btn btn-block btn-secondary">Historico - SAT</button>
              </a>
              </div>
          </div>

         


          <div class="col-12">
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">
                
                
            <div class="card">
              <div class="col-4"></div>
             
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Situacion</th>
                    <th>
                    <div class="row">
                    <div class="col-lg-6 col-6">
                        Contribuyentes TOTAL 
                    </div>
                    <div class="col-lg-4 col-6">
                          <a href="http://omawww.sat.gob.mx/cifras_sat/Documents/Listado_Completo_69-B.csv">
                          <button type="button" class="btn btn-block btn-success  btn-sm">Descargar</button>
                          </a>
                          </div>
                      </div>
                      </th>
                    <th>
                    <div class="row">
                    <div class="col-lg-6 col-6">
                    Relacionados
                    </div>
                    <div class="col-lg-5 col-6">
                    <a  href="<?php echo base_url();?>proveedores/efos_excel_relacionados" >
                      <button type="button" class="btn btn-block btn-success  btn-sm">Descargar</button>
                      </a>
                      </div>
                      </div>
                      
                    </th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>Definitivos</td>
                    <td align="center"><?php echo $Definitivo;?></td>
                    <td align="center"><?php echo $Definitivo2;?></td>
                  
                  </tr>
                  <tr>
                    <td>Desvirtuados</td>
                    <td align="center"><?php echo $Desvirtuado;?></td>
                    <td align="center"><?php echo $Desvirtuado2;?></td>

                  </tr>
                  <tr>
                    <td>Presuntos</td>
                    <td align="center"><?php echo $Presunto;?></td>
                    <td align="center"><?php echo $Presunto2;?></td>
                  
                  </tr>
                  <tr>
                    <td>Sentencias favorables</td>
                    <td align="center"><?php echo $SentenciaFavorable;?></td>
                    <td align="center"><?php echo $SentenciaFavorable2;?></td>
                  
                  </tr>
                  <tr>
                    <td><B>Total general</B></td>
                    <td align="center"><?php echo $Definitivo+$Desvirtuado+$Presunto+$SentenciaFavorable;?></td>
                    <td align="center"><?php echo $Definitivo2+$Desvirtuado2+$Presunto2+$SentenciaFavorable2;?></td>
                   
                  </tr>
         
          
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
  </div>




<script src="<?php echo base_url();?>/inc/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url();?>/inc/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>/inc/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script>
  $('#Efos').attr('class','nav-link active');


</script>
