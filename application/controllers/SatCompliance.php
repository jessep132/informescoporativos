<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SatCompilance
 * 
 * Este archivo contiene las rutas y la logica para la subida del 
 * 32D del cumplimiento de los proveedores
 * 
 * @author Luis Manuel Torres Trevino
 * @version 1.0.0
 * @since 16/02/2021
 */
class SatCompilance extends CI_Controller
{
    /**
     * __construct
     * 
     * Constructor del controlador SatCompilance
     * 
     * @author Luis Manuel Torres Trevino
     * @version 1.0.0
     * @since 16/02/2021
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    /**
     * index
     * 
     * Funcion que se encarga de cargar la pagina para subir los cumplimientos
     * de los proveedores
     * 
     * @author Luis Manuel Torres Trevino
     * @version 1.0.0
     * @since 16/02/2021
     */
    public function index()
    {
        $data = array();
        $this->load->view("login/subir32d.php", $data);
    }

    /** 
     * validateSatCompliance
     * 
     * Esta funcion envia los datos del pdf al webservice de desserviciosweb
     * para validar el cumplimiento del sat del proveedor.
     * 
     * @author	Luis Manuel Torres Trevino
     * @version	1.0.0
     * @since 	16/02/2021
     * @return	array	Arreglo con un boolean que indica si el proceso tuvo un error y
     * un mensaje con la respuesta del servicio
     */
    public function validateSatCompliance()
    {
        $data = array();
        $this->load->database();

        $serviceUrl = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FIN/aws_set_32d_prov.aspx?wsdl";
        $userRFC = $_POST["rfc"];
        $documentMonth = $_POST["document_month"];
        $documentYear = $_POST["document_year"];
        $documentBase64 = $_POST["document_base64"];

        $requestClient = new SoapClient($serviceUrl, []);
        $requestParams = array(
            "PARMPRUSERRFC" => $userRFC,
            "PARMMES" => $documentMonth,
            "PARMANIO" => $documentYear,
            "PARMARCHIVO_BASE64" => $documentBase64
        );

        $response = $requestClient->Execute($requestParams);
        redirect(base_url()."home/menu");
    }
}
