<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * IT cordinador
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2020 - Grupo Solana
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 08-Junio-2020
 * 
*/


class Proveedores extends MY_Controller 
{
    public function __construct()
    {
		parent::__construct();
		
	}
	

	
	public function index($estatus)
	{

		$data = array();
		$this->load->database();

		dd('entro');
		/* Prueba de conexion a la DB */
		$sql ="SELECT * FROM proveedores WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = count($agencias);
		
		$sql ="SELECT * FROM proveedores WHERE Estatus='Positivo' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivo'] = count($agencias);

		$sql ="SELECT * FROM proveedores WHERE Estatus='SinRespuesta' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuesta'] = count($agencias);

		$sql ="SELECT * FROM proveedores WHERE Estatus='Negativo' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativo'] = count($agencias);


		$sql ="SELECT * FROM proveedores WHERE Estatus='Positivo' AND tipoProveedor='Insumos' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivoInsumos'] = count($agencias);


		$sql ="SELECT * FROM proveedores WHERE Estatus='SinRespuesta' AND tipoProveedor='Insumos' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuestaInsumos'] = count($agencias);

		$sql ="SELECT * FROM proveedores WHERE Estatus='Negativo' AND tipoProveedor='Insumos' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativoInsumos'] = count($agencias);


		$sql ="SELECT * FROM proveedores WHERE Estatus='Positivo' AND tipoProveedor='Servicio' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivoServicio'] = count($agencias);


		$sql ="SELECT * FROM proveedores WHERE Estatus='SinRespuesta' AND tipoProveedor='Servicio' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuestaServicio'] = count($agencias);

		$sql ="SELECT * FROM proveedores WHERE Estatus='Negativo' AND tipoProveedor='Servicio' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativoServicio'] = count($agencias);

		$this->view("dashboard/home", $data);
	}


	public function todos()
	{

		$data = array();
		$this->load->database();

		/* Prueba de conexion a la DB */
		$sql ="SELECT * FROM proveedores WHERE  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = $agencias;
		$data['proveedores2'] = count($agencias);

		
		$sql ="SELECT * FROM proveedores WHERE Estatus='Positivo'  AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivo'] = count($agencias);

	

		$sql ="SELECT * FROM proveedores WHERE Estatus='SinRespuesta'  AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuesta'] = count($agencias);

	


		$sql ="SELECT * FROM proveedores WHERE Estatus='Negativo'  AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativo'] = count($agencias);

		




		$sql ="SELECT * FROM proveedores WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."' AND (Correo = '' OR Correo IS NULL) ";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresInactivos'] = count($agencias);


	

		
		$data['mostratTodos'] = 1;
		$this->view2("proveedores/proveedores_estatus", $data);
	}



	public function todos_constancia()
	{

		$data = array();
		$this->load->database();

		/* Prueba de conexion a la DB */
		$sql ="SELECT * FROM proveedores WHERE   rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = $agencias;
		$data['proveedores2'] = count($agencias);

		
		$sql ="SELECT * FROM proveedores WHERE constancia2='1' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivo'] = count($agencias);

		$sql ="SELECT * FROM proveedores WHERE constancia2='0' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuesta'] = count($agencias);


		
		$data['mostratTodos'] = 1;
		$this->view2("proveedores/proveedores_estatus_constancia", $data);
	}



	public function positivo()
	{

		$data = array();
		$this->load->database();

		/* Prueba de conexion a la DB */
		$sql ="SELECT * FROM proveedores WHERE Estatus='Positivo' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = $agencias;
		

		$this->view2("proveedores/proveedores_estatus", $data);
	}


	public function sinrespuesta()
	{

		$data = array();
		$this->load->database();

		/* Prueba de conexion a la DB */
		$sql ="SELECT * FROM proveedores WHERE Estatus='SinRespuesta' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = $agencias;
		

		$this->view2("proveedores/proveedores_estatus", $data);
	}


	public function negativo()
	{

		$data = array();
		$this->load->database();

		/* Prueba de conexion a la DB */
		$sql ="SELECT * FROM proveedores WHERE Estatus='Negativo' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = $agencias;
		$data['proveedoresNegativo'] = count($agencias);


		

		$this->view2("proveedores/proveedores_estatus", $data);
	}



	public function nuevo(){
		$data = array();
		$this->view2("proveedores/proveedores_nuevo", $data);

	}


	public function editar($id){
		$data = array();


		$sql ="SELECT * FROM proveedores WHERE id='".$id."' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = $agencias[0];


		
		$this->view2("proveedores/proveedores_edit", $data);

	}


	public function guardar(){

		$data = array();
		$this->load->database();
		$data = array(
			'NombreComercial' => $_POST['nombreComercial'],
			'nombre' => $_POST['nombre'],
			'apellidoPaterno' => $_POST['apellidoPaterno'],
			'apellidoMaterno' => $_POST['apellidoMaterno'],
			'tipoPersona' => $_POST['tipoPersona'],
			'tipoProveedor' => $_POST['tipoProveedor'],
			'RazonSocial' => $_POST['nombreRazonSocial'],
			'RFC' => $_POST['RFC'],
			'codigoPostal' => $_POST['codigoPostal'],
			'noInt' => $_POST['noInt'],
			'noExt' => $_POST['noExt'],
			'colonia' => $_POST['colonia'],
			'estado' => $_POST['estado'],
			'municipio' => $_POST['municipio'],
			'ciudad' => $_POST['ciudad'],
			'Contacto' => $_POST['contacto'],
			'telefono' => $_POST['telefono'],
			'Correo' => $_POST['correo'],
			'Estatus' => 'SinRespuesta'
		);

		$this->db->insert('proveedores', $data);



		redirect(base_url().'proveedores/todos');
	}



	public function update(){

		$data = array();
		$this->load->database();
		$data = array(
			'tipoProveedor' => $_POST['tipoProveedor'],
			'RazonSocial' => $_POST['nombreRazonSocial'],
			'RFC' => $_POST['RFC'],
			'Correo' => $_POST['correo']
		);

	

		$this->db->where('id', $_POST['id']);
		$this->db->update('proveedores', $data);

		redirect(base_url().'proveedores/todos');
	}


	public function efos(){
		$data = array();
		$data = array();
		$this->load->database();

		/* Prueba de conexion a la DB */
		$sql ="SELECT * FROM efos WHERE situacion='Desvirtuado' ";
		$agencias = $this->db->query($sql)->result_array();
		$data['Desvirtuado'] = count($agencias);

		$sql ="SELECT * FROM efos INNER JOIN proveedores ON efos.RFC = proveedores.RFC  WHERE situacion='Desvirtuado' AND  proveedores.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Desvirtuado2'] = count($agencias);

		$sql ="SELECT * FROM efos INNER JOIN proveedores_sin_cfdi ON efos.RFC = proveedores_sin_cfdi.RFC  WHERE situacion='Desvirtuado' AND  proveedores_sin_cfdi.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Desvirtuado2'] = $data['Desvirtuado2'] + count($agencias);


		$sql ="SELECT * FROM efos WHERE situacion='Sentencia Favorable'";
		$agencias = $this->db->query($sql)->result_array();
		$data['SentenciaFavorable'] = count($agencias);

		$sql ="SELECT * FROM efos INNER JOIN proveedores ON efos.RFC = proveedores.RFC WHERE situacion='Sentencia Favorable' AND  proveedores.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['SentenciaFavorable2'] = count($agencias);

		$sql ="SELECT * FROM efos INNER JOIN proveedores_sin_cfdi ON efos.RFC = proveedores_sin_cfdi.RFC WHERE situacion='Sentencia Favorable' AND  proveedores_sin_cfdi.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['SentenciaFavorable2'] = $data['SentenciaFavorable2']  + count($agencias);

		$sql ="SELECT * FROM efos WHERE situacion='Presunto'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Presunto'] = count($agencias);

		$sql ="SELECT * FROM efos INNER JOIN proveedores ON efos.RFC = proveedores.RFC WHERE situacion='Presunto' AND  proveedores.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Presunto2'] = count($agencias);

		$sql ="SELECT * FROM efos INNER JOIN proveedores_sin_cfdi ON efos.RFC = proveedores_sin_cfdi.RFC WHERE situacion='Presunto' AND  proveedores_sin_cfdi.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Presunto2'] = $data['Presunto2'] + count($agencias);


		$sql ="SELECT * FROM efos WHERE situacion='Definitivo'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Definitivo'] = count($agencias);


		$sql ="SELECT * FROM efos INNER JOIN proveedores ON efos.RFC = proveedores.RFC WHERE situacion='Definitivo' AND  proveedores.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Definitivo2'] = count($agencias);


		$sql ="SELECT * FROM efos INNER JOIN proveedores_sin_cfdi ON efos.RFC = proveedores_sin_cfdi.RFC WHERE situacion='Definitivo' AND  proveedores_sin_cfdi.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Definitivo2'] = $data['Definitivo2'] + count($agencias);


		


		$sql ="SELECT efosActualiza FROM  users WHERE RFC = '".$_SESSION['SEUS']['RFC']."'";
		$efosActualiza = $this->db->query($sql)->result_array();
		$data['efosActualiza'] = $efosActualiza[0];

		$this->view2("proveedores/efos", $data);
	}

	public function efos69(){
		$data = array();
		$data = array();
		$this->load->database();

		/* Prueba de conexion a la DB */
		$sql ="SELECT * FROM efos69 WHERE situacion='CONDONADOS' ";
		$agencias = $this->db->query($sql)->result_array();
		$data['Condonados'] = count($agencias);
		
		$sql ="SELECT * FROM efos69 INNER JOIN proveedores ON efos69.RFC = proveedores.RFC  WHERE situacion='CONDONADOS' AND  proveedores.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Condonados2'] = count($agencias);
		
		$sql ="SELECT * FROM efos69 WHERE situacion='CANCELADOS' ";
		$agencias = $this->db->query($sql)->result_array();
		$data['Cancelados'] = count($agencias);

		$sql ="SELECT * FROM efos69 INNER JOIN proveedores_sin_cfdi ON efos69.RFC = proveedores_sin_cfdi.RFC  WHERE situacion='CANCELADOS' AND  proveedores_sin_cfdi.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Cancelados2'] = count($agencias);

		
		$sql ="SELECT * FROM efos69 WHERE situacion='RETORNO INVERSIONES'";
		$agencias = $this->db->query($sql)->result_array();
		$data['RetornoInversiones'] = count($agencias);

		$sql ="SELECT * FROM efos69 INNER JOIN proveedores ON efos69.RFC = proveedores.RFC WHERE situacion='RETORNO INVERSIONES' AND  proveedores.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['RetornoInversiones2'] = count($agencias);

		$sql ="SELECT * FROM efos69 WHERE situacion='ELIMINADOS DE NO LOCALIZADOS'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Elimiandos'] = count($agencias);

		$sql ="SELECT * FROM efos69 INNER JOIN proveedores ON efos69.RFC = proveedores.RFC WHERE situacion='ELIMINADOS DE NO LOCALIZADOS' AND  proveedores.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Eliminados2'] = count($agencias);

		$sql ="SELECT * FROM efos69 WHERE situacion='FIRMES'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Firmes'] = count($agencias);


		$sql ="SELECT * FROM efos69 INNER JOIN proveedores ON efos69.RFC = proveedores.RFC WHERE situacion='FIRMES' AND  proveedores.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Firmes2'] = count($agencias);

		$sql ="SELECT * FROM efos69 WHERE situacion='NO LOCALIZADOS'";
		$agencias = $this->db->query($sql)->result_array();
		$data['NoLocalizados'] = count($agencias);


		$sql ="SELECT * FROM efos69 INNER JOIN proveedores ON efos69.RFC = proveedores.RFC WHERE situacion='NO LOCALIZADOS' AND  proveedores.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['NoLocalizados2'] = count($agencias);

		$sql ="SELECT * FROM efos69 WHERE situacion='SENTENCIAS'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Sentencias'] = count($agencias);


		$sql ="SELECT * FROM efos69 INNER JOIN proveedores ON efos69.RFC = proveedores.RFC WHERE situacion='SENTENCIAS' AND  proveedores.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['Sentencias2'] = count($agencias);

		
		$sql ="SELECT efosActualiza FROM  users WHERE RFC = '".$_SESSION['SEUS']['RFC']."'";
		$efosActualiza = $this->db->query($sql)->result_array();
		$data['efosActualiza'] = $efosActualiza[0];


		$this->view2("proveedores/efos69",$data);
	}

	public function cfdi(){

		$data = array();
		$this->load->database();

		if(!empty($_POST)){

		

		


			try {
				$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_cfdis.aspx?wsdl";
				$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_cfdis.aspx";
				$client = new SoapClient($url, [] );
				//var_dump($client->__getFunctions()); 
				//var_dump($client->__getTypes()); 
				$params = array(
					"Tipo_com" => $_POST['tipoComprobante'],
					"Rfc" => $_SESSION['SEUS']['RFC'],
					"Inidate" =>  $_POST['fecha1'],
					"Findate" =>  $_POST['fecha2'],
				  );

				  ini_set('memory_limit', '-1');
				  $client->__setLocation($url2);
				  $response = $client->Execute($params);

	
				if(!empty($response) && isset($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS)){
				
					$tr = "";
					$montototal = 0;
					$montoGrafica = array();
					$fechaGrafica = array();


					if(isset($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT)){
						$montototal += $response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT; 
					
						if($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATEST == 'V'){
							$estatus = "Vigente";
						}else{
							$estatus = "Cancelado";
						}

						$dt = new DateTime($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATFEC);

						if(isset($montoGrafica[$dt->format('d-m-Y')])){
							$montoGrafica[$dt->format('d-m-Y')] += $response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT;

						}else{
							$montoGrafica[$dt->format('d-m-Y')] = $response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT;
							$montoGrafica[$dt->format('d-m-Y')] = $response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT;
							$fechaGrafica[$dt->format('d-m-Y')] = "'".$dt->format('d/m/Y')."'";
						}

						
				
				
						 $tr .= '
							<tr class=" text-center">
								<td>'.$response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATERFC.'</td>
								<td>'.$response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATRRFC.'</td>
								<td>'.$response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATFOL.'</td>
								<td>'.$dt->format('d-m-Y').'</td>
								<td>$ '.number_format($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT,2).'</td>
								<td>'.$estatus.'</td>
							</tr>
						';

						$data['totales'] = 1;

					
				
				

					}else{

						foreach ($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS as $key => $value) {

										$montototal += $value->FEMVSATTOT; 

										if($value->FEMVSATEST == 'V'){
											$estatus = "Vigente";
										}else{
											$estatus = "Cancelado";
										}

										$dt = new DateTime($value->FEMVSATFEC);

										if(isset($montoGrafica[$dt->format('d-m-Y')])){
											$montoGrafica[$dt->format('d-m-Y')] += $value->FEMVSATTOT;

										}else{
											$montoGrafica[$dt->format('d-m-Y')] = $value->FEMVSATTOT;
											$montoGrafica[$dt->format('d-m-Y')] = $value->FEMVSATTOT;
											$fechaGrafica[$dt->format('d-m-Y')] = "'".$dt->format('d/m/Y')."'";
										}

										
								
								
										$tr .= '
											<tr class=" text-center">
												<td>'.$value->FEMVSATERFC.'</td>
												<td>'.$value->FEMVSATRRFC.'</td>
												<td>'.$value->FEMVSATFOL.'</td>
												<td>'.$dt->format('d-m-Y').'</td>
												<td>$ '.number_format($value->FEMVSATTOT,2).'</td>
												<td>'.$estatus.'</td>
											</tr>
										';
						}


						$data['totales'] = count($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS);
					}

		
				

							ksort($montoGrafica);
							ksort($fechaGrafica);
							$separado_por_comas1 = implode(",", $montoGrafica);
							$data['encabezadoGrafica'] = $separado_por_comas1;
							
							$separado_por_comas2 = implode(",", $fechaGrafica);
							$data['detalleGrafica'] = $separado_por_comas2;
							//dd($data,false);

							//dd($montoGrafica,false);
							//dd($fechaGrafica);
							$tabla = '
							<table id="example1" class="table  table-hover table-bordered">   
							<thead>
							<tr class=" text-center">
								<td >
									<span class="title text-center"><B>RFC Emisor</B></span>
								</td>
								<td >
									<span class="title text-center"><B>RFC Receptor</B></span>
								</td>
								<td >
									<span class="title text-center"><B>Folio</B></span>
								</td>
								<td >
									<span class="title text-center"><B>Fecha Comprobante</B></span>
								</td>
								<td >
									<span class="title text-center"><B>Total</B></span>
								</td>
								<td >
									<span class="title text-center"><B>Estatus SAT</B></span>
								</td>
							</tr>
						</thead>
									<tbody>
								
										'.$tr.' 
									
									</tbody>
							</table>';

							$data['detalle'] = $tabla; 
							$data['mostrarGrafica'] = 1; 

					
							$data['montoTotales'] = $montototal;

							$data['error'] = 0;

							$data['fechaInicio'] =$_POST['fecha1'];
							$data['fechaFin'] = $_POST['fecha2'];
					


	
				}else{
				  $tabla = '
				  ';
	  
				  $data['detalle'] = $tabla; 
				  $data['mostrarGrafica'] = 0; 
				  $data['montoTotales'] = 0;
				  $data['totales'] = 0;

				  $data['encabezadoGrafica'] = "";
				  $data['detalleGrafica'] = "";

				  $data['error'] = 1;
				  $data['fechaInicio'] =$_POST['fecha1'];
				  $data['fechaFin'] = $_POST['fecha2'];
				  
  
				}
			} catch (Exception $exc) {
				//echo 'Fatal exception caught: '.$exc->getMessage();
				//die;
				$tabla = '
				  
				  ';
	  
				  $data['detalle'] = $tabla; 
				  $data['mostrarGrafica'] = 0; 
				  $data['montoTotales'] = 0;
				  $data['totales'] = 0;
				  $data['encabezadoGrafica'] = "";
				  $data['detalleGrafica'] = "";
				  $data['error'] = 1;
				  $data['fechaInicio'] = "";
					$data['fechaFin'] = "";
			} catch (Error $err) {
				//echo 'Fatal error caught: '.$err->getMessage();
				//die;
				$tabla = '
				 
				  ';
	  
				  $data['detalle'] = $tabla;
				  $data['mostrarGrafica'] = 0;  
				  $data['montoTotales'] = 0;
				  $data['totales'] = 0;
				  $data['encabezadoGrafica'] = "";
				  $data['detalleGrafica'] = "";
				  $data['error'] = 1;
				  $data['fechaInicio'] = "";
					$data['fechaFin'] = "";
			}
			  

			 

			
		}else{
		
			$tabla = '';

			$data['detalle'] = $tabla; 
			$data['mostrarGrafica'] = 0; 
			$data['montoTotales'] = 0;
			$data['totales'] = 0;
			$data['encabezadoGrafica'] = "";
				  $data['detalleGrafica'] = "";
				  $data['error'] = 0;
				  $data['fechaInicio'] = "";
					$data['fechaFin'] = "";
			
		}

		
		
		$this->view2("proveedores/cfdi", $data);

	}



	public function sincfdi(){

		$data = array();
		$this->load->database();		
					
		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."' ";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = $agencias;

		if(isset($_SESSION['ArchivoGuardado']) && $_SESSION['ArchivoGuardado'] == 1){
			$data['ArchivoGuardado'] = 1;
			$this->load->library('session');	
			$this->session->unset_userdata('ArchivoGuardado');
		}else{
			$data['ArchivoGuardado'] = 0;
		}


		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='Positivo'  AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivo'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='SinRespuesta'  AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuesta'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='Negativo'  AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativo'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."' AND (Correo = '' OR Correo IS NULL) ";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresInactivos'] = count($agencias);


		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores2'] = count($agencias);
		
		$data['mostratTodos'] = 1;
		
		$this->view2("proveedores/sincfdi", $data);

	}


	public function historico32d(){
		$data = array();
		$this->load->database();

		if(!empty($_POST)){


	
			//$fecha1 = date('Y-m-d', strtotime($_POST['fecha1']));
			//$fecha2 = date('Y-m-d', strtotime($_POST['fecha2']));
			$ano = $_POST['ano'];

			$sql2 ="SELECT efos_historicos_resumen.*, proveedores.tipoProveedor FROM  efos_historicos_resumen 
			INNER JOIN proveedores ON efos_historicos_resumen.RFC = proveedores.RFC
			WHERE efos_historicos_resumen.RFC='".$_POST['RFC']."'  
			and efos_historicos_resumen.ano = '".$ano."' 
			and efos_historicos_resumen.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";

			$proveedores = $this->db->query($sql2)->result_array();

		

			if(!empty($proveedores)){
				$tr ="";

				
				foreach ($proveedores as $key => $value) {
					$img="";

					if($value['Situacion'] == 'Positivo'){
						$status = 'Cumplimiento Positivo';
					} elseif($value['Situacion'] == 'SinRespuesta'){
						$status = 'Sin Respuesta Portal SAT / Proveedor';
					}else{
						$status = 'Opinión del cumplimiento de obligaciones fiscales con inconsistencias';
					}

					$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FIN/aws_get_32d.aspx?wsdl";
					$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FIN/aws_get_32d.aspx";
					$client = new SoapClient($url, [] );
					//var_dump($client->__getFunctions()); 
					//var_dump($client->__getTypes()); 
					$params = array(
					  "Pruserrfc" => $value['RFC'],
					  "Pruser4anio" =>  $value['ano'],
					  "Pruser4mes" =>  $value['mes'],
					  );
					//dd($params,false);
			  
					  $client->__setLocation($url2);
					  $response = $client->Execute($params);

					 //dd($response->Result);

					  if(trim($response->Result) == 'No existe archivo para recuperar'){
							$img='<img src="'.base_url().'/inc/iconoPdfNegro.jpg" alt="icono-pdf"  style="width:40%" >';
					  }else{
					
							$img='
							<a href="'.$response->Result.'" target="_blank">
							<img src="'.base_url().'/inc/icono-pdf.png" alt="icono-pdf"  style="width:40%" >
							</a>
							';
					 }

							  $url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_url_32d.aspx?wsdl";
                              $url2 = "http://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_url_32d.aspx";
                              $client = new SoapClient($url, [] );
                              //var_dump($client->__getFunctions()); 
                              //var_dump($client->__getTypes()); 
                              $params = array(
								"Pruserrfc" => $value['RFC'],
								"Pruser4anio" =>  $value['ano'],
								"Pruser4mes" =>  $value['mes'],
								);
                      
                        
                                $client->__setLocation($url2);
                                $response = $client->Execute($params);
							

						if($response->Result == 'No existe url para recuperar'){
							$img2='';
						}else{
						
							$img2='
							<a href="'.$response->Result.'" target="_blank">
							<img src="'.base_url().'/inc/qr.png" alt="icono-pdf"  style="width:80%" >
							</a>
							';
						}


					$tr .= '
						<tr class=" text-center">
							<td>'.$value['Nombre'].'</td>
							<td>'.$value['RFC'].'</td>
							<td>'.$value['mes'].'-'.$value['ano'].'</td>
							<td>'.$value['tipoProveedor'].'</td>
							<td>'.$status.'</td>
							<td>'.$img.'</td>
							<td>'.$img2.'</td>

						</tr>
					';
				}
				

				$tabla = '
				<table id="example1"  class="table table-bordered table-striped">   
					<thead>
						<tr class=" text-center">
							<td >
								<span class="title text-center"><B>Razón Social</B></span>
							</td>
							<td >
								<span class="title text-center"><B>RFC</B></span>
							</td>
						
							<td >
							<span class="title text-center"><B>Fecha</B></span>
							</td>
							<td >
								<span class="title text-center"><B>Tipo Proveedor</B></span>
							</td>
							<td >
								<span class="title text-center"><B>Situación Fiscal</B> </span>
							</td>
							<td >
								<span class="title text-center"><B>Documento</B> </span>
							</td>
						
							<td >
								<span class="title text-center"><B>QR</B> </span>
							</td>
						</tr>
					</thead>
					<tbody>
			
						'.$tr.' 
			
					</tbody>
				</table>
				';

				$data['detalle'] = $tabla; 


			}else{

				$tabla = '
				';

				$data['detalle'] = $tabla; 

			}


		}else{

			$tabla = '
			';

			$data['detalle'] = $tabla; 

		}

		




		$this->view2("proveedores/3dhistorico", $data);
	}



	public function historicoConstancia(){

		$data = array();
		$this->load->database();

		if(!empty($_POST)){

			$fecha1 = date('Y-m-d', strtotime($_POST['fecha1']));
			$fecha2 = date('Y-m-d', strtotime($_POST['fecha2']));
		
			$sql2 ="SELECT * FROM  constancia_historicos 
			WHERE rfc='".$_POST['RFC']."'  
			and fecha >= '".$fecha1."' 
			and  fecha <= '".$fecha2."'
			and rfc_identy = '".$_SESSION['SEUS']['RFC']."'";

			$proveedores = $this->db->query($sql2)->result_array();

			if(!empty($proveedores)){
				$tr ="";

				
				foreach ($proveedores as $key => $value) {
					$img="";
					if($value['constancia'] == 1){
						$img='
						<div class="icon" align="center" style="cursor:pointer">
                             <img src="<?php echo base_url();?>/inc/icono-pdf.png" alt="icono-pdf"  style="width:7%" >
                               </div>
						';
					}

					$tr .= '
						<tr class=" text-center">
							<td>'.$value['razon_social'].'</td>
							<td>'.$value['rfc'].'</td>
							<td>'.$value['fecha'].'</td>
							<td>'.$value['tipoProveedor'].'</td>
							<td>'.$value['status'].'</td>
							<td>'.$img.'</td>

						</tr>
					';
				}
				

				$tabla = '
				<table id="example1"  class="table table-bordered table-striped">   
					<thead>
						<tr class=" text-center">
							<td >
								<span class="title text-center"><B>Razón Social</B></span>
							</td>
							<td >
								<span class="title text-center"><B>RFC</B></span>
							</td>
						
							<td >
							<span class="title text-center"><B>Fecha</B></span>
						</td>
						<td >
							<span class="title text-center"><B>Tipo Proveedor</B></span>
						</td>
							<td >
								<span class="title text-center"><B>Situación Fiscal</B> </span>
							</td>
							<td >
								<span class="title text-center"><B>Documento</B> </span>
							</td>
						
						</tr>
					</thead>
					<tbody>
						<tr>
						'.$tr.' 
							</tr>
					</tbody>
				</table>
				';

				$data['detalle'] = $tabla; 


			}else{

				$tabla = '
				<table id="example1"  class="table table-bordered table-striped">   
					<thead>
		
						<tr class=" text-center">
						<td >
							<span class="title text-center"><B>Razón Social</B></span>
						</td>
						<td >
							<span class="title text-center"><B>RFC</B></span>
						</td>
					
						<td >
						<span class="title text-center"><B>Fecha</B></span>
					</td>
					<td >
							<span class="title text-center"><B>Tipo Proveedor</B></span>
						</td>
						<td >
						<span class="title text-center"><B>Situación Fiscal</B> </span>
						</td>
						<td >
								<span class="title text-center"><B>Documento</B> </span>
							</td>
						
						</tr>
					</thead>
					<tbody>
						<tr>
								<td colspan="">Sin informacion</td>
								<td colspan="">Sin informacion</td>
								<td colspan="">Sin informacion</td>
								<td colspan="">Sin informacion</td>
								<td colspan="">Sin informacion</td>
								<td colspan="">Sin informacion</td>
							</tr>
					</tbody>
				</table>
				';

				$data['detalle'] = $tabla; 

			}


		}else{

			$tabla = '
			<table id="example1"  class="table table-bordered table-striped">   
				<thead>
				
					<tr class=" text-center">
							<td >
								<span class="title text-center"><B>Razón Social</B></span>
							</td>
							<td >
								<span class="title text-center"><B>RFC</B></span>
							</td>
						
							<td >
							<span class="title text-center"><B>Fecha</B></span>
						</td>
						<td >
							<span class="title text-center"><B>Tipo Proveedor</B></span>
						</td>
							<td >
							<span class="title text-center"><B>Situación Fiscal</B> </span>
							</td>
							<td >
								<span class="title text-center"><B>Documento</B> </span>
							</td>
					
					</tr>
				</thead>
				<tbody>
					<tr>
							<td colspan="">Sin informacion</td>
							<td colspan="">Sin informacion</td>
							<td colspan="">Sin informacion</td>
							<td colspan="">Sin informacion</td>
							<td colspan="">Sin informacion</td>
							<td colspan="">Sin informacion</td>
						</tr>
				</tbody>
			</table>
			';

			$data['detalle'] = $tabla; 

		}

		




		$this->view2("proveedores/historicoconstancia", $data);

	}


	public function historicoefos(){

		$data = array();
		$this->load->database();


		if(!empty($_POST)){

			if($_POST['tipoComprobante'] == 'todos'){

				$sql2 ="
				SELECT * FROM efos_historicos
				WHERE (efos_historicos.fecha >= '".$_POST['fecha1']."' AND efos_historicos.fecha <= '".$_POST['fecha2']."' )
				";

				$proveedores = $this->db->query($sql2)->result_array();

			}else{

				$sql2 ="
				SELECT * FROM efos_historicos
				WHERE (efos_historicos.fecha >= '".$_POST['fecha1']."' AND efos_historicos.fecha <= '".$_POST['fecha2']."' )
				AND efos_historicos.Situacion = '".$_POST['tipoComprobante']."'
				";

				$proveedores = $this->db->query($sql2)->result_array();

			}


			if(!empty($proveedores)){

				$tr="";

				foreach ($proveedores as $key => $value) {
					$tr.="<tr class=' text-center'>
						<td >".$value['RFC']."</td>
						<td >".$value['Nombre']."</td>
						<td >".$value['fecha']."</td>
						<td >".$value['Situacion']."</td>
					</tr>";
				}


				$tabla = '
					<table id="example1"  class="table table-bordered table-striped">   
						<thead>
							<tr class=" text-center">
								<td >
									<span class="title text-center"><B>RFC</B></span>
								</td>
								<td >
									<span class="title text-center"><B>Nombre Contribuyente</B></span>
								</td>
								<td >
									<span class="title text-center"><B>Fecha</B></span>
								</td>
								<td >
									<span class="title text-center"><B>Situacion</B></span>
								</td>
								
							
							</tr>
						</thead>
						<tbody>
							'.$tr.'
						</tbody>
					</table>
					';


			}else{
				$tabla = '
					';

					$data['detalle'] = $tabla; 

			}







		}else{

			$tabla = '
		';

		$data['detalle'] = $tabla; 

		}






		




		$this->view2("proveedores/efosistorico", $data);
		
	}

	public function efos_excel_relacionados(){
		
					$this->load->library('Excel');
	
					$objPHPExcel = new PHPExcel();
					// Set document properties

				
					$objPHPExcel->getProperties()->setCreator("ZilaModule")
							->setLastModifiedBy("ZilaModule")
							->setTitle('efosRelacionados')
							->setSubject('efosRelacionados')
							->setDescription("efosRelacionados");

						


					$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(50);
					$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFont()->setBold(true);
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1');
					$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);  

					
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', "\nEfos relacionados ");  
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);


					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:G2');
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H2:I2');
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J2:L2');

					$objPHPExcel->getActiveSheet()
								->getStyle('B2:L2')
								->getFill()
								->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
								->getStartColor()
								->setRGB('33C7FF');

					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A2', 'RFC')
					->setCellValue('B2', 'RAZON SOCIAL')
					->setCellValue('H2', 'ESTATUS')
					->setCellValue('J2', 'SITUACION');

					




					$objPHPExcel->getActiveSheet()->setTitle('Relacionados');

					// Set active sheet index to the first sheet, so Excel opens this as the first sheet
					$objPHPExcel->setActiveSheetIndex(0);
					
					// Redirect output to a client’s web browser (Excel2007)
					header('Content-Type: application/vnd.ms-excel');
					header("Content-Disposition: attachment;filename=efos_relacionados.xls");
					header('Cache-Control: max-age=0');
				
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					//die;
					
					$objWriter->save('php://output');


	}



	public function historicoefos2(){

		$data = array();
		$this->load->database();



		$data = array();
		$this->load->database();

		if(!empty($_POST)){



			if($_POST['tipoComprobante'] == 'todos'){

				$sql2 ="
				SELECT * FROM cfdis_historicos
				INNER JOIN efos_historicos ON 
				efos_historicos.RFC = cfdis_historicos.FEMVSATERFC
				WHERE (cfdis_historicos.FEMVSATFEC >= '".$_POST['fecha1']."' AND cfdis_historicos.FEMVSATFEC <= '".$_POST['fecha2']."' )
				AND (efos_historicos.fecha >= '".$_POST['fecha1']."' AND efos_historicos.fecha <= '".$_POST['fecha2']."')
				AND efos_historicos.rfc_identy = '".$_SESSION['SEUS']['RFC']."'
				";

				$proveedores = $this->db->query($sql2)->result_array();

			}else{

				$sql2 ="
				SELECT 
				cfdis_historicos.FEMVSATERFC, 
				cfdis_historicos.FEMVSATENOM,
				efos_historicos.Situacion,
				cfdis_historicos.FEMVSATSTOT,
				cfdis_historicos.FEMVSATIVA,
				efos_historicos.fecha as fecha
				FROM cfdis_historicos
				INNER JOIN efos_historicos ON 
				efos_historicos.RFC = cfdis_historicos.FEMVSATERFC
				WHERE (cfdis_historicos.FEMVSATFEC >= '".$_POST['fecha1']."' AND cfdis_historicos.FEMVSATFEC <= '".$_POST['fecha2']."' )
				AND (efos_historicos.fecha >= '".$_POST['fecha1']."' AND efos_historicos.fecha <= '".$_POST['fecha2']."')
				AND efos_historicos.Situacion = '".$_POST['tipoComprobante']."'
				AND efos_historicos.rfc_identy = '".$_SESSION['SEUS']['RFC']."'
				";

				$proveedores = $this->db->query($sql2)->result_array();

			}


			if(!empty($proveedores)){
				$tr ="";

								foreach ($proveedores as $key => $value) {
										$tr .= '
										<tr>
											
											<td>'.$value['FEMVSATENOM'].'</td>
											<td>'.$value['FEMVSATERFC'].'</td>
											<td>'.$value['fecha'].'</td>
											<td>'.$value['Situacion'].'</td>
											<td>'.$value['FEMVSATSTOT'].'</td>
											<td>'.$value['FEMVSATIVA'].'</td>
										</tr>
									';
								}

								$tabla = '
								<table id="example1"  class="table table-bordered table-striped">   
									<thead>
										<tr class=" text-center">
										<td >
										<span class="title text-center"><B>Razón Social</B></span>
									</td>
											<td >
												<span class="title text-center"><B>RFC</B></span>
											</td>
											<td >
												<span class="title text-center"><B>Fecha</B></span>
											</td>
											
											<td >
												<span class="title text-center"><B>Situación Fiscal</B></span>
											</td>
						
											<td >
												<span class="title text-center"><B>Valor Factura</B></span>
											</td>
						
											<td >
											<span class="title text-center"><B>IVA </B></span>
											</td>
											
										
										</tr>
									</thead>
									<tbody>
										'.$tr.'
									</tbody>
								</table>
								';
						
								$data['detalle'] = $tabla; 

			}else{

				$tabla = '
			
			';
	
			$data['detalle'] = $tabla; 

			}
			
								




		}else{

			$tabla = '
			
			';
	
			$data['detalle'] = $tabla; 

		}





	




		$this->view2("proveedores/efosistorico", $data);
		
	}


	public function configuracion(){

		$data = array();
		 $sql2 ="SELECT users.*, count(*) as total, log_ingresos.usuario as metodo 
		FROM users LEFT JOIN log_ingresos ON log_ingresos.usuario = users.use_session 
		WHERE 
		users.RFC = '".$_SESSION['SEUS']['RFC']."'
		GROUP BY use_session";
	
		$proveedores = $this->db->query($sql2)->result_array();
		$data['usuarios'] = $proveedores;


		$sql22 ="SELECT * FROM configuracion_frecuencia WHERE  configuracion_frecuencia.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$proveedores2 = $this->db->query($sql22)->result_array();
		$data['configuracion'] = $proveedores2;

		$sql221 ="SELECT * FROM configuracion_frecuencia2 WHERE  configuracion_frecuencia2.rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$proveedores21 = $this->db->query($sql221)->result_array();
		$data['configuracion2'] = $proveedores21;


		if(isset($_SESSION['ArchivoGuardado']) && $_SESSION['ArchivoGuardado'] == 1){
			$data['ArchivoGuardado'] = 1;
			$this->load->library('session');	
			$this->session->unset_userdata('ArchivoGuardado');
		}else{
			$data['ArchivoGuardado'] = 0;
		}


		$this->view2("proveedores/configuracion", $data);
	}

	public function deleteUsers($id){

		$this->db->where('use_id', $id);
		$this->db->delete('users');


		redirect(base_url()."proveedores/configuracion");

	}

	public function guardarUsuario(){

		
			$data = array();
			$mensaje = '
			<tablet>
			  <tr>
				<td colspan="2"><img src="'.base_url().'inc/logo.png" style="width:50%"/><td>
			  </tr>
			  <tr>
			  <td colspan="2"><br></td>
			   </tr>
			  <tr>
				 <td colspan="2"> Estimado(a) usuario tus accesos al sistema son los siguientes</td>
			  </tr>
			  <tr>
				 <td colspan="2" align="center"><a href="'.base_url().'" >'.base_url().'</a></td>
			  </tr>
			  
			  <tr>
				 <td >Usuario:</td>
				 <td align="center">'.$_POST['Usuario'].'</td>
			  </tr>
			  <tr>
				 <td >Password:</td>
				 <td align="center">'.$_POST['password'].'</td>
			  </tr>
			  <tr>
				<td colspan="2"><br><br><br></td>
			  </tr>
			   <tr>
				<td colspan="2"><br><br><br></td>
			  </tr>
			</tablet>
			';

			$this->load->library('email');
			$config['protocol']    = 'smtp';
			$config['smtp_host']    = 'smtp.sendgrid.net';
			$config['smtp_port']    = '587';
			$config['smtp_timeout'] = '7';
			$config['smtp_user']    = 'grupociosa';
			$config['smtp_pass']    = 'Ciosagrupo03';
			$config['charset']    = 'utf-8';
			$config['newline']    = "\r\n";
			$config['mailtype'] = 'html'; // or html
			$config['validation'] = TRUE; // bool whether to validate email or not      

				$this->email->initialize($config);
				$email = $_POST['correo'];
				
				$this->email->from('no-replay@zilamodule.com', 'Accesos ZILA Module');
		        $this->email->to($email);
		        $this->email->subject('Accesos ZILA Module');
		        $this->email->message($mensaje);
		        $this->email->send();
	
			/*
			//$pw = hash('sha256',$password);
			$this->load->library('email');
			$asunto = 'Usuario y contraseña ZILA-MODULE';
			 $mensaje = '
				<tablet>
				  <tr>
				    <td colspan="2"><img src="'.base_url().'inc/logo.png" style="width:50%"/><td>
				  </tr>
				  <tr>
				  <td colspan="2"><br></td>
			   	</tr>
				  <tr>
				     <td colspan="2"> Estimado(a) usuario tus accesos al sistema son los siguientes</td>
				  </tr>
				  <tr>
				     <td colspan="2" align="center"><a href="'.base_url().'" >'.base_url().'</a></td>
				  </tr>
				  
				  <tr>
				     <td >Usuario:</td>
				     <td align="center">'.$_POST['password'].'</td>
				  </tr>
				  <tr>
				     <td >Password:</td>
				     <td align="center">'.$this->input->post('keys', TRUE).'</td>
				  </tr>
				  <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				   <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				</tablet>
				';
	
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				$this->email->initialize($config);
				$email = $_POST['correo'];
				
				$this->email->from('no-replay@zilamodule.com', 'SISTEMA');
		        $this->email->to($email);
		        $this->email->subject($asunto);
		        $this->email->message($mensaje);
		        $this->email->send();

				//dd($this->email);
				*/

				//mailSend('Zila-module','no-replay@zilamodule.com',$_POST['correo'],'Usuario Creado',$mensaje);
		
		$data = array(
			'use_login' => $_POST['Usuario'],
			'use_session' => $_POST['Usuario'],
			'use_email' => $_POST['correo'],
			'use_typ_id' => $_POST['tipoUser'],
			'RFC' => $_SESSION['SEUS']['RFC'],
			'use_active' => 1,
			'use_key' =>hash('sha256',$_POST['password'])
		);

		$this->db->insert('users', $data);

		redirect(base_url()."proveedores/configuracion");

	}

	

	public function guardarFrecuencia(){

		$data = array();
		//$pw = hash('sha256',$password);


		$this->db->delete('configuracion_frecuencia', array('rfc_identy' => $_SESSION['SEUS']['RFC']));
		if($_POST['frecuencua'] == 'Mes'){

			$data = array(
				'frecuencia' => 'Mes',
				'diames' => 11,
				'rfc_identy' => $_SESSION['SEUS']['RFC'],
				'diasemana' => '',
				'texto' => $_POST['exampleFormControlTextarea1'],
				'recordatorio' => $_POST['exampleFormControlTextarea123']
			);
	
			$this->db->insert('configuracion_frecuencia', $data);

		}


		if($_POST['frecuencua'] == 'Dia'){

			$data = array(
				'frecuencia' => $_POST['frecuencua'],
				'diames' => '',
				'rfc_identy' => $_SESSION['SEUS']['RFC'],
				'diasemana' => $_POST['diaSemana'],
				'texto' => $_POST['exampleFormControlTextarea1']
			);
	
			$this->db->insert('configuracion_frecuencia', $data);

		}

	

		$this->db->delete('configuracion_frecuencia2', array('rfc_identy' => $_SESSION['SEUS']['RFC']));
		$data2 = array(
				'frecuencia' => $_POST['frecuencua2'],
				'rfc_identy' => $_SESSION['SEUS']['RFC'],
				'texto' => $_POST['exampleFormControlTextarea12']
			);

	
	
			$this->db->insert('configuracion_frecuencia2', $data2);

		

		redirect(base_url()."proveedores/configuracion");

	}


	public function excelProveedores(){


		$this->load->library('Excel');

		$objPHPExcel = new PHPExcel();
		// Set document properties


		$objPHPExcel->getProperties()->setCreator("ZilaModule")
				->setLastModifiedBy("ZilaModule")
				->setTitle('proveedoresSinCFDIS')
				->setSubject('proveedoresSinCFDIS')
				->setDescription("proveedoresSinCFDIS");

			


		$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(10);
		$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');
		$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);  


		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1', "\nPlantilla Correos");  
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);





					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A2', 'RFC')
					->setCellValue('B2', 'RAZON SOCIAL')
					->setCellValue('C2', 'CORREO');

		$sql ="SELECT * FROM proveedores WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = ($agencias);

		$y = 3;
		foreach ($data['proveedores'] as $key => $value) {

			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A' . $y, $value['RFC'])
			->setCellValue('B' . $y, $value['RazonSocial'])
			->setCellValue('C' . $y, $value['Correo']);

			$y++;

		}

		$objPHPExcel->getActiveSheet()->setTitle('ProveedoresSinCFDIS');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);
            
            // Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=PlantillaCorreos.xls");
            header('Cache-Control: max-age=0');
        
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            //die;
            
            $objWriter->save('php://output');

		/*
		$filename = "PlantillaCorreos.xls";

		$sql ="SELECT * FROM proveedores WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = ($agencias);

		$tr ="";

								foreach ($data['proveedores'] as $key => $value) {
										$tr .= '
										<tr>
											
											<td>'.$value['RFC'].'</td>
											<td>'.$value['RazonSocial'].'</td>
											<td>'.$value['Correo'].'</td>
										</tr>
									';
								}

								header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
								header("Content-type:   application/x-msexcel; charset=utf-8");
								header("Content-Disposition: attachment; filename=".$filename); 
								header("Expires: 0");
								header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
								header("Cache-Control: private",false);

		echo $tabla = '
			<table id="example1"  class="table table-bordered table-striped">   
				<thead>
					<tr class=" text-center">

						<td >
							<span class="title text-center"><B>RFC</B></span>
						</td>

			
						
						<td >
							<span class="title text-center"><B>Nombre Proveedor</B></span>
						</td>
	
						<td >
							<span class="title text-center"><B>Correo</B></span>
						</td
					</tr>
				</thead>
				<tbody>
				'.$tr.'
				</tbody>
			</table>
			';

			exit;

			*/
	}

	/*
	  public function excelProveedoressinCFDI(){

		$this->load->library('Excel');


		$objPHPExcel = new PHPExcel();
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("ZilaModule")
				   ->setLastModifiedBy("ZilaModule")
				   ->setTitle('FacturasNuevas')
				   ->setSubject('FacturasNuevas')
				   ->setDescription("Facturas Nuevas");


		$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(50);
		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);  
				

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A2', 'RFC')
		->setCellValue('B2', 'Nombre')
		->setCellValue('C2', 'Tipo Proveedor')
		->setCellValue('D2', 'Correo');




		$i = 2;
		$sql ="SELECT * FROM  proveedores_sin_cfdi WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = ($agencias);
		foreach ($data['proveedores'] as $key => $value) {

			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A' . $i, $value['RFC'])
						->setCellValue('B' . $i, $value['RazonSocial'])
						->setCellValue('C' . $i, $value['tipoProveedor'])
						->setCellValue('D' . $i, $value['Correo']);



			$i++;
		}


		
		$objPHPExcel->getActiveSheet()->setTitle('Facturas Nuevas');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.ms-excel');
		header("Content-Disposition: attachment;filename=proveedoresSincfdi.xls");
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		$objWriter->save('php://output');

	


	

	}

	 */


	public function excelProveedoressinCFDI(){

		$this->load->library('Excel');

        $objPHPExcel = new PHPExcel();
        // Set document properties

    
        $objPHPExcel->getProperties()->setCreator("ZilaModule")
                ->setLastModifiedBy("ZilaModule")
                ->setTitle('proveedoresSinCFDIS')
                ->setSubject('proveedoresSinCFDIS')
                ->setDescription("proveedoresSinCFDIS");

            


        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(50);
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);  

        
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "\nProveedores sin CFDI´S ");  
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
  



					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A2', 'RFC')
					->setCellValue('B2', 'RAZON SOCIAL')
					->setCellValue('C2', 'TIPO PROVEEDOR')
					->setCellValue('D2', 'CORREO');
					
					$sql ="SELECT * FROM  proveedores_sin_cfdi WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
					$agencias = $this->db->query($sql)->result_array();
					$data['proveedores'] = ($agencias);

					$y = 3;
					foreach ($data['proveedores'] as $key => $value) {

						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A' . $y, $value['RFC'])
						->setCellValue('B' . $y, $value['RazonSocial'])
						->setCellValue('C' . $y, $value['tipoProveedor'])
						->setCellValue('D' . $y, $value['Correo']);

						$y++;

					}


					$objPHPExcel->getActiveSheet()->setTitle('ProveedoresSinCFDIS');

					// Set active sheet index to the first sheet, so Excel opens this as the first sheet
					$objPHPExcel->setActiveSheetIndex(0);
					
					// Redirect output to a client’s web browser (Excel2007)
					header('Content-Type: application/vnd.ms-excel');
					header("Content-Disposition: attachment;filename=ProveedoresSinCFDI.xls");
					header('Cache-Control: max-age=0');
				
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					//die;
					
					$objWriter->save('php://output');
			
		/*

		$filename = "librosProveedoresSinCFDI.xls";

		$sql ="SELECT * FROM  proveedores_sin_cfdi WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = ($agencias);

		$tr ="";

								foreach ($data['proveedores'] as $key => $value) {
										$tr .= '
										<tr>
											
											<td>'.$value['RFC'].'</td>
											<td>'.$value['RazonSocial'].'</td>
											<td>'.$value['tipoProveedor'].'</td>
											<td>'.$value['Correo'].'</td>
										</tr>
									';
								}

								header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
								header("Content-type:   application/x-msexcel; charset=utf-8");
								header("Content-Disposition: attachment; filename=".$filename); 
								header("Expires: 0");
								header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
								header("Cache-Control: private",false);


		echo $tabla = '
			<table id="example1"  class="table table-bordered table-striped">   
				<thead>
					<tr class=" text-center">

						<td >
							<span class="title text-center"><B>RFC</B></span>
						</td>

			
						
						<td >
							<span class="title text-center"><B>Nombre</B></span>
						</td>
						<td >
						<span class="title text-center"><B>Tipo Proveedor</B></span>
						</td
						<td >
							<span class="title text-center"><B>Correo</B></span>
						</td

					</tr>
				</thead>
				<tbody>
				'.$tr.'
				</tbody>
			</table>
			';

			exit;
			*/
	}


	public function guardarCorreos(){
		$this->load->library('excel');
	
		$tmp_fichero = $_FILES['subirPlantilla']['tmp_name'];
		$name        = $_FILES['subirPlantilla']['name'];
		$this->excel = PHPExcel_IOFactory::load($tmp_fichero);

		$worksheet   = $this->excel->getActiveSheet();
		$highestRow = $worksheet->getHighestRow();
		$highestColumn      = $worksheet->getHighestColumn();

	
		for ( $row = 3; $row <= $highestRow; ++$row )
		{
			$cell = $worksheet->getCellByColumnAndRow( 0, $row );
      		$val0 = trim( $cell->getValue() );

			$cell = $worksheet->getCellByColumnAndRow( 1, $row );
      		$val1 = trim( $cell->getValue() );

			$cell = $worksheet->getCellByColumnAndRow( 2, $row );
      		$val2 = trim( $cell->getValue() );


			$data = array();
			$this->load->database();
			$data = array(
				'Correo' => $val2
			);


			$this->db->where('RFC',$val0);
			$this->db->or_where('RazonSocial', $val1); 
			$this->db->update('proveedores', $data);		

		}

		$this->load->library('session');
		$this->session->set_userdata('ArchivoGuardado',1);

		redirect(base_url().'proveedores/configuracion');
	}


	public function guardarProveedores(){
		
		$this->load->library('excel');
	
		$tmp_fichero = $_FILES['subirPlantilla']['tmp_name'];
		$name        = $_FILES['subirPlantilla']['name'];
		$this->excel = PHPExcel_IOFactory::load($tmp_fichero);

		$worksheet   = $this->excel->getActiveSheet();
		$highestRow = $worksheet->getHighestRow();
		$highestColumn      = $worksheet->getHighestColumn();

		$this->db->delete('proveedores_sin_cfdi', array('rfc_identy' => $_SESSION['SEUS']['RFC']));
		for ( $row = 3; $row <= $highestRow; ++$row )
		{
			$cell = $worksheet->getCellByColumnAndRow( 0, $row );
      		$val0 = trim( $cell->getValue() );

			$cell = $worksheet->getCellByColumnAndRow( 1, $row );
      		$val1 = trim( $cell->getValue() );

			$cell = $worksheet->getCellByColumnAndRow( 2, $row );
      		$val2 = trim( $cell->getValue() );

			$cell = $worksheet->getCellByColumnAndRow( 3, $row );
      		$val3 = trim( $cell->getValue() );

			  $data = array();
		$this->load->database();
		$data = array(
			'NombreComercial' =>$val1,
			'nombre' => $val1,
			'tipoProveedor' => $val2,
			'RazonSocial' => $val1,
			'RFC' => $val0,
			'Correo' => $val3,
			'Estatus' => 'SinRespuesta',
			'fecha_constancia1' => date('Y-m-d'),
			'rfc_identy' => $_SESSION['SEUS']['RFC']
		);

		$this->db->insert('proveedores_sin_cfdi', $data);



		

		}

		$this->load->library('session');
		$this->session->set_userdata('ArchivoGuardado',1);

		redirect(base_url().'proveedores/sincfdi');
	}
	
	
}
// END Searchs controller

/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
