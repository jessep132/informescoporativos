<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * IT cordinador
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2020 - Grupo Solana
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 08-Junio-2020
 * 
*/


class Home extends MY_Controller 
{
    public function __construct()
    {
		parent::__construct();
		
	}
	

	
	public function index()
	{

		$data = array();
		$this->load->database();



		/* Prueba de conexion a la DB */
		$sql ="SELECT * FROM proveedores WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores'] = $data['proveedores'] + count($agencias);



		$sql ="SELECT * FROM proveedores WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."' AND  (Correo = '' OR Correo IS NULL) ";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresInactivos'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."' AND (Correo = '' OR Correo IS NULL) ";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresInactivos'] = $data['proveedoresInactivos'] + count($agencias);
		/******************************************************************************** */



		
		$sql ="SELECT * FROM proveedores WHERE Estatus='Positivo' AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivo'] = count($agencias);


		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='Positivo'  AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivo'] = $data['proveedoresPositivo'] + count($agencias);
        /******************************************************************************** */





		$sql ="SELECT * FROM proveedores WHERE Estatus='SinRespuesta' AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuesta'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='SinRespuesta'  AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuesta'] = $data['proveedoresSinRespuesta'] + count($agencias);
		/******************************************************************************** */



		$sql ="SELECT * FROM proveedores WHERE Estatus='Negativo' AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativo'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='Negativo'  AND Correo != '' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativo'] = $data['proveedoresNegativo'] + count($agencias);
		/******************************************************************************** */



		$sql ="SELECT * FROM proveedores WHERE Estatus='Positivo' AND tipoProveedor='Otros' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivoInsumos'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='Positivo' AND tipoProveedor='Otros' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivoInsumos'] = $data['proveedoresPositivoInsumos']  + count($agencias);


		$sql ="SELECT * FROM proveedores WHERE Estatus='SinRespuesta' AND tipoProveedor='Otros' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuestaInsumos'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='SinRespuesta' AND tipoProveedor='Otros' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuestaInsumos'] = $data['proveedoresSinRespuestaInsumos'] + count($agencias);




		$sql ="SELECT * FROM proveedores WHERE Estatus='Negativo' AND tipoProveedor='Otros' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativoInsumos'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='Negativo' AND tipoProveedor='Otros' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativoInsumos'] = $data['proveedoresNegativoInsumos'] + count($agencias);









		$sql ="SELECT * FROM proveedores WHERE Estatus='Positivo' AND tipoProveedor='Servicios' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivoServicio'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='Positivo' AND tipoProveedor='Servicios' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivoServicio'] = $data['proveedoresPositivoServicio'] + count($agencias);


		$sql ="SELECT * FROM proveedores WHERE Estatus='SinRespuesta' AND tipoProveedor='Servicios' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuestaServicio'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='SinRespuesta' AND tipoProveedor='Servicios' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuestaServicio'] = $data['proveedoresSinRespuestaServicio'] + count($agencias);


		$sql ="SELECT * FROM proveedores WHERE Estatus='Negativo' AND tipoProveedor='Servicios' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativoServicio'] = count($agencias);

		$sql ="SELECT * FROM proveedores_sin_cfdi WHERE Estatus='Negativo' AND tipoProveedor='Servicios' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresNegativoServicio'] = $data['proveedoresNegativoServicio']  + count($agencias);


		$sql ="SELECT * FROM  cfdis_dashborad WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();

		

		if(!empty($agencias)){
			$data['totalrecibidos'] = ($agencias[0]['total_recibidos']);
			$data['montorecibidos'] = ($agencias[0]['monto_recibidos']);
			$data['montoremitidos'] = ($agencias[0]['monto_emitidos']);
			$data['totalemitidos'] = ($agencias[0]['total_emitidos']);
		}else{
			$data['totalrecibidos'] = 0;
			$data['montorecibidos'] = 0;
			$data['montoremitidos'] = 0;
			$data['totalemitidos'] = 0;
		}

		$mesActual = date('m');
		$fecha_actual = date('Y-m-d');
		$fechamesActualIni = date('Y').'-'.$mesActual.'-01';
		$fechamesActualFin = date("Y-m-t", strtotime($fecha_actual));

		$sql2 ="
		SELECT * FROM cfdis_historicos
		INNER JOIN efos_historicos ON 
		efos_historicos.RFC = cfdis_historicos.FEMVSATERFC
		WHERE (cfdis_historicos.FEMVSATFEC >= '".$fechamesActualIni."' AND cfdis_historicos.FEMVSATFEC <= '".$fechamesActualFin."' )
		AND (efos_historicos.fecha >= '".$fechamesActualIni."' AND efos_historicos.fecha <= '".$fechamesActualFin."')
		AND cfdis_historicos.rfc_identy = '".$_SESSION['SEUS']['RFC']."'
		";

		$proveedoresmesActual = $this->db->query($sql2)->result_array();

		$tr = '';

	    if(!empty($proveedoresmesActual)){
			foreach ($proveedoresmesActual as $key => $value) {
				$tr .= ' <tr>
					<td colspan=""><FONT SIZE="1px">'.$value['FEMVSATENOM'].'</font></td>
					<td colspan=""><FONT SIZE="1px">'.$value['Situacion'].'</font></td>
					<td colspan=""><FONT SIZE="1px">'.$value['FEMVSATSTOT'].'</font></td>
					<td colspan=""><FONT SIZE="1px">'.$value['FEMVSATIVA'].'</font></td>

				</tr>';
				
			}

		}else{
			$tr = ' <tr>
			<td colspan=""><FONT SIZE="1px"></font></td>
			<td colspan=""><FONT SIZE="1px"></font></td>
			<td colspan=""><FONT SIZE="1px"></font></td>
			<td colspan=""><FONT SIZE="1px"></font></td>

		  </tr>';
		}

		$data['proveedoresmesActual'] = $tr;


		switch ($mesActual) {
			case '01':
				$mesA = 'ENERO';
				break;
			case '02':
				$mesA = 'FEBRERO';
				break;
			case '03':
				$mesA = 'MARZO';
				break;
			case '04':
				$mesA = 'ABRIL';
				break;
			case '05':
				$mesA = 'MAYO';
				break;
			case '06':
				$mesA = 'JUNIO';
				break;
			case '07':
				$mesA = 'JULIO';
				break;
			case '08':
				$mesA = 'AGOSTO';
				break;
			case '09':
				$mesA = 'SEPTIEMBRE';
				break;
			case '10':
				$mesA = 'OCTUBRE';
				break;
			case '11':
				$mesA = 'NOVIEMBRE';
				break;
			case '12':
				$mesA = 'DICIEMBRE';
				break;
		}

		$fecha_actual = date('d-m-Y');
		$mesAnterior = date("m",strtotime($fecha_actual. "- 1 month"));
		$mesAnterior2 = date("Y-m-d",strtotime($fecha_actual. "- 1 month"));
		$anomesAnterior2 = date("Y",strtotime($fecha_actual. "- 1 month"));
		$fechamesAnteriorPreIni = $anomesAnterior2.'-'.$mesAnterior.'-01';
		$fechamesAnteriorPreFin = date("Y-m-t", strtotime($mesAnterior2));

		
		$sql2 ="
		SELECT * FROM cfdis_historicos
		INNER JOIN efos_historicos ON 
		efos_historicos.RFC = cfdis_historicos.FEMVSATERFC
		WHERE (cfdis_historicos.FEMVSATFEC >= '".$fechamesAnteriorPreIni."' AND cfdis_historicos.FEMVSATFEC <= '".$fechamesAnteriorPreFin."' )
		AND (efos_historicos.fecha >= '".$fechamesAnteriorPreIni."' AND efos_historicos.fecha <= '".$fechamesAnteriorPreFin."')
		AND cfdis_historicos.rfc_identy = '".$_SESSION['SEUS']['RFC']."'
		";

		$proveedoresAnteriorPre = $this->db->query($sql2)->result_array();

		$tr = '';

	    if(!empty($proveedoresAnteriorPre)){
			foreach ($proveedoresAnteriorPre as $key => $value) {
				$tr .= ' <tr>
					<td colspan=""><FONT SIZE="1px">'.$value['FEMVSATENOM'].'</font></td>
					<td colspan=""><FONT SIZE="1px">'.$value['Situacion'].'</font></td>
					<td colspan=""><FONT SIZE="1px">'.$value['FEMVSATSTOT'].'</font></td>
					<td colspan=""><FONT SIZE="1px">'.$value['FEMVSATIVA'].'</font></td>

				</tr>';
				
			}

		}else{
			$tr = ' <tr>
			<td colspan=""><FONT SIZE="1px"></font></td>
			<td colspan=""><FONT SIZE="1px"></font></td>
			<td colspan=""><FONT SIZE="1px"></font></td>
			<td colspan=""><FONT SIZE="1px"></font></td>

		  </tr>';
		}

		$data['proveedoresAnterior'] = $tr;

		switch ($mesAnterior) {
			case '01':
				$mesAn = 'ENERO';
				break;
			case '02':
				$mesAn = 'FEBRERO';
				break;
			case '03':
				$mesAn = 'MARZO';
				break;
			case '04':
				$mesAn = 'ABRIL';
				break;
			case '05':
				$mesAn = 'MAYO';
				break;
			case '06':
				$mesAn = 'JUNIO';
				break;
			case '07':
				$mesAn = 'JULIO';
				break;
			case '08':
				$mesAn = 'AGOSTO';
				break;
			case '09':
				$mesAn = 'SEPTIEMBRE';
				break;
			case '10':
				$mesAn = 'OCTUBRE';
				break;
			case '11':
				$mesAn = 'NOVIEMBRE';
				break;
			case '12':
				$mesAn = 'DICIEMBRE';
				break;
		}

		$fecha_actual2 = date('d-m-Y');
		$mesAnteriorPre = date("m",strtotime($fecha_actual2. "- 2 month"));
		$mesAnteriorPre2 = date("Y-m-d",strtotime($fecha_actual2. "- 2 month"));

		$anomesAnteriorPre = date("Y",strtotime($fecha_actual2. "- 2 month"));

		$fechamesAnteriorPreIni = $anomesAnteriorPre.'-'.$mesAnteriorPre.'-01';
		$fechamesAnteriorPreFin = date("Y-m-t", strtotime($mesAnteriorPre2));

		
		$sql2 ="
		SELECT * FROM cfdis_historicos
		INNER JOIN efos_historicos ON 
		efos_historicos.RFC = cfdis_historicos.FEMVSATERFC
		WHERE (cfdis_historicos.FEMVSATFEC >= '".$fechamesAnteriorPreIni."' AND cfdis_historicos.FEMVSATFEC <= '".$fechamesAnteriorPreFin."' )
		AND (efos_historicos.fecha >= '".$fechamesAnteriorPreIni."' AND efos_historicos.fecha <= '".$fechamesAnteriorPreFin."')
		AND cfdis_historicos.rfc_identy = '".$_SESSION['SEUS']['RFC']."'
		";

		$proveedoresAnteriorPre = $this->db->query($sql2)->result_array();

		$tr = '';

	    if(!empty($proveedoresAnteriorPre)){
			foreach ($proveedoresAnteriorPre as $key => $value) {
				$tr .= ' <tr>
					<td colspan=""><FONT SIZE="1px">'.$value['FEMVSATENOM'].'</font></td>
					<td colspan=""><FONT SIZE="1px">'.$value['Situacion'].'</font></td>
					<td colspan=""><FONT SIZE="1px">'.$value['FEMVSATSTOT'].'</font></td>
					<td colspan=""><FONT SIZE="1px">'.$value['FEMVSATIVA'].'</font></td>

				</tr>';
				
			}

		}else{
			$tr = ' <tr>
			<td colspan=""><FONT SIZE="1px"></font></td>
			<td colspan=""><FONT SIZE="1px"></font></td>
			<td colspan=""><FONT SIZE="1px"></font></td>
			<td colspan=""><FONT SIZE="1px"></font></td>

		  </tr>';
		}

		$data['proveedoresAnteriorPre'] = $tr;

		switch ($mesAnteriorPre) {
			case '01':
				$mesAnp = 'ENERO';
				break;
			case '02':
				$mesAnp = 'FEBRERO';
				break;
			case '03':
				$mesAnp = 'MARZO';
				break;
			case '04':
				$mesAnp = 'ABRIL';
				break;
			case '05':
				$mesAnp = 'MAYO';
				break;
			case '06':
				$mesAnp = 'JUNIO';
				break;
			case '07':
				$mesAnp = 'JULIO';
				break;
			case '08':
				$mesAnp = 'AGOSTO';
				break;
			case '09':
				$mesAnp = 'SEPTIEMBRE';
				break;
			case '10':
				$mesAnp = 'OCTUBRE';
				break;
			case '11':
				$mesAnp = 'NOVIEMBRE';
				break;
			case '12':
				$mesAnp = 'DICIEMBRE';
				break;
		}
		
		$data['mesA'] = $mesA;
		$data['mesAn'] = $mesAn;
		$data['mesAnp'] = $mesAnp;



		$sql ="SELECT efosActualiza FROM  users WHERE RFC = '".$_SESSION['SEUS']['RFC']."'";
		$efosActualiza = $this->db->query($sql)->result_array();
		$data['efosActualiza'] = $efosActualiza[0];


		
		$sql ="SELECT * FROM  proveedores_sin_cfdi WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$proveedores_sin_cfdi = $this->db->query($sql)->result_array();
		$data['proveedores_sin_cfdi'] = count($proveedores_sin_cfdi);


		$sql ="SELECT * FROM  proveedores WHERE rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$proveedores_con_cfdi = $this->db->query($sql)->result_array();
		$data['proveedores_con_cfdi'] = count($proveedores_con_cfdi);



		/* Prueba de conexion a la DB */
		$sql ="SELECT * FROM proveedores WHERE   rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedores2c'] = count($agencias);

		
		$sql ="SELECT * FROM proveedores WHERE constancia2='1' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresPositivoc'] = count($agencias);

		$sql ="SELECT * FROM proveedores WHERE constancia2='0' AND  rfc_identy = '".$_SESSION['SEUS']['RFC']."'";
		$agencias = $this->db->query($sql)->result_array();
		$data['proveedoresSinRespuestac'] = count($agencias);

		//$this->view("dashboard/home", $data);
		$this->view2("dashboard/home", $data);
	}

	public function menu(){
		$data = array();
		$this->load->database();

		$this->view3("dashboard/menu", $data);

	}

	public function crearSessionAgencias(){


		$data = json_decode($_POST['agencias']);


		if(!empty($data)){
			$this->session->unset_userdata('agencias_session');
			$this->session->set_userdata('agencias_session', $data);
			$respuesta = array('Msj' => "Exito");
			echo json_encode($respuesta);

		}else{
			$respuesta = array('Msj' => "Error");
			echo json_encode($respuesta);
		}


	}
	
	
}
// END Searchs controller

/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
