<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Grupo solana - Sistemas
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2020 - Grupo solana
 * @Developer: Jessep Barba (jessep.martinez@gruposolana.com)
 * @Create: 19-Mayo-2020
 * 
*/

class Login extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');		
		$this->load->model('login_model');
				
	}	

	/*************************************Cargamos la vista de login****************************************** */
	public function index($mensaje = '')
	{
		$data = array();
		$data['error'] = $mensaje;
		$this->load->view("login/login.php", $data);
	}

	function is_valid_email($str)
	{
		return (false !== strpos($str, "@") && false !== strpos($str, "."));
	}


	public function recuperaPass($mensaje = ''){

		$data = array();
		$data['error'] = $mensaje;
		$this->load->view("login/loginPass.php", $data);




	}


	public function validPass(){


		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$password = '';
		for ($i = 0; $i < 10; $i++) {
			$password .= $characters[rand(0, $charactersLength - 1)];
		}

		$pw = hash('sha256',$password);

		$data2 = array(
			'use_key' => $pw,
		
		);
	
		$this->db->where('use_login',$_POST['usuarios']);
		$this->db->update('users', $data2);


		$data = array();
			$mensaje = '
			<tablet>
			  <tr>
				<td colspan="2"><img src="'.base_url().'inc/logo.png" style="width:50%"/><td>
			  </tr>
			  <tr>
			  <td colspan="2"><br></td>
			   </tr>
			  <tr>
				 <td colspan="2"> Estimado(a) usuario tu contraseña a sido restaurada</td>
			  </tr>
			  <tr>
				 <td colspan="2" align="center"><a href="'.base_url().'" >'.base_url().'</a></td>
			  </tr>
			  
			  <tr>
				 <td >Usuario:</td>
				 <td align="center">'.$_POST['usuarios'].'</td>
			  </tr>
			  <tr>
				 <td >Password:</td>
				 <td align="center">'.$password.'</td>
			  </tr>
			  <tr>
				<td colspan="2"><br><br><br></td>
			  </tr>
			   <tr>
				<td colspan="2"><br><br><br></td>
			  </tr>
			</tablet>
			';

			$this->load->library('email');
			$config['protocol']    = 'smtp';
			$config['smtp_host']    = 'smtp.sendgrid.net';
			$config['smtp_port']    = '587';
			$config['smtp_timeout'] = '7';
			$config['smtp_user']    = 'grupociosa';
			$config['smtp_pass']    = 'Ciosagrupo03';
			$config['charset']    = 'utf-8';
			$config['newline']    = "\r\n";
			$config['mailtype'] = 'html'; // or html
			$config['validation'] = TRUE; // bool whether to validate email or not      

				$this->email->initialize($config);

				$sql ="SELECT * FROM users  WHERE use_login = '".$_POST['usuarios']."' GROUP BY RFC";
				$rfc = $this->db->query($sql)->result_array();


				$this->email->from('no-replay@zilamodule.com', 'Restaurar contraseña ZILA Module');
		        $this->email->to($rfc[0]['use_email']);
		        $this->email->subject('Accesos ZILA Module');
		        $this->email->message($mensaje);
		        $this->email->send();


				redirect(base_url().'login/index/Restaruarda');
	}
	

	public function valid()
	{
		
			$login = $this->login_model->loginUser($_POST['usuarios'], $_POST['password']);
			if(isset($login['error']) && "" != $login['error'])
			{
				redirect( base_url() . "login" );
			}
			else
			{
				if($_POST['usuarios'] != null && $_POST['password'] != null){	
					 // Agrega usuario a SESSIO
					 $data = array(
						'usuario' => $login['use_login'],
						'fecha' =>date('Y-m-d')
					);
					$this->load->database();
					$this->db->insert('log_ingresos', $data);



					$data2 = array(
						'use_lastaccess' => date('Y-m-d H:s:i'),
					
					);
					$this->db->where('RFC',$login['RFC']);
					$this->db->where('use_login',$login['use_login']);
					$this->db->update('users', $data2);
					
					$this->session->set_userdata('SEUS',$login);
		        	redirect(base_url()."home/menu");
				
				}else{
					redirect(base_url() . "login");
				
				}	
								

			}
		
		
	}

	
	public function logout()
	{

		$this->session->unset_userdata('SEUS');
		redirect(base_url().'login');
	}

	public function subir32D(){
		$data = array();
		
		$this->load->view("login/subir32d.php", $data);
	}





	public function apiprovedoressatatussat(){

		$data = array();
		$this->load->database();
		$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_sentido_32d_prov.aspx?wsdl";
		$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_sentido_32d_prov.aspx";
		$client = new SoapClient($url, [] );

		$sql2 ="SELECT * FROM proveedores";
		$proveedores = $this->db->query($sql2)->result_array();

		//[Error_ban] => S
		//[Result] => No se encuentra información de la 32D para el rfc SCU010206HU2
		//
		/******HACER UN PROCESO POR MES COMO GUARDAR TAMBIEN EN HISTORICO3D*/

		foreach ($proveedores as $key => $value) {
			$params = array(
				"Rfc" => $value['RFC']
			  );
	
			  
			  $client->__setLocation($url2);
			  $response = $client->Execute($params);
	
			  dd($response,false);
		}
	
	}
	


	/*************************************** */




	public function apitotalesEmitidosRecibidos(){

				$data = array();
				$this->load->database();

				$sql ="SELECT RFC FROM users  WHERE RFC = 'REGM740826EX6' GROUP BY RFC";
				$rfc = $this->db->query($sql)->result_array();

				

	
				$L = new DateTime( date('Y').'-'.date('m').'-01' ); 
				$fechaFin = $L->format( 't/m/Y' );
			
				foreach ($rfc as $key => $value) {

				$sql ="SELECT fecha FROM cfdis_dashborad WHERE rfc_identy = '".$value['RFC']."'";
				$dash = $this->db->query($sql)->result_array();
				if(!empty($dash)){
						if($dash[0]['fecha'] == date('Y-m')){

							$insert = 'No';

						}else{
							$insert = 'Si';
						}

				}else{
					$insert = 'Si';
				}

					$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_cfdis.aspx?wsdl";
					$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_cfdis.aspx";
					$client = new SoapClient($url, [] );
					//var_dump($client->__getFunctions()); 
					//var_dump($client->__getTypes()); 
					/******HACER UN PROCESO POR MES */
					$params = array(
						"Tipo_com" => 'R',
						"Rfc" =>  $value['RFC'],
						//"Inidate" =>  '01/'.date("m").'/'.date("Y").'',
						//"Findate" =>  $fechaFin
						"Inidate" =>  '01/01/2021',
						"Findate" =>  '31/01/2021'
					  );
	
		
					  $client->__setLocation($url2);
					  $response = $client->Execute($params);

				

					  $contadorRecibidas = 0;
					  $montoRecibidos = 0;
					  foreach ($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS as $key2 => $value2) {
						$contadorRecibidas++;
						$montoRecibidos += $value2->FEMVSATTOT;
					  }

					 


					  $params = array(
						"Tipo_com" => 'E',
						"Rfc" =>  $value['RFC'],
						//"Inidate" =>  '01/'.date("m").'/'.date("Y").'',
						//"Findate" =>  $fechaFin
						"Inidate" =>  '01/01/2021',
						"Findate" =>  '31/01/2021'
					  );
	
		
					  $client->__setLocation($url2);
					  $response = $client->Execute($params);
						  
				
					  $contadorEmitidos = 0;
					  $montoEmitidos = 0;
					  if(isset($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS)){
						foreach ($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS as $key2 => $value2) {
							$contadorEmitidos++;
							$montoEmitidos += $value2->FEMVSATTOT;
						  }
					  }
					 


					  if($insert == 'No'){
							$data = array(
								'total_emitidos' => $contadorEmitidos,
								'monto_emitidos' => $montoEmitidos,
								'total_recibidos' => $contadorRecibidas,
								'monto_recibidos' => $montoRecibidos,
								'fecha' => date('Y-m')
							);
							
							$this->db->where('rfc_identy', $value['RFC']);
							$this->db->where('fecha', date('Y-m'));
							$this->db->update('cfdis_dashborad', $data);
					  }else{
						$data = array(
							'total_emitidos' => $contadorEmitidos,
							'monto_emitidos' => $montoEmitidos,
							'total_recibidos' => $contadorRecibidas,
							'monto_recibidos' => $montoRecibidos,
							'fecha' => date('Y-m'),
							'rfc_identy' => $value['RFC']
						);
						$this->db->insert('cfdis_dashborad', $data);

					  }
				}
	}


	public function cargarMontos($rfc = null){


		set_time_limit(500);
		echo $anoInicial =  date('Y');
		$anoFinal = date('Y');

			    ///////////////////////// - 1
				$contadorEmitidos = 0;
				$montoEmitidos = 0;
				$contadorRecibidas = 0;
				$montoRecibidos = 0;
				
				
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'01',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);
				
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'02',$rfc);	
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'03',$rfc);	
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'04',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'05',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'06',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'07',$rfc);	
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'08',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'09',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'10',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'11',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);
				$data = $this->apiproveedoresRFCMonto($anoInicial-1,'12',$rfc);	
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];


				//////////////////// -2
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'01',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);
				
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'02',$rfc);	
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'03',$rfc);	
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'04',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'05',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'06',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'07',$rfc);	
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'08',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'09',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'10',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);	
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'11',$rfc);
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];
				sleep(5);
				$data = $this->apiproveedoresRFCMonto($anoInicial-2,'12',$rfc);	
				$contadorEmitidos += $data['contadorEmitidos'];
				$montoEmitidos += $data['montoEmitidos'];
				$contadorRecibidas += $data['contadorRecibidas'];
				$montoRecibidos += $data['montoRecibidos'];

	//////////////////// -3
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'01',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'02',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'03',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'04',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'05',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'06',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'07',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'08',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'09',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'10',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'11',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-3,'12',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];



	//////////////////// -4
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'01',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'02',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'03',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'04',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'05',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'06',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'07',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'08',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'09',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'10',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'11',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-4,'12',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	


	//////////////////// -5
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'01',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'02',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'03',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'04',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'05',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'06',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'07',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'08',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'09',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'10',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);	
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'11',$rfc);
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];
	sleep(5);
	$data = $this->apiproveedoresRFCMonto($anoInicial-5,'12',$rfc);	
	$contadorEmitidos += $data['contadorEmitidos'];
	$montoEmitidos += $data['montoEmitidos'];
	$contadorRecibidas += $data['contadorRecibidas'];
	$montoRecibidos += $data['montoRecibidos'];










				$data = array(
					'total_emitidos' => $contadorEmitidos,
					'monto_emitidos' => $montoEmitidos,
					'total_recibidos' => $contadorRecibidas,
					'monto_recibidos' => $montoRecibidos,
					'fecha' => date('Y-m'),
					'rfc_identy' => $rfc
				);
				$this->db->insert('cfdis_dashborad', $data);


	}


	public function apiproveedoresRFCMonto($ano = null, $mes = null, $rfc=null){

		$data = array();
		$this->load->database();


		$sql ="SELECT RFC FROM users WHERE RFC = '".$rfc."' GROUP BY RFC";
		

		
		
		$rfc = $this->db->query($sql)->result_array();
		$L = new DateTime( date('Y').'-'.date('m').'-01' ); 
		$fechaFin = $L->format( 't/m/Y' );
		
		foreach ($rfc as $key => $value) {
					$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_cfdis.aspx?wsdl";
					$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_cfdis.aspx";
					$client = new SoapClient($url, [] );
					/******HACER UN PROCESO POR MES */
					if($ano != NULL && $mes != NULL){
						$L = new DateTime( $ano.'-'.$mes.'-01' ); 
						$fechaFin = $L->format( 't/m/Y' );
						$params = array(
							"Tipo_com" => 'R',
							"Rfc" =>  $value['RFC'],
							"Inidate" =>  '01/'.$mes.'/'.$ano.'',
							"Findate" =>  $fechaFin
						  );
					}else{
						$params = array(
							"Tipo_com" => 'R',
							"Rfc" =>  $value['RFC'],
							"Inidate" =>  '01/'.date("m").'/'.date("Y").'',
							"Findate" =>  $fechaFin
						  );
					}
					
					//ini_set('memory_limit', '-1');
					$client->__setLocation($url2);
					$response = $client->Execute($params);

					$contadorRecibidas = 0;
					$montoRecibidos = 0;
					if(isset($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS)){

					
						if(isset($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT)){
							$contadorRecibidas++;
							$montoRecibidos += $response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT;

							$dt = new DateTime($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATFEC);
							$data = array(
								'FEMVSATERFC' =>$response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATERFC,
								'FEMVSATENOM' => $response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATENOM,
								'FEMVSATFEC' =>$dt->format('Y-m-d'),
								'FEMVSATSTOT' =>$response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATSTOT,
								'FEMVSATIVA' => $response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATIVA,
								'FEMVSATTOT'=> $response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT,
								'rfc_identy' => $value['RFC']
							);
							$this->db->insert('cfdis_historicos', $data);
						}else{
							foreach ($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS as $key2 => $value2) {
								$contadorRecibidas++;
								$montoRecibidos += $value2->FEMVSATTOT;

								$dt = new DateTime($value2->FEMVSATFEC);
								$data = array(
									'FEMVSATERFC' =>$value2->FEMVSATERFC,
									'FEMVSATENOM' => $value2->FEMVSATENOM,
									'FEMVSATFEC' => $dt->format('Y-m-d'),
									'FEMVSATSTOT' => $value2->FEMVSATSTOT,
									'FEMVSATIVA' => $value2->FEMVSATIVA,
									'FEMVSATTOT'=> $value2->FEMVSATTOT,
									'rfc_identy' => $value['RFC']
								);
								$this->db->insert('cfdis_historicos', $data);
							}
						}
					  }


					  $url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_cfdis.aspx?wsdl";
					$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_cfdis.aspx";
					$client = new SoapClient($url, [] );
					/******HACER UN PROCESO POR MES */
					if($ano != NULL && $mes != NULL){
						$L = new DateTime( $ano.'-'.$mes.'-01' ); 
						$fechaFin = $L->format( 't/m/Y' );
						$params = array(
							"Tipo_com" => 'E',
							"Rfc" =>  $value['RFC'],
							"Inidate" =>  '01/'.$mes.'/'.$ano.'',
							"Findate" =>  $fechaFin
						  );
					}else{
						$params = array(
							"Tipo_com" => 'E',
							"Rfc" =>  $value['RFC'],
							"Inidate" =>  '01/'.date("m").'/'.date("Y").'',
							"Findate" =>  $fechaFin
						  );
					}
					
					//ini_set('memory_limit', '-1');
					$client->__setLocation($url2);
					$response = $client->Execute($params);

					$contadorEmitidos = 0;
					$montoEmitidos = 0;
					if(isset($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS)){
						if(isset($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT)){
							$contadorEmitidos++;
							$montoEmitidos += $response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS->FEMVSATTOT;
						}else{
							foreach ($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS as $key2 => $value2) {
								$contadorEmitidos++;
								$value2->FEMVSATSTOT;
								$montoEmitidos += $value2->FEMVSATSTOT;
							}
						}

					}



					$monto = array(
						'contadorEmitidos' => $contadorEmitidos,
						'montoEmitidos' =>  $montoEmitidos,
						'contadorRecibidas' => $contadorRecibidas,
						'montoRecibidos' => $montoRecibidos
					);
					

					return $monto;

					
		}


	}


	public function cargarProvedores($rfc = null){

		set_time_limit(500);
		echo $anoInicial =  date('Y');
		$anoFinal = date('Y');

			    ///////////////////////// - 1
				$this->apiproveedoresRFC($anoInicial-1,'01',$rfc);
				echo "1";	
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-1,'02',$rfc);	
				echo "2";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-1,'03',$rfc);	
				echo "3";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-1,'04',$rfc);
				echo "4";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-1,'05',$rfc);
				echo "5";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-1,'06',$rfc);
				echo "6";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-1,'07',$rfc);	
				echo "7";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-1,'08',$rfc);
				echo "8";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-1,'09',$rfc);
				echo "9";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-1,'10',$rfc);
				echo "10";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-1,'11',$rfc);
				echo "11";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-1,'12',$rfc);	
				echo "12";



				///////////////////////// - 2
				$this->apiproveedoresRFC($anoInicial-2,'01',$rfc);
				echo "1";	
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-2,'02',$rfc);	
				echo "2";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-2,'03',$rfc);	
				echo "3";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-2,'04',$rfc);
				echo "4";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-2,'05',$rfc);
				echo "5";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-2,'06',$rfc);
				echo "6";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-2,'07',$rfc);	
				echo "7";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-2,'08',$rfc);
				echo "8";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-2,'09',$rfc);
				echo "9";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-2,'10',$rfc);
				echo "10";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-2,'11',$rfc);
				echo "11";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-2,'12',$rfc);	
				echo "12";


				///////////////////////// - 3
				$this->apiproveedoresRFC($anoInicial-3,'01',$rfc);
				echo "1";	
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-3,'02',$rfc);	
				echo "2";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-3,'03',$rfc);	
				echo "3";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-3,'04',$rfc);
				echo "4";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-3,'05',$rfc);
				echo "5";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-3,'06',$rfc);
				echo "6";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-3,'07',$rfc);	
				echo "7";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-3,'08',$rfc);
				echo "8";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-3,'09',$rfc);
				echo "9";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-3,'10',$rfc);
				echo "10";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-3,'11',$rfc);
				echo "11";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-3,'12',$rfc);	
				echo "12";



				///////////////////////// - 4
				$this->apiproveedoresRFC($anoInicial-4,'01',$rfc);
				echo "1";	
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-4,'02',$rfc);	
				echo "2";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-4,'03',$rfc);	
				echo "3";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-4,'04',$rfc);
				echo "4";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-4,'05',$rfc);
				echo "5";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-4,'06',$rfc);
				echo "6";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-4,'07',$rfc);	
				echo "7";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-4,'08',$rfc);
				echo "8";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-4,'09',$rfc);
				echo "9";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-4,'10',$rfc);
				echo "10";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-4,'11',$rfc);
				echo "11";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-4,'12',$rfc);	
				echo "12";


				///////////////////////// - 5
				$this->apiproveedoresRFC($anoInicial-5,'01',$rfc);
				echo "1";	
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-5,'02',$rfc);	
				echo "2";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-5,'03',$rfc);	
				echo "3";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-5,'04',$rfc);
				echo "4";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-5,'05',$rfc);
				echo "5";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-5,'06',$rfc);
				echo "6";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-5,'07',$rfc);	
				echo "7";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-5,'08',$rfc);
				echo "8";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-5,'09',$rfc);
				echo "9";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-5,'10',$rfc);
				echo "10";
				sleep(5);	
				$this->apiproveedoresRFC($anoInicial-5,'11',$rfc);
				echo "11";
				sleep(5);
				$this->apiproveedoresRFC($anoInicial-5,'12',$rfc);	
				echo "12";
		
		
	}

	public function apiproveedoresRFC($ano = null, $mes = null, $rfc=null){
		$data = array();
		$this->load->database();

		if($rfc==null){
			$sql ="SELECT RFC FROM users GROUP BY RFC";
		}else{
			$sql ="SELECT RFC FROM users WHERE RFC = '".$rfc."' GROUP BY RFC";
		
		}
		
		
		$rfc = $this->db->query($sql)->result_array();
		$L = new DateTime( date('Y').'-'.date('m').'-01' ); 
		$fechaFin = $L->format( 't/m/Y' );
		
		foreach ($rfc as $key => $value) {
					$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_cfdis.aspx?wsdl";
					$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_cfdis.aspx";
					$client = new SoapClient($url, [] );
					/******HACER UN PROCESO POR MES */
					if($ano != NULL && $mes != NULL){
						$L = new DateTime( $ano.'-'.$mes.'-01' ); 
						$fechaFin = $L->format( 't/m/Y' );
						$params = array(
							"Tipo_com" => 'R',
							"Rfc" =>  $value['RFC'],
							"Inidate" =>  '01/'.$mes.'/'.$ano.'',
							"Findate" =>  $fechaFin
						  );
					}else{
						$params = array(
							"Tipo_com" => 'R',
							"Rfc" =>  $value['RFC'],
							"Inidate" =>  '01/'.date("m").'/'.date("Y").'',
							"Findate" =>  $fechaFin
						  );
					}
					
					//ini_set('memory_limit', '-1');
					$client->__setLocation($url2);
					$response = $client->Execute($params);
					if(isset($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS)){
						foreach ($response->Sdt_femvsat1_ws->SDT_FEMVSAT1_WS as $key2 => $value2) {

							if(isset($value2->FEMVSAT3->FEMVSAT3Item->FEMVSAT3LIN)){
	
								
								$codigo = $value2->FEMVSAT3->FEMVSAT3Item->FEMVSAT3CLAPRO;
								$sql ="SELECT * FROM codigo_servicio WHERE codigo='".$codigo."'";
								$agencias = $this->db->query($sql)->result_array();
								if(count($agencias)>0){
									$tipo ='Servicios';
								}else{
									$tipo ='Otros';
								}
		
							}else{
							
								if(isset($value2->FEMVSAT3->FEMVSAT3Item)){
									$codigo = $value2->FEMVSAT3->FEMVSAT3Item[0]->FEMVSAT3CLAPRO;
									$sql ="SELECT * FROM codigo_servicio WHERE codigo='".$codigo."'";
									$agencias = $this->db->query($sql)->result_array();
									if(count($agencias)>0){
										$tipo ='Servicios';
									}else{
										$tipo ='Otros';
									}
								}
								
		
							}
		
		
							$sql2 ="SELECT * FROM proveedores WHERE RFC='".$value2->FEMVSATERFC."'";
							$proveedores = $this->db->query($sql2)->result_array();
		
							if(empty($proveedores)){
								$data = array(
									'tipoProveedor' => $tipo,
									'RazonSocial' => $value2->FEMVSATENOM,
									'RFC' => $value2->FEMVSATERFC,
									'Estatus' => 'SinRespuesta',
									'rfc_identy' => $value['RFC'],
									'fecha_constancia1' => date('Y-m-d'),
									'fecha_constancia2' => date('Y-m-d')
								);
						
								$this->db->insert('proveedores', $data);
		
							}
									
		
						}

					}else{
						echo "sin informacion";
						echo '<br>';
						print_r($params);
						//dd($params,false);

					}
					
		}


	}///fin



	public function apiproveedoresRFCHistorico(){
		$data = array();
		$this->load->database();

		$sql ="SELECT RFC FROM users GROUP BY RFC";
		$rfc = $this->db->query($sql)->result_array();



			
		foreach ($rfc as $key => $value) {

					$dia = date('d');
		if($dia == '01'){	
			$data2 = array(
				'constancia' => 0
			);
			$this->db->where('rfc_identy', $value2['RFC']);
			$this->db->update('proveedores', $data2);
		}

			$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_sentido_32d_prov.aspx?wsdl";
			$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_sentido_32d_prov.aspx";
			$client = new SoapClient($url, [] );

			$sql2 ="SELECT * FROM proveedores WHERE  rfc_identy ='".$value['RFC']."'";
			$proveedores = $this->db->query($sql2)->result_array();
	
			//[Error_ban] => S
			//[Result] => No se encuentra información de la 32D para el rfc SCU010206HU2
			//
			/******HACER UN PROCESO POR MES COMO GUARDAR TAMBIEN EN HISTORICO3D*/

			foreach ($proveedores as $key2 => $value2) {
				$params = array(
					"Rfc" => $value2['RFC']
				);
		
				
				$client->__setLocation($url2);
				$response = $client->Execute($params);

			
		
				/********************falta ver demas estatus */
	

				if (strpos($response->Result, 'No se encuentra información de la 32D') !== false) {
					$status = 'SinRespuesta';
					$constancia = 0;
				}else{

					if($response->Result == 'Positivo'){
						$status = 'Positivo';
						$constancia = 1;
					}else{
						$status = 'Negativo';
						$constancia = 0;
					}
					
				}

				$data = array(
					'rfc' => $value2['RFC'],
					'razon_social' =>$value2['RazonSocial'],
					'status' => $value2['Estatus'],
					'fecha' => $value2['fecha_constancia1'],
					'tipoProveedor' =>$value2['tipoProveedor'],
					'constancia' => $value2['constancia'],
					'rfc_identy' => $value['RFC']
				);
		
				$this->db->insert('historia3d', $data);


				
				$data2 = array(
					'Estatus' => $status,
					'fecha_constancia1' => date('Y-m-d'),
					'constancia' => $constancia
				);
				
				$this->db->where('rfc', $value2['RFC']);
				$this->db->update('proveedores', $data2);



			}

		}
	}//FIN





	public function apiproveedoresConstanciaHistorico(){
		$data = array();
		$this->load->database();

		$sql ="SELECT RFC FROM users GROUP BY RFC";
		$rfc = $this->db->query($sql)->result_array();

		foreach ($rfc as $key => $value) {
			$sql2 ="SELECT * FROM proveedores WHERE  rfc_identy ='".$value['RFC']."'";
			$proveedores = $this->db->query($sql2)->result_array();


			foreach ($proveedores as $key2 => $value2) {

					$data = array(
						'RFC' => $value2['RFC'],
						'Nombre' =>$value2['RazonSocial'],
						'Situacion' => $value2['Estatus'],
						'fecha' => date('Y-m-d'),
						'tipoProveedor' =>$value2['tipoProveedor'],
						'constancia' => $value2['constancia'],
						'rfc_identy' => $value['RFC']
					);
			
					$this->db->insert('historia3d', $data);
			}
		}
	}//FIN

	


	public function ejempliapiEnviarcorreo(){

		$texto = 'hola';
		 $html = '<html>
			<img src="'.base_url().'/inc/logo.png" alt=""  width=""/>
			<br><br><br>
			'.$texto.'
		   
		</html>
		';


		$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_enviar_correo_prov.aspx?wsdl";
		$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_enviar_correo_prov.aspx";
		$client = new SoapClient($url, [] );

		$params = array(
			"Parmfeciasrfc" => 'REGM740826EX6',
			"Correo" => 'jessep.barba@gmail.com', 
			"Pruserrfc" => 'MTV760226G73',
			"Nombre" => 'Jessep Barba',
			"Asunto" => 'Solicitud de opinión de cumplimiento',
			"Html" => 'Prueba'
		);

		$client->__setLocation($url2);
		$response = $client->Execute($params);

		dd($response);
	}//FIN


	public function apiEnviarcorreo(){

		$data = array();
		$this->load->database();

		$sql ="SELECT RFC FROM users GROUP BY RFC";
		$rfc = $this->db->query($sql)->result_array();
		
		foreach ($rfc as $key => $value) {

							$sql2 ="SELECT * FROM configuracion_frecuencia WHERE  rfc_identy ='".$value['RFC']."'";
							$correo = $this->db->query($sql2)->result_array();

			if(!empty($correo)){
				
		

							$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_enviar_correo_prov.aspx?wsdl";
							$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_enviar_correo_prov.aspx";
							$client = new SoapClient($url, [] );

							$sql2 ="SELECT * FROM proveedores WHERE  rfc_identy ='".$value['RFC']."'";
							$proveedores = $this->db->query($sql2)->result_array();

							
							if($correo[0]['frecuencia'] == 'Mes' && date('d') == $correo[0]['diames']){
								$html = '<html>
									<img src="'.base_url().'/inc/logo.png" alt=""  width=""/>
									<br><br><br>
									'.$correo[0]['texto'].'
								
								</html>
								';

						

								foreach ($proveedores as $key2 => $value2) {
									$params = array(
										"Parmfeciasrfc" => $correo[0]['rfc_identy'],
										"Correo" => $value2['Correo'],
										"Pruserrfc" => $value2['RFC'],
										"Nombre" => $value2['RazonSocial'],
										"Asunto" => 'Solicitud de opinión de cumplimiento',
										"Html" => $html
									);
									dd($params,false);
									$client->__setLocation($url2);
									$response = $client->Execute($params);
									dd($response,false);
								}
							}


							if($correo[0]['frecuencia'] == 'Dia' && date("w") == $correo[0]['diasemana']){
								foreach ($proveedores as $key2 => $value2) {

									$html = '<html>
									<img src="'.base_url().'/inc/logo.png" alt=""  width=""/>
									<br><br><br>
									'.$correo[0]['texto'].'
									</html>
									';
									$params = array(
										"Parmfeciasrfc" => $correo[0]['rfc_identy'],
										"Correo" => $value2['Correo'],
										"Pruserrfc" => $value2['RFC'],
										"Nombre" => $value2['RazonSocial'],
										"Asunto" => 'Solicitud de opinión de cumplimiento',
										"Html" => $html
									);
									dd($params,false);
									$client->__setLocation($url2);
									$response = $client->Execute($params);
									dd($response,false);
								}
							}
			
						}
		}

	}


	public function apienviarCorreoEfos(){

		$data = array();
		$this->load->database();

		$sql ="SELECT * FROM proveedores INNER JOIN efos ON efos.RFC = proveedores.RFC  WHERE msjefos = 0";
		$rfc = $this->db->query($sql)->result_array();

		foreach ($rfc as $key => $value) {

			$data = array(
				'msjefos' =>1
			);
			$this->db->where('RFC', $value['RFC']);
			$this->db->update('proveedores', $data);
	
				$this->load->library('email');
				$config['protocol']    = 'smtp';
				$config['smtp_host']    = 'smtp.sendgrid.net';
				$config['smtp_port']    = '587';
				$config['smtp_timeout'] = '7';
				$config['smtp_user']    = 'grupociosa';
				$config['smtp_pass']    = 'Ciosagrupo03';
				$config['charset']    = 'utf-8';
				$config['newline']    = "\r\n";
				$config['mailtype'] = 'html'; // or html
				$config['validation'] = TRUE; // bool whether to validate email or not      

				$this->email->initialize($config);
				$email = $value['Correo'];
				
				$this->email->from('no-replay@zilamodule.com', 'Accesos ZILA Module');
		        $this->email->to($email);
		        $this->email->subject('Accesos ZILA Module');
		        $this->email->message($mensaje);
		        $this->email->send();


		}


		$sql ="SELECT * FROM proveedores_sin_cfdi INNER JOIN efos ON efos.RFC = proveedores_sin_cfdi.RFC WHERE msjefos = 0";
		$rfc = $this->db->query($sql)->result_array();
		//dd($rfc);
		foreach ($rfc as $key => $value) {

			
			$data = array(
				'msjefos' =>1
			);
			$this->db->where('RFC', $value['RFC']);
			$this->db->update('proveedores', $data);
	

			 $this->load->library('email');
			 $asunto = 'Usuario y contraseña ZILA-MODULE';
			 $mensaje = '
				<tablet>
				  <tr>
				    <td colspan="2"><img src="'.base_url().'inc/logo.png" style="width:50%"/><td>
				  </tr>
				  <tr>
				  <td colspan="2"><br><br></td>
			   	  </tr>
				  <tr>
				     <td colspan="2"> Estimado(a) proveedor tu empresa aparece en los Efos</td>
				  </tr>
				</tablet>
				';
	
				$this->load->library('email');
				$config['protocol']    = 'smtp';
				$config['smtp_host']    = 'smtp.sendgrid.net';
				$config['smtp_port']    = '587';
				$config['smtp_timeout'] = '7';
				$config['smtp_user']    = 'grupociosa';
				$config['smtp_pass']    = 'Ciosagrupo03';
				$config['charset']    = 'utf-8';
				$config['newline']    = "\r\n";
				$config['mailtype'] = 'html'; // or html
				$config['validation'] = TRUE; // bool whether to validate email or not      

				$this->email->initialize($config);
				$email = $value['Correo'];
				
				$this->email->from('no-replay@zilamodule.com', 'Accesos ZILA Module');
		        $this->email->to($email);
		        $this->email->subject('Accesos ZILA Module');
		        $this->email->message($mensaje);
		        $this->email->send();


		}

	

	}




	public function apiproveedoresRFCHistoricoAgrupado(){
		$data = array();
		$this->load->database();

		$sql ="SELECT RFC FROM users GROUP BY RFC";
		$rfc = $this->db->query($sql)->result_array();

		foreach ($rfc as $key => $value) {
	
				$sql ="SELECT * FROM proveedores WHERE rfc_identy = '".$value['RFC']."'";
				$agencias = $this->db->query($sql)->result_array();
			
				foreach ($agencias as $key2 => $value2) {

					$sql ="SELECT RFC FROM efos_historicos_resumen WHERE RFC = '".$value2['RFC']."' AND rfc_identy = '".$value['RFC']."'";
					$agenciasH = $this->db->query($sql)->result_array();

					if(!empty($agenciasH)){
									$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_sentido_32d_prov.aspx?wsdl";
									$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_sentido_32d_prov.aspx";
									$client = new SoapClient($url, [] );

									$params = array(
										"Rfc" => $value2['RFC']
									);
							
									
									$client->__setLocation($url2);
									$response = $client->Execute($params);

									if (strpos($response->Result, 'No se encuentra información de la 32D') !== false) {
										$status = 'SinRespuesta';
									}else{
					
										if($response->Result == 'Positivo'){
											$status = 'Positivo';
										}else{
											$status = 'Negativo';
										}
										
									}

									$fechaC = date('m');
									$ano = date('Y');
									$sql3 ="SELECT RFC FROM efos_historicos_resumen WHERE ano ='".$ano."' AND mes ='".$fechaC."' AND RFC = '".$value2['RFC']."' AND rfc_identy = '".$value['RFC']."'";
									$agenciasH2 = $this->db->query($sql3)->result_array();
									if(!empty($agenciasH2)){

										$data = array(
											'Nombre' =>$value2['RazonSocial'],
											'Situacion' => $status,
											'ano' => date('Y'),
											'mes' => $fechaC,
											'rfc_identy' => $value['RFC']
										);
	
										$this->db->where('rfc', $value2['RFC']);
										$this->db->where('ano', date('Y') );
										$this->db->where('mes', date('m'));
										$this->db->update('efos_historicos_resumen', $data);

									}else{

										$data = array(
											'rfc' => $value2['RFC'],
											'Nombre' =>$value2['RazonSocial'],
											'Situacion' => $status,
											'ano' => date('Y'),
											'mes' => $fechaC,
											'rfc_identy' => $value['RFC']
										);
								
										$this->db->insert('efos_historicos_resumen', $data);

									}
						
								
					}else{
						for ($i=1; $i <= 12; $i++) { 
							
							if($i < 10){
								$fechaC = '0'.$i;
							}else{
								$fechaC = $i;
							}

							if($fechaC == date('m')){

									$url = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_sentido_32d_prov.aspx?wsdl";
									$url2 = "https://www.desserviciosweb.com.mx/ZILA/ERP_DES_FE/aws_get_sentido_32d_prov.aspx";
									$client = new SoapClient($url, [] );

									$params = array(
										"Rfc" => $value2['RFC']
									);
							
									
									$client->__setLocation($url2);
									$response = $client->Execute($params);

									if (strpos($response->Result, 'No se encuentra información de la 32D') !== false) {
										$status = 'SinRespuesta';
									}else{
					
										if($response->Result == 'Positivo'){
											$status = 'Positivo';
										}else{
											$status = 'Negativo';
										}
										
									}

									$data = array(
										'rfc' => $value2['RFC'],
										'Nombre' =>$value2['RazonSocial'],
										'Situacion' => $status,
										'ano' => date('Y'),
										'mes' => $fechaC,
										'rfc_identy' => $value['RFC']
									);
							
									$this->db->insert('efos_historicos_resumen', $data);

									break;
							}else{



								$data = array(
									'rfc' => $value2['RFC'],
									'Nombre' =>$value2['RazonSocial'],
									'Situacion' => 'SinRespuesta',
									'ano' => date('Y'),
									'mes' => $fechaC,
									'rfc_identy' => $value['RFC']
								);
						
								$this->db->insert('efos_historicos_resumen', $data);
							}
				
						}///////////////fin for
					}

				}

		}
	}




}
