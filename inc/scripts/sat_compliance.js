/**
 * @author Luis Manuel Torres Trevino
 * @version 1.0.0
 * @since 16/02/2021
 * @description Este archivo contiene las funciones utilizadas en la vista subir32d.php
 */

const INVALID_SELECTION_FILE = "Seleccione el pdf para poder continuar";

/**
 * readSingleFile
 * 
 * @description Esta funcion lee el documento desde el input
 * @author Luis Manuel Torres Trevino
 * @version 1.0.0
 * @since 16/02/2021
 * @param {Event} e evento del input file.
 */

function readSingleFile(e) {
    let file = e.target.files[0];
    if (!file) {
        console.log(INVALID_SELECTION_FILE);
        return;
    }
    var reader = new FileReader();
    reader.onload = (e) => {
        let contents = e.target.result;
        displayContents(contents);
    };
    reader.readAsText(file);
}

/**
 * displayContents
 * 
 * @description Esta funcion agrega la cadena base64 del documento pdf en un input oculto.
 * @param {string} contents 
 * @version 1.0.0
 * @since 16/02/2021
 */
function displayContents(contents) {
    var element = document.getElementById('document_base64');
    element.value = window.btoa(unescape(encodeURIComponent(contents)));
}

/**
 * validateMonth
 * 
 * @description funcion que valida el mes del documento.
 * @param {Event} e evento del input del mes 
 */
function validateMonth(e) {
    let month = e.target.value;
    let inputMonth = document.getElementById('document_month');
    if (month < 0 && month >= 12) {
        inputMonth.value = 1;
    }
}

document.getElementById('document')
    .addEventListener('change', readSingleFile, false);

document.getElementById('document_month')
    .addEventListener("change", validateMonth, false);